unit ufrmBasePurchase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxCalendar, cxDBLookupComboBox, cxDropDownEdit, cxMemo, cxCurrencyEdit,
  cxSpinEdit, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxContainer,
  Datasnap.DBClient, Vcl.ExtCtrls, cxDBEdit, cxLabel, cxMaskEdit, Vcl.StdCtrls,
  cxGroupBox, RzButton, RzPanel,ufrmBaseController, Vcl.Menus, Data.Win.ADODB,
  cxButtons, Datasnap.Provider, cxLookupEdit, cxDBLookupEdit;

type
  TfrmBasePurchase = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    RzToolbar13: TRzToolbar;
    RzSpacer30: TRzSpacer;
    RzToolButton85: TRzToolButton;
    RzSpacer113: TRzSpacer;
    RzToolButton87: TRzToolButton;
    RzToolButton88: TRzToolButton;
    RzToolButton90: TRzToolButton;
    RzSpacer120: TRzSpacer;
    RzSpacer1: TRzSpacer;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewColumn24: TcxGridDBColumn;
    tvViewColumn28: TcxGridDBColumn;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn17: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn21: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    tvViewColumn9: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    tvViewColumn14: TcxGridDBColumn;
    tvViewColumn25: TcxGridDBColumn;
    tvViewColumn26: TcxGridDBColumn;
    tvViewColumn27: TcxGridDBColumn;
    tvViewColumn13: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzPanel3: TRzPanel;
    Bevel2: TBevel;
    cxGroupBox4: TcxGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label3: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    cxSpinEdit5: TcxSpinEdit;
    cxComboBox1: TcxComboBox;
    cxGroupBox2: TcxGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    cxDBMemo2: TcxDBMemo;
    cxDBSpinEdit2: TcxDBSpinEdit;
    cxDBSpinEdit3: TcxDBSpinEdit;
    cxLabel1: TcxLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxLabel2: TcxLabel;
    cxComboBox2: TcxComboBox;
    RzPanel4: TRzPanel;
    Bevel1: TBevel;
    cxDBTextEdit3: TcxDBTextEdit;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    DataMaking: TDataSource;
    ADOMaking: TADOQuery;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    RzSpacer10: TRzSpacer;
    btnGridSet: TRzToolButton;
    ds: TDataSource;
    Storage: TADOQuery;
    tvViewColumn16: TcxGridDBColumn;
    ADOSignName: TADOQuery;
    DataSignName: TDataSource;
    RzPanel13: TRzPanel;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    tvViewColumn15: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    Label5: TLabel;
    Label1: TLabel;
    cxDBLookupComboBox1: TcxDBLookupComboBox;
    cxDBLookupComboBox2: TcxDBLookupComboBox;
    ADOSpec: TADOQuery;
    DataSpec: TDataSource;
    cxDBLookupComboBox3: TcxDBLookupComboBox;
    procedure FormCreate(Sender: TObject);
    procedure tvViewDblClick(Sender: TObject);
    procedure RzToolButton90Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton88Click(Sender: TObject);
    procedure tvViewColumn24GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton85Click(Sender: TObject);
    procedure StorageAfterInsert(DataSet: TDataSet);
    procedure tvViewColumn4PropertiesCloseUp(Sender: TObject);
    procedure tvViewColumn4PropertiesInitPopup(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxDBMemo2PropertiesChange(Sender: TObject);
    procedure cxDBSpinEdit3PropertiesChange(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvViewColumn10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvViewColumn9GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvViewColumn8PropertiesInitPopup(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure btnGridSetClick(Sender: TObject);
    procedure tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure tvViewColumn8PropertiesPopup(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure StorageAfterOpen(DataSet: TDataSet);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure cxComboBox2PropertiesCloseUp(Sender: TObject);
    procedure tvViewColumn8PropertiesCloseUp(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
  private
    { Private declarations }
    Radix : Integer;
    procedure PopupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
     function GetRadix(lpRadix : Integer):BOOL;
  public
    { Public declarations }
    sqltext : string;
    g_ProjectName : string;
    g_PostCode    : string;
    g_sqlText : string;
    IsSave : Boolean;
    IsEidt : Boolean;
    Prefix : string;
    Suffix : string;
    EnterStorage : Double;
    ComeStorage  : Double;
    ExistStorage : Double;
    ReturnCount  : Double;
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    procedure GetSumMoney(Count , UnitPrice : Variant);
  end;

var
  frmBasePurchase: TfrmBasePurchase;


implementation

uses
   uProjectFrame,global,uDataModule,ufunctions,System.Math,ufrmIsViewGrid;

type
  TCusDropDownEdit = class(TcxCustomDropDownEdit);

{$R *.dfm}

procedure TfrmBasePurchase.PopupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 27 then //如果按下“ESC”键
    TcxCustomEditPopupWindow(sender).ClosePopup;//关闭弹出窗
end;

procedure  TfrmBasePurchase.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;

begin
  case Message.Msg of
    WM_FrameClose:
    begin
      Self.ADOMaking.Close;
      Self.Storage.Close;
    end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode    := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;
      if not IsEidt then
      begin

        g_sqlText := 'Select * from ' + g_Table_Project_BuildStorage +
                     ' Where Code ="' +  g_PostCode +
                     '" AND ' + Self.tvViewColumn16.DataBinding.FieldName + '="入库"';
        with Self.Storage do
        begin
          Close;
          SQL.Text := g_sqlText;
          Open;
        end;

        with Self.ADOMaking do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from '+ g_Table_Maintain_Science +
                      ' Where ProjectName="' + g_ProjectName +'"';
          Open;
        end;

        with Self.ADOSpec do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from '+ g_Table_MakingsList;
          Open;
        end;

      end;

      with Self.ADOSignName do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select pt.SignName AS Name,pycode from '+ g_Table_Maintain_Science +
                    ' AS PT left join '+ g_Table_CompanyManage +
                    ' AS PS on PT.SignName = PS.SignName Where ProjectName="'+
                    g_ProjectName +
                    '" group by pt.SignName,pycode';
        Open;
      end;

    end;
    WM_FrameDele :
    begin

      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;
      szTableList[0].ATableName := g_Table_Project_BuildStorage;
      szTableList[0].ADirectory := g_DirBuildStorage;
      DeleteTableFile(szTableList,szCodeList);
      DM.InDeleteData(g_Table_Maintain_Work,szCodeList); //清除主目录表内关联

    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmBasePurchase.RzToolButton85Click(Sender: TObject);
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  adoTmp : TADOQuery;
  szCount : Integer;
  szRowsCount , lpDir : string;
  szIndex : Integer;
  s : Variant ;
  szFieldName : string;

begin
  szFieldName := Self.tvViewColumn1.DataBinding.FieldName;
  szCount := Self.tvView.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;

  if szCount > 0 then
  begin
      if Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin

        with Self.tvView  do
        begin
          if not DataController.DataSource.DataSet.IsEmpty then
          begin
            str := '';
            for I := szCount -1 downto 0 do
            begin

              szIndex := GetColumnByFieldName( szFieldName ).Index;
              s := Controller.SelectedRows[i].Values[szIndex];
              Id := VarToStr(s);

              if Length(id) <> 0 then
              begin

                if Length(lpDir) <> 0 then
                begin
                  dir := ConcatEnclosure(lpDir, id);
                  if DirectoryExists(dir) then
                  begin
                    DeleteDirectory(dir);
                  end;

                end;

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;
              end;

            end;
            Controller.DeleteSelection;

            if (str <> ',') and (Length(str) <> 0) and ( Length( szFieldName ) <> 0) then
            begin

              with DM.Qry do
              begin

                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_Project_BuildStorage +' where '+ szFieldName +' in("' + str + '")';
                ExecSQL;

              end;

            end;

          end;

        end;

      end;
  end;

end;

procedure TfrmBasePurchase.RzToolButton88Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmBasePurchase.RzToolButton90Click(Sender: TObject);
begin
  inherited;
  //数据保存锁定
//  if Self.tvView.DataController.IsEditing then  Self.tvView.DataController.Post();
  IsDeleteEmptyData(Self.Storage ,
                    g_Table_Project_BuildStorage ,
                    Self.tvViewColumn15.DataBinding.FieldName);
  IsSave := False;
end;

procedure TfrmBasePurchase.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmBasePurchase.StorageAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  inherited;

  with DataSet do
  begin
    if State <> dsInactive then
    begin

      FieldByName('Code').Value := g_PostCode;
      szColName := Self.tvViewColumn3.DataBinding.FieldName;

      FieldByName(szColName).Value := g_ProjectName;
      szColName := Self.tvViewColumn28.DataBinding.FieldName;
      FieldByName(szColName).Value := True;
      szColName := Self.tvViewColumn2.DataBinding.FieldName;
      FieldByName(szColName).Value := Date;


      szColName := Self.tvViewColumn15.DataBinding.FieldName;
      FieldByName(szColName).Value := '';

      szColName := Self.tvViewColumn4.DataBinding.FieldName;
      FieldByName(szColName).Value := '';

      szColName := Self.tvViewColumn16.DataBinding.FieldName;
      FieldByName(szColName).Value := '入库';

      szColName := Self.tvViewColumn6.DataBinding.FieldName; //变量数
      FieldByName(szColName).Value := 1;

      Self.tvViewColumn26.EditValue := '帐目入库';

      szColName := Self.tvViewColumn1.DataBinding.FieldName; //编号
      szCode := Prefix + GetRowCode(g_Table_Project_BuildStorage,szColName,Suffix,20000 + Self.tvView.DataController.RecordCount);
      FieldByName(szColName).Value := szCode;

    end;

  end;

end;

procedure TfrmBasePurchase.StorageAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmBasePurchase.GetSumMoney(Count , UnitPrice : Variant);
var
  szRowIndex : Integer;
  szSumMoney : Currency;
  szColName : string;
begin
  inherited;
//  Self.tvViewColumn8.Index //数量
//  Count := VarToStr(Count);
//  UnitPrice := VarToStr(UnitPrice);

//  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
//  Count := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn8.Index];

  if (UnitPrice <> null) and (Count <> null) then
  begin

    szSumMoney := StrToCurrDef(VarToStr(UnitPrice),0) * StrToFloatDef( VarToStr(Count) ,0 );

    with Self.Storage do
    begin
      if State <> dsInactive then
      begin
        if (State = dsInsert) or (State = dsEdit) then
        begin
          szColName := Self.tvViewColumn12.DataBinding.FieldName;
          FieldByName(szColName).Value := szSumMoney;
          szColName := Self.tvViewColumn10.DataBinding.FieldName;
          FieldByName(szColName).Value := UnitPrice;
          szColName := Self.tvViewColumn8.DataBinding.FieldName;
          FieldByName(szColName).Value := Count;
        end;

      end;

    end;

  end;
end;


procedure TfrmBasePurchase.tvViewColumn10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szRowIndex : Integer;
  Count : Variant;
  szSumMoney : Currency;
  szColName : string;

begin
  inherited;
  //Self.tvViewColumn8.Index //数量
  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
  Count := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn8.Index];
  GetSumMoney(Count,DisplayValue);
  {
  if (DisplayValue <> null) and (Count <> null) then
  begin
    szSumMoney := DisplayValue * Count;

    with Self.Storage do
    begin
      if State <> dsInactive then
      begin
        if (State = dsInsert) or (State = dsEdit) then
        begin
          szColName := Self.tvViewColumn12.DataBinding.FieldName;
          FieldByName(szColName).Value := szSumMoney;
          szColName := Self.tvViewColumn10.DataBinding.FieldName;
          FieldByName(szColName).Value := DisplayValue;
        end;

      end;

    end;

  end;
  }
end;

procedure TfrmBasePurchase.tvViewColumn24GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index +1 );
end;

procedure TfrmBasePurchase.tvViewColumn4PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;
  szColContentB : Variant;
  szColContentC : Variant;
  szColContentD : Variant;
  szColContentE : Variant;
  szColContentF : Variant;
  szColContentG : Variant;
  szRowIndex:Integer;
  szColName : string;
  szCount : Variant;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;

    if szRowIndex >= 0 then
    begin

      szColContentA :=  DataController.Values[szRowIndex,0] ; //名称
      szColContentB :=  DataController.Values[szRowIndex,1] ; //单价
      szColContentC :=  DataController.Values[szRowIndex,2] ; //录入日期
      szColContentD :=  DataController.Values[szRowIndex,3] ; //品牌
      szColContentE :=  DataController.Values[szRowIndex,4] ; //型号
      szColContentF :=  DataController.Values[szRowIndex,5] ; //规格
      szColContentG :=  DataController.Values[szRowIndex,6] ; //单位

      if szColContentA <> null then
      begin

        with Self.Storage do
        begin
          if (State <> dsInactive) then
          begin
            if (State <> dsInsert) or (State <> dsEdit) then  Edit;

            szColName := Self.tvViewColumn4.DataBinding.FieldName;
            FieldByName(szColName).Value := szColContentA;
            szColName := Self.tvViewColumn5.DataBinding.FieldName; //规格
            FieldByName(szColName).Value := szColContentF;
            szColName := Self.tvViewColumn10.DataBinding.FieldName;//单价
            FieldByName(szColName).Value := szColContentB; //单价
            szColName := Self.tvViewColumn9.DataBinding.FieldName; //单位
            FieldByName(szColName).Value := szColContentG;

            szRowIndex := Self.tvView.Controller.FocusedRowIndex;
            szCount := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn8.Index];
            if szCount <> null then
            begin
              szColName := Self.tvViewColumn12.DataBinding.FieldName;
              FieldByName(szColName).Value := szCount * szColContentB;
            end;
          end;

        end;

      end;

    end;

  end;

end;

procedure TfrmBasePurchase.tvViewColumn4PropertiesInitPopup(Sender: TObject);
var
  s  : Variant;
  szRowIndex : Integer;
begin
  inherited;
  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
  s := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn15.Index];
  with Self.ADOMaking do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from '+ g_Table_Maintain_Science +' WHERE SignName="' +
    VarToStr(s) + '" and ProjectName="' + g_ProjectName +'"';
    Open;
  end;
end;

procedure TfrmBasePurchase.tvViewColumn8PropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  Count,UnitPrice : Variant;

begin
  inherited;
  //Self.tvViewColumn8.Index //数量
  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
  UnitPrice := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn10.Index];
  Count := Self.cxDBSpinEdit1.EditingValue;

  GetSumMoney(Count,UnitPrice);
end;

procedure TfrmBasePurchase.tvViewColumn8PropertiesInitPopup(Sender: TObject);
begin
  inherited;
  TCusDropDownEdit(sender).PopupWindow.KeyPreview := true;//令弹出窗按键事件有效
  TCusDropDownEdit(sender).PopupWindow.OnKeyDown  := PopupKeyDown;//关联按键事件给弹出窗
end;

function TfrmBasePurchase.GetRadix(lpRadix : Integer):BOOL;
begin
  case lpRadix of
    0: Radix := -1;
    1: Radix := -2;
    2: Radix := -3;
    3: Radix := -4;
    4: Radix := -5;
    5: Radix := -6;
    6: Radix := -7;
    7: Radix := -8;
  end;
end;

procedure TfrmBasePurchase.tvViewColumn8PropertiesPopup(Sender: TObject);
begin
  inherited;
//  Radix := -2;
  GetRadix(Self.cxComboBox2.ItemIndex);
end;

procedure TfrmBasePurchase.tvViewColumn9GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;
begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;
end;

procedure TfrmBasePurchase.tvViewColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvView,1,g_DirBuildStorage);
end;

procedure TfrmBasePurchase.tvViewDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources, '\'+ g_DirBuildStorage);
  CreateEnclosure(Self.tvView.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmBasePurchase.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;

begin
  inherited;
  szColName := Self.tvViewColumn28.DataBinding.FieldName;
  if AItem.Index <> Self.tvViewColumn28.Index then
  begin
    with Self.Storage do
    begin
      if State <> dsInactive then
        begin
          if FieldByName(szColName).Value = False then
          begin
            AAllow := False;
          end else
          begin
            AAllow := True;
          end;

        end;
    end;

  end;

  if (AItem.Index = Self.tvViewColumn1.Index) or (AItem.Index = Self.tvViewColumn24.Index) then
  begin
    AAllow := False;
  end;

end;

procedure TfrmBasePurchase.tvViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if not IsEidt then
  begin
    if (Key = 13) and (AItem.Index = Self.tvViewColumn12.Index) then
    begin
      if (Self.tvView.Controller.FocusedRow.IsLast) and (IsNewRow) then
      begin
        with Self.Storage do
        begin
          if State <> dsInactive then
          begin
            Append;
            Self.tvViewColumn2.FocusWithSelection;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfrmBasePurchase.tvViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then Self.RzToolButton85.Click;
end;

procedure TfrmBasePurchase.tvViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  Value : Variant;
begin
  inherited;

  Value := ARecord.Values[Self.tvViewColumn28.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;

end;

procedure TfrmBasePurchase.tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;

  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.tvViewColumn13.Summary.FooterFormat := szCapital;
  except
  end;

end;

procedure TfrmBasePurchase.btnGridSetClick(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;

  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_DirBuildStorage;
    Child.ShowModal;
    SetcxGrid(Self.tvView,0, g_DirBuildStorage);
  finally
    Child.Free;
  end;

end;

procedure TfrmBasePurchase.cxButton1Click(Sender: TObject);
begin
  inherited;
  with Self.tvViewColumn5 do
  begin
    EditValue := Self.cxDBTextEdit3.EditValue;
    Editing   := false;
  end;

end;

procedure TfrmBasePurchase.cxButton2Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn5.Editing := False;
end;

procedure TfrmBasePurchase.cxButton3Click(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  with Self.tvViewColumn8 do
  begin
    szValue := Self.cxDBSpinEdit1.EditValue;
    if szValue <> null then
    begin
      s := Self.cxDBSpinEdit1.EditValue;
      EditValue := RoundTo( StrToFloatDef(s , 0 )  , Radix );
    end;
    Editing   := false;
  end;

end;

procedure TfrmBasePurchase.cxButton4Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn8.Editing := False;
end;

procedure TfrmBasePurchase.cxButton6Click(Sender: TObject);
begin
  inherited;
  {
  with Self.tvViewColumn5 do
  begin
    EditValue := Self.cxDBTextEdit3.EditValue;
    Editing   := false;
  end;
  }
  Self.tvViewColumn15.Editing := False;
end;

procedure TfrmBasePurchase.cxButton7Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn15.Editing := False;
end;

procedure TfrmBasePurchase.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  i : Integer;
begin
  inherited;

  i := Self.cxComboBox1.ItemIndex;
  if i >= 0 then
  begin
    Self.cxDBSpinEdit2.EditValue := RebarList[i].Weight;
    Self.cxDBSpinEdit2.PostEditValue;
    Self.cxDBTextEdit3.EditValue := Self.cxComboBox1.Text;
    Self.cxDBTextEdit3.PostEditValue;
  end;

end;

procedure TfrmBasePurchase.cxComboBox2PropertiesCloseUp(Sender: TObject);
begin
  inherited;
//
  GetRadix(Self.cxComboBox2.ItemIndex);
end;

procedure TfrmBasePurchase.cxDBMemo2PropertiesChange(Sender: TObject);
var
  s : Variant;
  szValue : string;
  i : Integer;
  f : Double;
begin
  inherited;

  s := Self.cxDBMemo2.EditingValue;

  if s <> null then
  begin
    szValue := FunExpCalc(s,2);

    if TryStrToInt(szValue,i) or TryStrToFloat(szValue, f) then
    begin

      with Self.Storage do
      begin
        if State <> dsInactive then
        begin
          if (State = dsInsert) or (State = dsEdit) then
          begin

            FieldByName(Self.cxDBSpinEdit3.DataBinding.DataField).Value := szValue;
            FieldByName(Self.cxDBSpinEdit1.DataBinding.DataField).Value := RoundTo( StrToFloatDef( szValue , 0 ) , Radix );

          end;
        end;
      end;

    end;
  end;

end;

procedure TfrmBasePurchase.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;

  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0) , Radix );
    Self.cxDBSpinEdit1.PostEditValue;
  end;

end;

procedure TfrmBasePurchase.cxDBSpinEdit3PropertiesChange(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  szValue := Self.cxDBSpinEdit3.EditingValue;
  if szValue <> null then
  begin
    s := VarToStr(szValue);
    Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0) , Radix ) ;
    Self.cxDBSpinEdit1.PostEditValue;
  end;

end;

procedure TfrmBasePurchase.cxSpinEdit1PropertiesChange(Sender: TObject);
var
  szWeight    : Double;   //重量
  szMeter     : Double;   //米数
  szRootCount : Double;   //根数
  szTheoryWeight : Double; //理论重量
  szPieceCount: Double;   //件数

begin
  inherited;
  szTheoryWeight:= Self.cxDBSpinEdit2.EditingValue;
  szMeter       := Self.cxSpinEdit2.Value;
  szRootCount   := Self.cxSpinEdit5.Value;
  szPieceCount  := Self.cxSpinEdit1.Value;

  if (szRootCount <> 0) then
  begin
    szWeight := (szMeter * (szRootCount * szPieceCount) );

    szWeight := RoundTo( szWeight * szTheoryWeight ,  Radix );
  //  szWeight := RoundTo(szWeight,-3);
    Self.cxDBSpinEdit3.EditValue := szWeight; //计算数值编辑框
    Self.cxDBSpinEdit3.PostEditValue;

    Self.cxDBSpinEdit1.EditValue := szWeight;// szWeight;
    Self.cxDBSpinEdit1.PostEditValue;

  end;

end;

procedure TfrmBasePurchase.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'材料明细表');
end;

procedure TfrmBasePurchase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmBasePurchase.FormCreate(Sender: TObject);
var
  I : Integer;
  ChildFrame : TProjectFrame;
begin
  inherited;
  if (Self.Name = 'frmPurchaseDetailed') or (Self.Name = 'frmStorageManage')  then
  begin
    IsEidt := True;
  end else
  if (Self.Name = 'frmProjectPurchase') then
  begin
    IsEidt := False;
  end;

  Prefix := 'CK';
  Suffix := '00002';
  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);

  SetcxGrid(Self.tvView,0,g_DirBuildStorage);

  Self.cxComboBox1.Clear;
  for I := 0 to High(RebarList) do
  begin
    Self.cxComboBox1.Properties.Items.Add( RebarList[i].Model );
  end;

end;

procedure TfrmBasePurchase.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

end.

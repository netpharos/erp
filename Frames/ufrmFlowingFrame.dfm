object frmFlowingFrame: TfrmFlowingFrame
  Left = 0
  Top = 0
  Width = 275
  Height = 616
  Align = alClient
  TabOrder = 0
  OnClick = FrameClick
  object RzPanel2: TRzPanel
    Left = 0
    Top = 0
    Width = 275
    Height = 616
    Align = alClient
    BorderOuter = fsNone
    BorderSides = [sdRight]
    TabOrder = 0
    object TopSplitter: TSplitter
      Left = 0
      Top = 294
      Width = 275
      Height = 10
      Cursor = crVSplit
      Align = alTop
      Color = 16250613
      ParentColor = False
      ResizeStyle = rsUpdate
      ExplicitTop = 308
      ExplicitWidth = 267
    end
    object KeyTool: TRzToolbar
      Left = 0
      Top = 0
      Width = 275
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer15
        cxLabel3
        cxLookupComboBox2
        RzSpacer51
        RzToolButton12
        RzSpacer13
        Refresh)
      object RzSpacer13: TRzSpacer
        Left = 237
        Top = 2
      end
      object RzSpacer15: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzToolButton12: TRzToolButton
        Left = 212
        Top = 2
        ImageIndex = 10
        OnClick = RzToolButton12Click
      end
      object RzSpacer51: TRzSpacer
        Left = 204
        Top = 2
      end
      object Refresh: TRzToolButton
        Left = 245
        Top = 2
        ImageIndex = 11
        OnClick = RefreshClick
      end
      object cxLabel3: TcxLabel
        Left = 12
        Top = 6
        Caption = #20851#38190#23383#65306
        Transparent = True
      end
      object cxLookupComboBox2: TcxLookupComboBox
        Left = 64
        Top = 4
        RepositoryItem = DM.SignNameBox
        Properties.DropDownWidth = 260
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'SignName'
        Properties.ListColumns = <
          item
            Caption = #24448#26469#21333#20301
            Width = 150
            FieldName = 'SignName'
          end
          item
            Caption = #31616#30721
            Width = 60
            FieldName = 'PYCode'
          end>
        Properties.OnCloseUp = cxLookupComboBox2PropertiesCloseUp
        TabOrder = 1
        Width = 140
      end
    end
    object Tv: TTreeView
      Left = 0
      Top = 29
      Width = 275
      Height = 265
      Align = alTop
      DragMode = dmAutomatic
      HideSelection = False
      Images = DM.TreeImageList
      Indent = 19
      ReadOnly = True
      RowSelect = True
      TabOrder = 1
      OnClick = TvClick
      Items.NodeData = {
        0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        00000000000104805F656755534D4F}
      ExplicitLeft = 3
      ExplicitWidth = 269
    end
    object RzPageControl1: TRzPageControl
      Left = 0
      Top = 304
      Width = 275
      Height = 312
      Hint = ''
      ActivePage = TabSheet1
      Align = alClient
      BoldCurrentTab = True
      ShowCardFrame = False
      TabIndex = 0
      TabOrder = 2
      TabStyle = tsCutCorner
      ExplicitTop = 254
      ExplicitWidth = 267
      ExplicitHeight = 293
      FixedDimension = 20
      object TabSheet1: TRzTabSheet
        Caption = #24448#26469#21333#20301
        ExplicitHeight = 286
        object cxGrid1: TcxGrid
          Left = 0
          Top = 29
          Width = 275
          Height = 263
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 263
          ExplicitHeight = 190
          object tvSignName: TcxGridDBTableView
            OnDblClick = tvSignNameDblClick
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DataCompany
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.DataRowHeight = 23
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 16
            object tvSignNameCol1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvSignNameCol1GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 30
            end
            object tvSignNameCol2: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 118
            end
          end
          object Lv1: TcxGridLevel
            GridView = tvSignName
          end
        end
        object RzToolbar5: TRzToolbar
          Left = 0
          Top = 0
          Width = 275
          Height = 29
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsFlat
          BorderOuter = fsNone
          BorderSides = [sdTop, sdBottom]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 1
          VisualStyle = vsGradient
          ExplicitWidth = 263
          ToolbarControls = (
            RzSpacer26
            cxLabel1
            Search
            RzSpacer27
            RzToolButton9)
          object RzSpacer26: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzSpacer27: TRzSpacer
            Left = 225
            Top = 2
          end
          object RzToolButton9: TRzToolButton
            Left = 233
            Top = 2
            SelectionColorStop = 16119543
            ImageIndex = 10
            OnClick = RzToolButton9Click
          end
          object cxLabel1: TcxLabel
            Left = 12
            Top = 6
            Caption = #24448#26469#21333#20301#65306
            Transparent = True
          end
          object Search: TcxLookupComboBox
            Left = 76
            Top = 4
            RepositoryItem = DM.SignNameBox
            Properties.DropDownWidth = 260
            Properties.ImmediateDropDownWhenActivated = True
            Properties.KeyFieldNames = 'SignName'
            Properties.ListColumns = <
              item
                Caption = #24448#26469#21333#20301
                Width = 150
                FieldName = 'SignName'
              end
              item
                Caption = #31616#30721
                Width = 60
                FieldName = 'PYCode'
              end>
            Properties.OnCloseUp = SearchPropertiesCloseUp
            TabOrder = 1
            OnKeyDown = SearchKeyDown
            Width = 149
          end
        end
      end
      object TabSheet2: TRzTabSheet
        Caption = #20844#21496#21517#31216
        ExplicitLeft = 1
        ExplicitTop = 21
        ExplicitWidth = 263
        ExplicitHeight = 219
        object cxGrid3: TcxGrid
          Left = 0
          Top = 29
          Width = 275
          Height = 263
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 271
          ExplicitHeight = 253
          object tvCompany: TcxGridDBTableView
            OnDblClick = tvCompanyDblClick
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = DM.DataCompany
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.DataRowHeight = 23
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            object tvCompanyColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvSignNameCol1GetDisplayText
              HeaderAlignmentHorz = taCenter
              HeaderAlignmentVert = vaTop
              Options.Filtering = False
              Options.Moving = False
              Options.Sorting = False
              Width = 35
            end
            object tvCompanyColumn2: TcxGridDBColumn
              Caption = #20844#21496#21517#31216
              DataBinding.FieldName = 'SignName'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Moving = False
              Options.Sorting = False
              Width = 211
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = tvCompany
          end
        end
        object RzToolbar1: TRzToolbar
          Left = 0
          Top = 0
          Width = 275
          Height = 29
          AutoStyle = False
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsNone
          BorderSides = [sdBottom]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 1
          VisualStyle = vsGradient
          ExplicitWidth = 263
          ToolbarControls = (
            RzSpacer16
            cxLabel2
            cxLookupComboBox1
            RzSpacer17
            RzToolButton13)
          object RzSpacer16: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzSpacer17: TRzSpacer
            Left = 218
            Top = 2
          end
          object RzToolButton13: TRzToolButton
            Left = 226
            Top = 2
            ImageIndex = 10
            OnClick = RzToolButton13Click
          end
          object cxLabel2: TcxLabel
            Left = 12
            Top = 6
            Caption = #20844#21496#21517#31216#65306
            Transparent = True
          end
          object cxLookupComboBox1: TcxLookupComboBox
            Left = 76
            Top = 4
            RepositoryItem = DM.CompanyList
            Properties.ImmediateDropDownWhenActivated = True
            Properties.ListColumns = <>
            Properties.OnCloseUp = cxLookupComboBox1PropertiesCloseUp
            TabOrder = 1
            OnKeyDown = cxLookupComboBox1KeyDown
            Width = 142
          end
        end
      end
    end
  end
  object DataCompany: TDataSource
    DataSet = ADOCompany
    Left = 112
    Top = 176
  end
  object ADOCompany: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 112
    Top = 120
  end
end

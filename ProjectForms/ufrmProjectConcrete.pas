unit ufrmProjectConcrete;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, Vcl.ExtCtrls, RzPanel, Vcl.ComCtrls, dxCore, cxDateUtils,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxCheckBox, cxCalendar, cxDBLookupComboBox,
  cxSpinEdit, cxCurrencyEdit, cxMemo, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxLabel, RzButton, Data.Win.ADODB, cxDBEdit, cxLookupEdit, cxDBLookupEdit,
  Vcl.StdCtrls,ufrmBaseController, Vcl.Menus, cxCustomPivotGrid, cxPivotGrid;
type
  TCalcValue = record
    dwName : string;
    dwValue: Variant;
  end;
type
  TfrmConcrete = class(TfrmBaseController)
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    RzPanel1: TRzPanel;
    RzToolbar9: TRzToolbar;
    RzSpacer64: TRzSpacer;
    RzToolButton49: TRzToolButton;
    RzSpacer65: TRzSpacer;
    RzToolButton50: TRzToolButton;
    RzSpacer66: TRzSpacer;
    RzToolButton51: TRzToolButton;
    RzSpacer67: TRzSpacer;
    RzToolButton52: TRzToolButton;
    RzToolButton53: TRzToolButton;
    RzToolButton54: TRzToolButton;
    RzSpacer69: TRzSpacer;
    RzSpacer70: TRzSpacer;
    RzSpacer71: TRzSpacer;
    RzSpacer72: TRzSpacer;
    RzToolButton55: TRzToolButton;
    RzSpacer73: TRzSpacer;
    RzSpacer74: TRzSpacer;
    RzSpacer75: TRzSpacer;
    cxLabel44: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel45: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    lv: TcxGridLevel;
    tvViewCol1: TcxGridDBColumn;
    tvViewCol2: TcxGridDBColumn;
    tvViewCol3: TcxGridDBColumn;
    tvViewCol4: TcxGridDBColumn;
    tvViewCol5: TcxGridDBColumn;
    tvViewCol6: TcxGridDBColumn;
    tvViewCol7: TcxGridDBColumn;
    tvViewCol8: TcxGridDBColumn;
    tvViewCol9: TcxGridDBColumn;
    RzPanel3: TRzPanel;
    tvViewCol11: TcxGridDBColumn;
    tvViewCol12: TcxGridDBColumn;
    tvViewCol13: TcxGridDBColumn;
    tvViewCol14: TcxGridDBColumn;
    tvViewColumn1: TcxGridDBColumn;
    qry: TADOQuery;
    ds: TDataSource;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    tvTableView: TcxGridDBTableView;
    tvTableViewCol1: TcxGridDBColumn;
    tvTableViewCol2: TcxGridDBColumn;
    tvTableViewCol3: TcxGridDBColumn;
    tvTableViewCol4: TcxGridDBColumn;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    tvGroupView: TcxGridDBTableView;
    Lv3: TcxGridLevel;
    Grid1: TcxGrid;
    tvGroupViewCol2: TcxGridDBColumn;
    tvGroupViewCol3: TcxGridDBColumn;
    tvGroupViewCol4: TcxGridDBColumn;
    ADOGroup: TADOQuery;
    DataGroup: TDataSource;
    ADOQuery2: TADOQuery;
    DataSource2: TDataSource;
    ADODetail: TADOQuery;
    DataDetail: TDataSource;
    tvGroupViewCol1: TcxGridDBColumn;
    qry0: TADOQuery;
    ds0: TDataSource;
    ADOGroupCaptionType: TStringField;
    ADOGroupspec: TStringField;
    ADOGroupUnitPrice: TCurrencyField;
    ADOGroupNumbers: TStringField;
    ADOGroupCode: TStringField;
    RzSpacer4: TRzSpacer;
    RzToolButton4: TRzToolButton;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RzSpacer5: TRzSpacer;
    RzToolButton5: TRzToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton54Click(Sender: TObject);
    procedure RzToolButton49Click(Sender: TObject);
    procedure tvViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure qryAfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure RzToolButton51Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvViewColumn2PropertiesChange(Sender: TObject);
    procedure RzToolButton52Click(Sender: TObject);
    procedure tvViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton55Click(Sender: TObject);
    procedure tvViewDblClick(Sender: TObject);
    procedure tvViewCol6PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvViewColumn5GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvViewCol8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvGroupViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvTableViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvTableViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvGroupViewCol2GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure RzToolButton1Click(Sender: TObject);
    procedure tvGroupViewCol3PropertiesInitPopup(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure ADOGroupAfterInsert(DataSet: TDataSet);
    procedure tvGroupViewCol3PropertiesCloseUp(Sender: TObject);
    procedure tvGroupViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvGroupViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvGroupViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvGroupViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvViewCol4GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure qryAfterOpen(DataSet: TDataSet);
    procedure tvViewColumn4PropertiesInitPopup(Sender: TObject);
    procedure tvViewColumn4PropertiesCloseUp(Sender: TObject);
    procedure tvGroupViewCol2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure RzToolButton4Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
  private
    { Private declarations }

    g_CalcValue   : array[0..4] of TCalcValue;
    g_ProjectName : string;
    g_PostCode    : string;
    g_RowCode     : string;
    g_GroupMoney  : Currency;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
    
    function CalcSumMoney(lpSumMonry : Currency):Boolean;
    function SetValue(lpColName : string;AQry : TADOQuery;AValue : Variant):BOOL;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
    
  public
    { Public declarations }
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  frmConcrete: TfrmConcrete;

implementation

uses
    uProjectFrame,
    uDataModule,
    global,
    ufunctions,
    ufrmIsViewGrid;

{$R *.dfm}


procedure TfrmConcrete.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;


procedure TfrmConcrete.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
      begin
        Self.qry.Close;
        Self.qry0.Close;
        Self.ADODetail.Close;
      end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;

      s := 'Select *from ' + g_Table_Project_Concrete  + ' Where Code ="' +  g_PostCode + '"';

      with Self.qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
      end;

      with Self.qry0 do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select pt.SignName AS Name,pycode from '+ g_Table_Maintain_Concrete +
                    ' AS PT left join ' + g_Table_CompanyManage +
                    ' AS PS on PT.SignName = PS.SignName WHERE ProjectName="'+ g_ProjectName +'" group by pt.SignName,pycode';
        Open;
      end;
      
      with Self.ADODetail do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select *from ' + g_Table_Project_Concrete_GroupMoney;
        Open;
      end;

    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;
      DeleteTableFile(szTableList,szCodeList);
      DM.InDeleteData(g_Table_Project_Concrete,szCodeList); //清除主目录表内关联
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

function TfrmConcrete.SetValue(lpColName : string;AQry : TADOQuery;AValue : Variant):BOOL;

begin

  with AQry do
  begin
    if State <> dsInactive then
    begin
      Edit;
      FieldByName(lpColName).Value := AValue;

    end;
  end;
end;


procedure TfrmConcrete.ADOGroupAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.tvViewColumn3.DataBinding.FieldName;
      FieldByName(szColName).Value := g_RowCode;
      FieldByName('Code').Value    := g_PostCode;
    end;
  end;
end;

procedure TfrmConcrete.tvGroupViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index +1 );
end;

procedure TfrmConcrete.tvGroupViewCol2GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  with (AProperties as TcxComboBoxProperties) do
  begin
    Items.Clear;

    Items.Add( g_ConcreteArray[0] );
    Items.Add( g_ConcreteArray[1] );
    Items.Add( g_ConcreteArray[2] );
    Items.Add( g_ConcreteArray[3] );
    Items.Add( g_ConcreteArray[4] );

  end;

end;

procedure TfrmConcrete.tvGroupViewCol2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szRowIndex : Integer;
  s : string;
  I: Integer;
  j: Integer;

begin
  inherited;
  szRowIndex := Self.tvGroupView.Controller.FocusedRowIndex;

  for I := 0 to Self.tvGroupView.DataController.RecordCount-1 do
  begin
    if i <> szRowIndex then
    begin
      s := VarToStr( Self.tvGroupView.DataController.Values[i,Self.tvGroupViewCol2.Index] );
      if s = VarToStr(DisplayValue) then
      begin
        for j := 0 to 5 do
        begin
          if j <> 2 then
          begin
              if VarToStr(DisplayValue) = g_ConcreteArray[j] then
              begin
                DisplayValue := null;
                ShowMessage('有相同的类型');
                Exit;
              end;
          end;

        end;

      end;

    end;

  end;
//  Self.tvGroupView.Controller.FocusedRowIndex := szRowIndex;
end;

procedure TfrmConcrete.tvGroupViewCol3PropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  s : Variant;

  szColContentA , szColContentB : Variant;
begin
  inherited;
  szRowIndex := Self.tvGroupView.Controller.FocusedRowIndex;
  s := Self.tvGroupView.DataController.Values[szRowIndex,Self.tvGroupViewCol2.Index];
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szColContentA := DataController.Values[szRowIndex,0]; //规格
      szColContentB := DataController.Values[szRowIndex,1]; //单价
      with Self.ADOGroup do
      begin
        if State <> dsInactive then
        begin
          if (State = dsInsert) or (State = dsEdit) then
          begin
            FieldByName(Self.tvGroupViewCol3.DataBinding.FieldName).Value := szColContentA;
            FieldByName(Self.tvGroupViewCol4.DataBinding.FieldName).Value := szColContentB;
          end;
        end;
          
      end;
      
      if VarToStr(s) = g_ConcreteArray[0] then
      begin
        SetValue(Self.tvViewColumn4.DataBinding.FieldName,Self.qry,szColContentA);
      end;  
      
    end;
    
  end;
  
end;

procedure TfrmConcrete.tvGroupViewCol3PropertiesInitPopup(Sender: TObject);
var
  szCaptionType : Variant;
  szRowIndex : Integer;
  szColName  : string;
  szSignName : string;
  szProjectName : string;

begin
  inherited;
  szRowIndex    := Self.tvGroupView.Controller.FocusedRowIndex;
  szCaptionType := Self.tvGroupView.DataController.Values[szRowIndex,Self.tvGroupViewCol2.Index];
  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
  szSignName    := VarToStr( Self.tvView.DataController.Values[szRowIndex,Self.tvViewCol3.Index] );
  szProjectName := VarToStr( Self.tvView.DataController.Values[szRowIndex,Self.tvViewCol5.Index] );

  szColName := Self.tvViewCol3.DataBinding.FieldName;
  with Self.ADOQuery2 do
  begin
    SQL.Text := 'Select * from ' + g_Table_Maintain_Concrete +
    ' Where CaptionType="' + VarToStr( szCaptionType ) +  '" AND ' +
    szColName + '="' +
    szSignName + '" AND ProjectName="' + szProjectName + '"';
    Open;
  end;
end;

procedure TfrmConcrete.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmConcrete.FormCreate(Sender: TObject);
var
  ChildFrame : TProjectFrame;
begin
  inherited;
  Prefix := 'SH';
  Suffix := '00008';
  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  SetcxGrid(Self.tvView,0,g_Dir_Project_Concrete);
end;

procedure TfrmConcrete.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then  Close;
end;

procedure TfrmConcrete.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '项目名称:' + g_ProjectName;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmConcrete.qryAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName('Code').Value := g_PostCode;
      szColName := Self.tvViewCol2.DataBinding.FieldName;
      FieldByName(szColName).Value := Date;
      szColName := Self.tvViewCol5.DataBinding.FieldName;
      FieldByName(szColName).Value := g_ProjectName;
      szColName := Self.tvViewColumn2.DataBinding.FieldName;
      FieldByName(szColName).Value := True;
      szColName := Self.tvViewCol11.DataBinding.FieldName;
      FieldByName(szColName).Value := Now;
      szColName := Self.tvViewCol12.DataBinding.FieldName;
      FieldByName(szColName).Value := Now;

      szColName := Self.tvViewColumn3.DataBinding.FieldName;
      szCode := Prefix + GetRowCode(g_Table_Project_Concrete,
                                    Self.tvViewColumn3.DataBinding.FieldName,
                                    Suffix,
                                    80000);
      FieldByName( szColName ).Value := szCode;

    end;
  end;
end;

procedure TfrmConcrete.qryAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmConcrete.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  with Self.ADOGroup do
  begin
    if State <> dsInactive then
    begin
      Append;
      Self.tvGroupViewCol2.FocusWithSelection;
      Self.Grid1.SetFocus;
      keybd_event(VK_RETURN,0,0,0);
    end;
  end;
end;

procedure TfrmConcrete.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  Self.ADOGroup.UpdateBatch(arAll);
end;

procedure TfrmConcrete.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  Self.tvGroupView.Controller.DeleteSelection;
end;

procedure TfrmConcrete.RzToolButton49Click(Sender: TObject);
begin
  if Sender = RzToolButton49 then
  begin
    with Self.qry do
    begin
      if State <> dsInactive then
      begin
        Append;
        Self.tvViewCol3.FocusWithSelection;
        Self.Grid.SetFocus;
        keybd_event(VK_RETURN,0,0,0);
      end;
    end;
  end else
  if Sender = RzToolButton50 then
  begin
    IsDeleteEmptyData(Self.qry, g_Table_Project_Concrete , Self.tvViewCol3.DataBinding.FieldName);
    IsSave := False;
  end;

end;

procedure TfrmConcrete.RzToolButton4Click(Sender: TObject);
begin
  inherited;
  Self.tvViewColumn4.Editing := False;
end;

procedure TfrmConcrete.RzToolButton51Click(Sender: TObject);
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  adoTmp : TADOQuery;
  szCount : Integer;
  szRowsCount  : string;
  szIndex : Integer;
  s : Variant ;
  szColName : string;

begin
  szColName := Self.tvViewColumn3.DataBinding.FieldName;
  szCount := Self.tvView.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount > 0 then
  begin
      if Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin

        with Self.tvView  do
        begin
          if not DataController.DataSource.DataSet.IsEmpty then
          begin
            str := '';

            for I := szCount -1 downto 0 do
            begin

              szIndex := GetColumnByFieldName( szColName ).Index;
              s := Controller.SelectedRows[i].Values[szIndex];
              Id := VarToStr(s);

              if Length(id) <> 0 then
              begin
                dir := Concat(g_Resources,'\'+ g_Dir_Project_Concrete);
                if Length(dir) <> 0 then
                begin
                  dir := ConcatEnclosure(Dir, id);
                  if DirectoryExists(dir) then
                  begin
                    DeleteDirectory(dir);
                  end;

                end;

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;
              end;

            end;
            Controller.DeleteSelection;

            DM.ADOconn.BeginTrans; //开始事务
            try
              with DM.Qry do
              begin
                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_Project_Concrete +' where '+ szColName +' in("' + str + '")';
                ExecSQL;
                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_Project_Concrete_GroupMoney +' where '+ szColName +' in("' + str + '")';
                ExecSQL;
              end;
              DM.ADOconn.Committrans; //提交事务
            except
              on E:Exception do
              begin
                DM.ADOconn.RollbackTrans;           // 事务回滚
              end;
            end;

          end;

        end;

      end;
  end;

end;

procedure TfrmConcrete.RzToolButton52Click(Sender: TObject);
var
  child : TfrmIsViewGrid;
begin
  inherited;
  //表格设置
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_Dir_Project_Concrete;
    Child.ShowModal;
    SetcxGrid(Self.tvView,0, g_Dir_Project_Concrete);
  finally
    Child.Free;
  end;

end;

procedure TfrmConcrete.RzToolButton54Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmConcrete.RzToolButton55Click(Sender: TObject);
var
  sqltext : string;
  szColName : string;
begin
  inherited;
  szColName   := Self.tvViewCol2.DataBinding.FieldName;
  sqltext := 'select *from '  + g_Table_Project_Concrete +
             ' where Code= "' + g_PostCode +
             ' " and ' + szColName + ' between :t1 and :t2';
  SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.qry);
end;

procedure TfrmConcrete.RzToolButton5Click(Sender: TObject);
begin
  inherited;
  {
  if Self.tvViewColumn2.EditValue then
  begin
    with Self.qry do
    begin
      if State <> dsInactive then
      begin
        if (State <> dsEdit) and (State <> dsInsert) then
           Edit;
      end;
    end;

  end else
  begin
    Application.MessageBox( '冻结状态下不可以编辑！', '提示:', MB_OK + MB_ICONWARNING)
  end;
  }
  ADOIsEdit( Self.tvViewColumn2.EditValue , Self.qry)

end;

procedure TfrmConcrete.tvGroupViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmConcrete.tvGroupViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  if AItem.Index = Self.tvGroupViewCol1.Index then
  begin
    AAllow := False;
  end;  
end;

procedure TfrmConcrete.tvGroupViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if ( Key = 13 ) and (AItem.Index = Self.tvGroupViewCol4.Index) then
  begin
    if Self.tvGroupView.Controller.FocusedRow.IsLast then
    begin
      with Self.ADOGroup do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvGroupView.Columns[0].Focused := True;
        end;  
      end;  
    end;
  end;  
end;

procedure TfrmConcrete.tvGroupViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
  
begin
  inherited;
  try
    s := VarToStr(AValue);
    if Length(s) <> 0 then
    begin
      g_GroupMoney := StrToCurrDef(s,0);
    end;  
  except
  end;
end;

procedure TfrmConcrete.tvTableViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmConcrete.tvTableViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := False;
end;

procedure TfrmConcrete.tvViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := IntToStr(ARecord.Index +1) ;
end;

procedure TfrmConcrete.tvViewCol4GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmConcrete.tvViewCol6PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szUnitPrice : Variant;
  szRowIndex : Integer;
  szSumMoney : Variant;
  i : Variant;

begin
  inherited;
  szRowIndex := Self.tvView.DataController.FocusedRowIndex;
  szUnitPrice:= Self.tvView.DataController.Values[szRowIndex,Self.tvViewCol7.Index];
  szSumMoney := szUnitPrice * DisplayValue;
  if szSumMoney <> null then
  begin
    CalcSumMoney(szSumMoney); //总额
    SetValue(Self.tvViewCol6.DataBinding.FieldName,Self.qry,DisplayValue);//方量
  end;
end;

function TfrmConcrete.CalcSumMoney( lpSumMonry : Currency):Boolean;
begin
  inherited;
  with Self.qry do
  begin
    if State <> dsInactive then
    begin
      if (State = dsInsert) or (State = dsEdit) then
      begin
        FieldByName(Self.tvViewCol8.DataBinding.FieldName).Value := lpSumMonry;
        FieldByName(Self.tvViewColumn6.DataBinding.FieldName).Value := MoneyConvert(lpSumMonry);
      end;  
      
    end;
  end;

end;

procedure TfrmConcrete.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'商混明细表');
end;

procedure TfrmConcrete.tvViewCol8PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szSumMonry : Currency;

begin
  inherited;
  szSumMonry := StrToCurr(VarToStr(DisplayValue));
  CalcSumMoney(szSumMonry);
end;

procedure TfrmConcrete.tvViewColumn2PropertiesChange(Sender: TObject);
var
  szRowIndex : Integer;

begin
  inherited;
  szRowIndex := Self.tvView.Controller.FocusedRowIndex;
  Self.qry.UpdateBatch(arAll);
  Self.qry.Requery();
  Self.tvView.DataController.FocusedRowIndex := szRowIndex;
end;

procedure TfrmConcrete.tvViewColumn4PropertiesCloseUp(Sender: TObject);
var
  szColName : string;
  szUnitPrice : Variant;
  szRowIndex : Integer;
  szSumMoney : Variant;
  szGroupMoney:Variant;

begin
  inherited;
  szGroupMoney := g_GroupMoney;
  SetValue(Self.tvViewCol7.DataBinding.FieldName,Self.qry,szGroupMoney);//单价
  szRowIndex := Self.tvView.DataController.FocusedRowIndex;
  szUnitPrice:= Self.tvView.DataController.Values[szRowIndex,Self.tvViewCol6.Index];
  szSumMoney := szUnitPrice * szGroupMoney;
  if szSumMoney <> null then
  begin
    CalcSumMoney(szSumMoney); //总额
    Self.ADODetail.Requery();
    Self.ADOGroup.Requery();
  end;

end;

procedure TfrmConcrete.tvViewColumn4PropertiesInitPopup(Sender: TObject);
var
  szColName : string;
  szSignName : string;
  szProjectName : string;

  szRowIndex : Integer;
  s , s1: string;


begin
  inherited;
  szRowIndex := Self.tvView.DataController.FocusedRecordIndex;
  szSignName := VarToStr( Self.tvView.DataController.Values[szRowIndex,Self.tvViewCol3.Index] );
  g_RowCode  := VarToStr( Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn3.Index]);

  szProjectName := VarToStr( Self.tvView.DataController.Values[szRowIndex,Self.tvViewCol5.Index] );

  if Length(szSignName) = 0 then
  begin
    ShowMessage('没有选择供应单位，无法计算出合同单价');
  end;

  szColName := Self.tvViewColumn3.DataBinding.FieldName;
  with Self.ADOGroup do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Project_Concrete_GroupMoney +
                ' Where '+ szColName +'="' + g_RowCode + ' "';
    Open;
  end;

  szColName := Self.tvViewCol3.DataBinding.FieldName;
  with Self.ADOQuery2 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Maintain_Concrete + ' Where ' +
                szColName + '="' +
                szSignName + '" AND ProjectName="' + szProjectName + '"';
    Open;
  end;

end;

procedure TfrmConcrete.tvViewColumn5GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmConcrete.tvViewColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvView,1,g_Dir_Project_Concrete);
end;

procedure TfrmConcrete.tvViewDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources,'\'+ g_Dir_Project_Concrete);
  CreateEnclosure(Self.tvView.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmConcrete.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  with Self.qry do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) and (State <> dsInsert) then
         AAllow := False
      else
         AAllow := True;
    end;
  end;

  if (AItem.Index = Self.tvViewCol1.Index) or
     (AItem.Index = Self.tvViewColumn3.Index) then
  begin
    AAllow := False;
  end;

end;

procedure TfrmConcrete.tvViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvViewCol8.Index) then
  begin
    if Self.tvView.Controller.FocusedRow.IsLast then
    begin
      if IsNewRow then
      begin
        with Self.qry do
        begin
          if State <> dsInactive then
          begin
            Append;
            Self.tvViewCol2.FocusWithSelection;
          end;  
        end;  
      end;  
    end;  
  end;
end;

procedure TfrmConcrete.tvViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvViewColumn2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmConcrete.tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      Caption := '';
    end;
    Self.tvViewColumn6.Summary.FooterFormat := szCapital;
  except
  end;

end;

end.

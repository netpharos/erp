unit ufrmProjectSanction;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls, RzButton,
  RzPanel, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCheckBox, cxCalendar,
  cxTextEdit, cxCurrencyEdit, cxMemo, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  cxMaskEdit, cxDropDownEdit, cxLabel, Data.Win.ADODB,ufrmBaseController,
  cxFontNameComboBox, cxDBLookupComboBox, Vcl.Menus;

type
  TfrmProjectSanction = class(TfrmBaseController)
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    RzPanel1: TRzPanel;
    RzToolbar3: TRzToolbar;
    RzSpacer13: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer15: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzSpacer16: TRzSpacer;
    RzToolButton11: TRzToolButton;
    RzToolButton12: TRzToolButton;
    RzSpacer17: TRzSpacer;
    btnClose: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzSpacer19: TRzSpacer;
    RzSpacer31: TRzSpacer;
    RzSpacer32: TRzSpacer;
    RzToolButton24: TRzToolButton;
    cxLabel17: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel37: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    Grid: TcxGrid;
    tvSanctionView: TcxGridDBTableView;
    tvSanctionView1: TcxGridDBColumn;
    tvSanctionViewColumn3: TcxGridDBColumn;
    tvSanctionView2: TcxGridDBColumn;
    tvSanctionView3: TcxGridDBColumn;
    tvSanctionView4: TcxGridDBColumn;
    tvSanctionView5: TcxGridDBColumn;
    tvSanctionView6: TcxGridDBColumn;
    tvSanctionView7: TcxGridDBColumn;
    tvSanctionView8: TcxGridDBColumn;
    tvSanctionView9: TcxGridDBColumn;
    tvSanctionViewColumn1: TcxGridDBColumn;
    tvSanctionView10: TcxGridDBColumn;
    tvSanctionView11: TcxGridDBColumn;
    tvSanctionView12: TcxGridDBColumn;
    tvSanctionView13: TcxGridDBColumn;
    tvSanctionView14: TcxGridDBColumn;
    tvSanctionViewColumn2: TcxGridDBColumn;
    Lv: TcxGridLevel;
    ADOSanction: TADOQuery;
    DATASanction: TDataSource;
    ADOSignName: TADOQuery;
    DataSignName: TDataSource;
    tvSanctionViewColumn4: TcxGridDBColumn;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    tvSanctionViewColumn5: TcxGridDBColumn;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton8Click(Sender: TObject);
    procedure RzToolButton11Click(Sender: TObject);
    procedure tvSanctionViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton24Click(Sender: TObject);
    procedure tvSanctionView1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvSanctionViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvSanctionViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvSanctionViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvSanctionViewStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure tvSanctionViewColumn3PropertiesChange(Sender: TObject);
    procedure ADOSanctionAfterInsert(DataSet: TDataSet);
    procedure tvSanctionView9GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvSanctionViewDblClick(Sender: TObject);
    procedure tvSanctionViewColumn2GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvSanctionViewColumn4GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvSanctionView8GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvSanctionViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure ADOSanctionAfterOpen(DataSet: TDataSet);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    g_ProjectName : string;
    g_PostCode    : string;
    g_BuildStorage: string;

    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  public
    { Public declarations }
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  frmProjectSanction: TfrmProjectSanction;
  g_DirSanction : string = 'ProjectSanction';

implementation

uses
   uProjectFrame,uDataModule,global,ufrmIsViewGrid,ufunctions;

{$R *.dfm}

procedure TfrmProjectSanction.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
      begin
        Self.ADOSanction.Close;
        Self.ADOSignName.Close;
      end;
    WM_FrameView :
      begin
        pMsg := PProject(Message.LParam);
        g_PostCode := pMsg.dwCode;
        g_ProjectName := pMsg.dwName;

        s := 'Select * from ' + g_Table_Project_Sanction + ' Where Code ="' +  g_PostCode + '"';
        with Self.ADOSanction do
        begin
          Close;
          SQL.Clear;
          SQL.Text := s;
          Open;
        end;

        with Self.ADOSignName do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from ' + g_Table_CompanyManage;
          Open;
        end;

      end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;
      DeleteTableFile(szTableList,szCodeList);
      DM.InDeleteData(g_Table_Project_Mechanics,szCodeList); //清除主目录表内关联
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmProjectSanction.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmProjectSanction.tvSanctionView1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmProjectSanction.tvSanctionView8GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from ' + g_Table_Maintain_section;
  DM.Qry.Open;
  if DM.Qry.RecordCount <> 0 then
  begin
    with (AProperties AS TcxComboBoxProperties) do
    begin
      Items.Clear;
      while Not DM.Qry.Eof do
      begin
        Items.Add(DM.Qry.Fields[1].AsString);
        DM.Qry.Next;

      end;

    end;

  end;
end;

procedure TfrmProjectSanction.tvSanctionView9GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('奖励');
    Items.Add('扣罚');
  end;
end;

procedure TfrmProjectSanction.tvSanctionViewColumn2GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmProjectSanction.tvSanctionViewColumn3PropertiesChange(
  Sender: TObject);
var
  szRowIndex : Integer;
begin
  inherited;
  szRowIndex := Self.tvSanctionView.DataController.FocusedRecordIndex;
  Self.ADOSanction.UpdateBatch();
  Self.ADOSanction.Requery();
  Self.tvSanctionView.Controller.FocusedRowIndex := szRowIndex;
end;

procedure TfrmProjectSanction.tvSanctionViewColumn4GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from ' + g_Table_Maintain_WorkType;
  DM.Qry.Open;
  if DM.Qry.RecordCount <> 0 then
  begin
    with (AProperties AS TcxComboBoxProperties) do
    begin
      Items.Clear;
      while Not DM.Qry.Eof do
      begin
        Items.Add(DM.Qry.Fields[1].AsString);
        DM.Qry.Next;

      end;

    end;

  end;

end;

procedure TfrmProjectSanction.tvSanctionViewColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  SetcxGrid(Self.tvSanctionView,1,g_DirSanction);
end;

procedure TfrmProjectSanction.tvSanctionViewDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources,'\'+ g_DirSanction);
  CreateEnclosure(Self.tvSanctionView.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmProjectSanction.tvSanctionViewEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  with Self.ADOSanction do
  begin
    if State <> dsInactive then
      begin
        if (State <> dsEdit) and ( State <> dsInsert ) then
        begin
          AAllow := False;
        end else
        begin
          AAllow := True;
        end;
        {
        if FieldByName(szColName).Value = False then
        begin
          AAllow := False;
        end else
        begin
          AAllow := True;
        end;
        }
      end;
  end;
  {
  szColName := Self.tvSanctionViewColumn3.DataBinding.FieldName;
  if AItem.Index <> Self.tvSanctionViewColumn3.Index then
  begin

  end;
  }
  if (AItem.Index = Self.tvSanctionView1.Index) or
     (AItem.Index = Self.tvSanctionView2.Index) then
  begin
    AAllow := False;
  end;
  
end;
procedure TfrmProjectSanction.tvSanctionViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
var
  szValue : Currency;
  szColName : string;

begin
  inherited;
  if Key = 13 then
  begin

    if AItem.Index = Self.tvSanctionView12.Index then
    begin
      if AItem.Editing then
      begin
        AItem.DataBinding.DataController.UpdateData;
        szColName := Self.tvSanctionView12.DataBinding.FieldName;
        with Self.ADOSanction do
        begin
          if State <> dsInactive then
          begin
            szValue := FieldByName(szColName).AsCurrency;
            szColName := Self.tvSanctionView13.DataBinding.FieldName;
          //  Edit;
            FieldByName(szColName).Value := MoneyConvert(szValue);
          //  Post;
          //  Refresh;
          end;
        end;
      end;

      if IsSaveData(self.tvSanctionView,self.tvSanctionView12.index,self.ADOSanction) then
      begin
        //可以换行
        if IsNewRow then
        begin
          Self.RzToolButton8.Click;
        end;
      end;
    end;
  end;

end;

procedure TfrmProjectSanction.tvSanctionViewKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = 13) or (Key = 9) then
  begin
    if IsSaveData(self.tvSanctionView,self.tvSanctionView12.index,Self.ADOSanction) then
    begin
      //可以换行
      if IsNewRow then
      begin
        Self.RzToolButton8.Click;
      end;
    end;
  end;

  case Key of
    46:
    begin
      Self.RzToolButton10.Click;
    end;

  end;
end;

procedure TfrmProjectSanction.tvSanctionViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvSanctionViewColumn3.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmProjectSanction.tvSanctionViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.tvSanctionView13.Summary.FooterFormat := szCapital;
  except
  end;

end;

procedure TfrmProjectSanction.ADOSanctionAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName('Code').Value := g_PostCode;
      szColName := Self.tvSanctionView2.DataBinding.FieldName;
      szCode := Prefix + GetRowCode(g_Table_Project_Sanction,szColName,Suffix,70000);
      FieldByName(szColName).Value := szCode;
      szColName:= Self.tvSanctionView3.DataBinding.FieldName;
      FieldByName(szColName).Value := Date;
      szColName := Self.tvSanctionView4.DataBinding.FieldName;
      FieldByName(szColName).Value := g_ProjectName;
      szColName := Self.tvSanctionViewColumn3.DataBinding.FieldName;
      FieldByName(szColName).Value := True;
    end;

  end;

end;

procedure TfrmProjectSanction.ADOSanctionAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectSanction.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmProjectSanction.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'奖罚明细表');
end;

procedure TfrmProjectSanction.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmProjectSanction.FormCreate(Sender: TObject);
var
  ChildFrame : TProjectFrame;
begin
  inherited;
  Prefix := 'JF';
  Suffix := '00007';
  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);
  SetcxGrid(Self.tvSanctionView,0,g_DirSanction);
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
end;

procedure TfrmProjectSanction.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Self.btnClose.Click;
end;

procedure TfrmProjectSanction.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmProjectSanction.RzToolButton11Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_DirSanction;
    Child.ShowModal;
    SetcxGrid(Self.tvSanctionView,0,g_DirSanction);
  finally
    Child.Free;
  end;
end;

procedure TfrmProjectSanction.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  {
  if  then
  begin
    with  do
    begin
      if State <> dsInactive then
        begin
          if (State <> dsEdit) and ( State <> dsInsert ) then
             Edit;
        end;
    end;
  end else
  begin
    Application.MessageBox( '冻结状态下不可以编辑！', '提示:', MB_OK + MB_ICONWARNING)
  end;
  }
  ADOIsEdit( tvSanctionViewColumn3.EditValue , Self.ADOSanction)
end;

procedure TfrmProjectSanction.RzToolButton24Click(Sender: TObject);
var
  szColName : string;
  sqltext : string;

begin
  inherited;
  szColName := Self.tvSanctionView3.DataBinding.FieldName;
  sqltext := 'select *from ' + g_Table_Project_Sanction + ' where Code= "' + g_PostCode + ' " and ' + szColName +' between :t1 and :t2';
  SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ADOSanction);
end;

procedure TfrmProjectSanction.RzToolButton8Click(Sender: TObject);
var
//  Child : TfrmProjectWorksheet;
  szName: string;
  szCount : Integer;
  szIndex : Integer;
  szRecordCount : Integer;
  i : Integer;
  str: string;

begin
  inherited;
  with Self.ADOSanction do
  begin
    if Sender = Self.RzToolButton8 then
    begin
      if State = dsInactive then
      begin
        Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
      end else
      begin
      //  Append;
        First;
        Insert;
        Self.tvSanctionView7.FocusWithSelection;
        Self.Grid.SetFocus;
      end;

    end else
    if Sender = Self.RzToolButton10 then
    begin
      DeleteSelection(Self.tvSanctionView,'',g_Table_Project_Sanction,
      Self.tvSanctionView2.DataBinding.FieldName);
      Self.ADOSanction.Requery();
    end else
    if Sender = Self.RzToolButton9 then
    begin
      IsDeleteEmptyData(Self.ADOSanction ,
                    g_Table_Project_Sanction ,
                    Self.tvSanctionView7.DataBinding.FieldName);
      IsSave := False;
    end;
  end;
end;


end.

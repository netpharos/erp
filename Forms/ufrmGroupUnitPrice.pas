unit ufrmGroupUnitPrice;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, RzButton, RzPanel, Vcl.ExtCtrls,UnitADO,
  Datasnap.DBClient,ufrmBaseController, cxDropDownEdit, dxmdaset, Data.Win.ADODB,
  cxContainer, cxTextEdit, cxCurrencyEdit, cxDBEdit, cxDBLookupComboBox;

type
  TfrmGroupUnitPrice = class(TfrmBaseController)
    RzToolbar1: TRzToolbar;
    RzSpacer8: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton11: TRzToolButton;
    RzSpacer11: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer12: TRzSpacer;
    RzToolButton13: TRzToolButton;
    Grid: TcxGrid;
    tView: TcxGridDBTableView;
    tViewColumn1: TcxGridDBColumn;
    tViewColumn2: TcxGridDBColumn;
    tViewColumn3: TcxGridDBColumn;
    tViewColumn4: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    ds: TDataSource;
    tViewColumn5: TcxGridDBColumn;
    ds1: TDataSource;
    ds2: TClientDataSet;
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton9Click(Sender: TObject);
    procedure tViewColumn2GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton12Click(Sender: TObject);
    procedure dxMemData1AfterInsert(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure FormActivate(Sender: TObject);
    procedure tViewColumn3PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton11Click(Sender: TObject);
  private
    { Private declarations }
    dwADO : TADO;
    dwSQLText : string;
    dwCategory: string;
  public
    { Public declarations }
    dwTableName : string;
    dwSumMoney  : Variant;
    dwSignName  : Variant;
    dwParentId  : Variant;
    dwModuleIndex : Integer;
  end;

var
  frmGroupUnitPrice: TfrmGroupUnitPrice;

implementation

uses
   global,uDataModule;

{$R *.dfm}

procedure TfrmGroupUnitPrice.dxMemData1AfterInsert(DataSet: TDataSet);
begin
  Self.tViewColumn5.EditValue := dwParentId;
end;

procedure TfrmGroupUnitPrice.FormActivate(Sender: TObject);
var
  s : string;
begin
  inherited;
  dwCategory := '组合辅材';
  s := 'SELECT A.NoId,A.SignName,A.ModuleIndex,A.status,A.InputDate,' +
       ' B.GoodsName,B.Spec,B.UnitPrice' +
       ' FROM ' +
       g_Table_Company_Storage_ContractList + ' as A '+
       ' LEFT JOIN ' + g_Table_Company_Storage_ContractDetailed +' AS B ON (A.NoId = B.NoId) ' +
       ' Where A.SignName="' + dwSignName +
       '" AND A.ModuleIndex=' + IntToStr(dwModuleIndex) +
       ' AND A.status=yes and Category="' + dwCategory + '"';;
  dwADO.OpenSQL(Self.ds2,s);
end;

procedure TfrmGroupUnitPrice.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  Self.RzToolButton12.Click;
end;

procedure TfrmGroupUnitPrice.RzToolButton11Click(Sender: TObject);
begin
  inherited;
  with Self.tView.DataController.DataSet do
  begin
    if State in [dsEdit,dsInsert] then Delete;
  end;
end;

procedure TfrmGroupUnitPrice.RzToolButton12Click(Sender: TObject);
begin
  with Self.tView.DataController.DataSet do
  begin
    if State in [dsEdit,dsInsert] then Post;
    First;
    while Not Eof do
    begin
      if null = FieldByName(tViewColumn2.DataBinding.FieldName).Value then
      begin
        Delete;
        continue;
      end;
      Next;
    end;
  end;
end;

procedure TfrmGroupUnitPrice.RzToolButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmGroupUnitPrice.RzToolButton9Click(Sender: TObject);
begin
  Self.tView.DataController.DataSet.Append;
end;

procedure TfrmGroupUnitPrice.tViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index  +1 );
end;

procedure TfrmGroupUnitPrice.tViewColumn2GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  {
  with (AProperties as TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add( g_ConcreteArray[0] );
    Items.Add( g_ConcreteArray[1] );
    Items.Add( g_ConcreteArray[2] );
    Items.Add( g_ConcreteArray[3] );
    Items.Add( g_ConcreteArray[4] );
  end;
  }
end;

procedure TfrmGroupUnitPrice.tViewColumn3PropertiesCloseUp(Sender: TObject);
var
  szSpec : Variant;
  szRowIndex   : Integer;
begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      Self.tViewColumn3.EditValue := DataController.Values[szRowIndex,1] ; //规格
      Self.tViewColumn4.EditValue := DataController.Values[szRowIndex,2] ; //单价
    end;
  end;
end;

procedure TfrmGroupUnitPrice.tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  if AValue <> null then  dwSumMoney := AValue;
end;

end.

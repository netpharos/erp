unit ufrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, RzTabs,Vcl.Imaging.jpeg,StdCtrls,
  ImgList, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, cxClasses, dxRibbon,
  dxSkinsdxBarPainter, dxBar, dxSkinsdxStatusBarPainter, dxStatusBar,dxRibbonForm,
  dxRibbonStatusBar, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsForm, dxSkinOffice2007Silver, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, dxRibbonSkins,
  dxSkinsdxRibbonPainter, dxRibbonCustomizationForm, dxBarExtItems,
  cxLocalization,System.IniFiles, System.ImageList, Vcl.AppEvnts, cxCustomData,
  cxStyles, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer;

type
  TDrawMode = (dwCenter, dwStretch, dwTile);
  TfrmMain = class(TDxRibbonForm)
    dxBarManager1: TdxBarManager;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxbrManager1Bar: TdxBar;
    dxBarManager1Bar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    il: TcxImageList;
    cxmgclctn1: TcxImageCollection;
    dxBarLargeButton14: TdxBarLargeButton;
    dlgOpen: TOpenDialog;
    dxRibbon1Tab6: TdxRibbonTab;
    dxBarManager1Bar9: TdxBar;
    dxBarManager1Bar10: TdxBar;
    dxBarLargeButton12: TdxBarLargeButton;
    ApplicationEvents1: TApplicationEvents;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarManager1Bar12: TdxBar;
    dxBarLargeButton26: TdxBarLargeButton;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarLargeButton32: TdxBarLargeButton;
    dxBarLargeButton33: TdxBarLargeButton;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton36: TdxBarLargeButton;
    dxBarLargeButton37: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarButton11: TdxBarButton;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton40: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton24: TdxBarButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton27: TdxBarButton;
    dxBarButton26: TdxBarButton;
    dxBarSubItem7: TdxBarSubItem;
    dxBarSubItem9: TdxBarSubItem;
    dxBarButton38: TdxBarButton;
    dxBarButton39: TdxBarButton;
    dxBarButton41: TdxBarButton;
    dxRibbon1Tab3: TdxRibbonTab;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton30: TdxBarLargeButton;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton46: TdxBarLargeButton;
    dxBarLargeButton47: TdxBarLargeButton;
    dxBarLargeButton44: TdxBarLargeButton;
    dxBarLargeButton48: TdxBarLargeButton;
    dxBarButton35: TdxBarButton;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton13: TdxBarButton;
    dxBarButton45: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarSubItem17: TdxBarSubItem;
    dxBarButton12: TdxBarButton;
    dxBarButton49: TdxBarButton;
    dxBarSubItem18: TdxBarSubItem;
    dxBarButton50: TdxBarButton;
    dxBarButton51: TdxBarButton;
    dxBarButton52: TdxBarButton;
    dxBarButton21: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton30: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton29: TdxBarButton;
    dxBarSubItem8: TdxBarSubItem;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    dxBarButton32: TdxBarButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarSubItem5: TdxBarSubItem;
    dxBarSeparator1: TdxBarSeparator;
    dxBarButton20: TdxBarButton;
    dxBarButton22: TdxBarButton;
    dxBarSeparator2: TdxBarSeparator;
    dxBarSubItem12: TdxBarSubItem;
    dxBarButton23: TdxBarButton;
    dxBarButton25: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton16: TdxBarButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxRibbon1Tab4: TdxRibbonTab;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarSubItem6: TdxBarSubItem;
    dxBarButton19: TdxBarButton;
    dxBarButton28: TdxBarButton;
    dxBarSubItem10: TdxBarSubItem;
    dxBarButton31: TdxBarButton;
    dxBarButton33: TdxBarButton;
    dxBarButton34: TdxBarButton;
    dxBarButton36: TdxBarButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarSeparator3: TdxBarSeparator;
    dxBarButton37: TdxBarButton;
    dxBarButton42: TdxBarButton;
    dxBarSeparator4: TdxBarSeparator;
    dxBarSeparator5: TdxBarSeparator;
    dxBarSeparator6: TdxBarSeparator;
    dxBarSeparator7: TdxBarSeparator;
    dxBarButton40: TdxBarButton;
    dxBarSeparator8: TdxBarSeparator;
    dxBarSeparator9: TdxBarSeparator;
    dxBarSeparator10: TdxBarSeparator;
    dxBarButton43: TdxBarButton;
    dxBarButton44: TdxBarButton;
    dxBarSeparator11: TdxBarSeparator;
    dxBarSeparator12: TdxBarSeparator;
    dxBarSeparator13: TdxBarSeparator;
    dxBarButton17: TdxBarButton;
    dxBarButton18: TdxBarButton;
    dxBarButton46: TdxBarButton;
    dxBarButton47: TdxBarButton;
    dxBarButton48: TdxBarButton;
    dxBarSubItem11: TdxBarSubItem;
    dxBarButton53: TdxBarButton;
    dxBarButton54: TdxBarButton;
    dxBarButton55: TdxBarButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure N6Click(Sender: TObject);
    procedure dxBarButton1Click(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure dxBarLargeButton6Click(Sender: TObject);
    procedure dxBarLargeButton7Click(Sender: TObject);
    procedure dxBarLargeButton8Click(Sender: TObject);
    procedure dxBarLargeButton9Click(Sender: TObject);
    procedure dxBarLargeButton14Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure dxBarLargeButton10Click(Sender: TObject);
    procedure dxBarLargeButton11Click(Sender: TObject);
    procedure dxBarLargeButton1Click(Sender: TObject);
    procedure dxBarLargeButton13Click(Sender: TObject);
    procedure dxBarLargeButton35Click(Sender: TObject);
    procedure dxBarLargeButton36Click(Sender: TObject);
    procedure dxBarLargeButton37Click(Sender: TObject);
    procedure dxBarLargeButton38Click(Sender: TObject);
    procedure dxBarButton7Click(Sender: TObject);
    procedure dxBarLargeButton3Click(Sender: TObject);
    procedure dxBarButton6Click(Sender: TObject);
    procedure dxBarLargeButton19Click(Sender: TObject);
    procedure dxBarButton11Click(Sender: TObject);
    procedure dxBarLargeButton24Click(Sender: TObject);
    procedure dxBarLargeButton40Click(Sender: TObject);
    procedure dxBarLargeButton25Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure dxBarButton27Click(Sender: TObject);
    procedure dxBarLargeButton47Click(Sender: TObject);
    procedure dxBarButton8Click(Sender: TObject);
    procedure dxBarButton13Click(Sender: TObject);
    procedure dxBarButton14Click(Sender: TObject);
    procedure dxBarButton16Click(Sender: TObject);
    procedure dxBarLargeButton44Click(Sender: TObject);
    procedure dxBarLargeButton30Click(Sender: TObject);
    procedure dxBarLargeButton48Click(Sender: TObject);
    procedure dxBarButton45Click(Sender: TObject);
    procedure dxBarButton5Click(Sender: TObject);
    procedure dxBarButton12Click(Sender: TObject);
    procedure dxBarButton30Click(Sender: TObject);
    procedure dxBarButton29Click(Sender: TObject);
    procedure dxBarButton9Click(Sender: TObject);
    procedure dxBarButton21Click(Sender: TObject);
    procedure dxBarButton15Click(Sender: TObject);
    procedure dxBarButton32Click(Sender: TObject);
    procedure dxBarButton3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure dxBarLargeButton4Click(Sender: TObject);
    procedure dxBarLargeButton17Click(Sender: TObject);
    procedure dxBarButton4Click(Sender: TObject);
    procedure dxBarButton10Click(Sender: TObject);
    procedure dxBarButton23Click(Sender: TObject);
    procedure dxBarButton25Click(Sender: TObject);
    procedure dxBarButton17Click(Sender: TObject);
    procedure dxBarLargeButton18Click(Sender: TObject);
    procedure dxBarLargeButton2Click(Sender: TObject);
    procedure dxBarButton37Click(Sender: TObject);
    procedure dxBarButton42Click(Sender: TObject);
    procedure dxBarButton40Click(Sender: TObject);
    procedure dxBarButton43Click(Sender: TObject);
    procedure dxBarButton44Click(Sender: TObject);
    procedure dxBarButton53Click(Sender: TObject);
    procedure dxBarButton54Click(Sender: TObject);
    procedure dxBarButton55Click(Sender: TObject);
    procedure dxBarLargeButton23Click(Sender: TObject);
    procedure dxBarLargeButton5Click(Sender: TObject);
    procedure dxBarLargeButton22Click(Sender: TObject);
    procedure dxBarLargeButton28Click(Sender: TObject);
    procedure dxBarLargeButton15Click(Sender: TObject);
  private
    { Private declarations }
    MyBitmap: TBitmap;
    function CreateForm(CForm:TForm):boolean;
  protected
    g_AppList : string;

  //  procedure Loaded; override;
  public
    { Public declarations }
    procedure RunSoft(Section : string);
  end;

var
  frmMain: TfrmMain;


implementation

{$R *.dfm}

uses
   ufrmCustomer,
   ufrmSystemConfig,
   ufrmContractInputBase,
   ufrmContract,
   ufrmGoodsPrice,
   ufrmQuotationlib,
   ufrmfastContrctReturnGoods,
   ufrmFastContractGiveBack,
   ufastContrctGooutStorage,
   ufastContrctEnterStorage,
   ufrmFastGiveBack,
   ufrmFastReturnGoods,
   ufrmFastGooutStorage,
   ufrmFastEnterStorage,
   ufrmInputform,
   ufrmStorageBase,
   ufrmSystemSet,
   ufrmRevenueAccount,
   ufrmRunningAccount,
   ufrmReturnStorage,
   ufrmOutStorage,
   ufrmEntryStorage,
   uMaterialStorage,
   ufrmStorageManage,
   ufrmBasePurchases,
   ufrmProjectPurchase,
   ufrmCompanyFormStorage,
   ufrmCompanyStorageEntry,
   ufrmCompanyfiles,
   ufrmCompanyWork,
   ufrmCompanySanction,
   ufrmExpend,
   ufrmBuild,
   ufrmFinalAccount,
   ufrmJobList,
   ufrmCostSubject,
   ufrmEmployee,
   ufrmCompleteManage,
   ufrmQualityManage,
   ufrmTenderManage,
   ufrmConstructManage,
   ufrmProjectConcrete,
   ufrmProjectFinance,
   ufrmMaintainProject,
   ufrmProjectManage,
   ufrmProjectMechanics,
   ufrmProjectLease,
   ufrmProjectSanction,
   ufrmProjectSubcontract,
   ufrmProjectWork,
   ufrmWorkType,
   ufrmSection,
   ufrmMakings,
   ufrmCompanyManage,
   ufrmDatum,
   ufrmReconciliationTable,
   ShellAPI,
   ufrmFlowingWaterAccount,
   ufrmlogin,
   uDataModule,
   ufrmsystem,ufrmSort,
   global,
   ufrmManage,
   ufrmBackups,
   ufrmReceivablesAccount,
   ufrmBorrowAccount;

Function ClientWindowProc( wnd: HWND; msg: Cardinal; wparam, lparam: Integer ): Integer; stdcall;
Var
  pUserdata: Pointer;
Begin
   pUserdata:= Pointer( GetWindowLong( wnd, GWL_USERDATA ));
   Case msg of
     WM_NCCALCSIZE:
       Begin
         If (GetWindowLong( wnd, GWL_STYLE ) and (WS_HSCROLL or WS_VSCROLL)) <> 0 Then
           SetWindowLong( wnd, GWL_STYLE, GetWindowLong(wnd, GWL_STYLE) and not (WS_HSCROLL or WS_VSCROLL));
       End;
   End;
   Result := CallWindowProc(pUserdata, wnd, msg, wparam, lparam );
end;

function TfrmMain.CreateForm(CForm:TForm):boolean;
var
  i:integer;
  FormExist:boolean;
begin

    FormExist:=false;
    
    if(CForm=Nil) then     //判断CFrom是否为空
    begin
       CreateForm:=false; //函数返回值赋false
       exit;               //退出本函数
    end;

    for i:=0 to Screen.FormCount-1 do                     //判断窗体是否已经建立起来
    begin
      if Screen.Forms[i].ClassType = CForm.ClassType then //判断窗体存在
         FormExist:=true;
    end;

    if FormExist=false then
    begin
      CreateForm := false; //函数返回值赋false
      exit;               //退出本函数
    end;

    if CForm.WindowState=wsMinimized then
      ShowWindow(CForm.Handle,SW_SHOWNORMAL) //显示窗体
    else
      ShowWindow(CForm.Handle,SW_SHOWNA);    //显示窗体

    if not CForm.Visible then
       CForm.Visible:=true;
   CForm.BringToFront;                     //当前窗口显示在最前面
   CForm.SetFocus;
   //CForm.WindowState :=wsMaximized;
   CreateForm:=true;

end;


function GetApplicationVersion:String;
var
  FileName:String;
  InfoSize,Wnd:DWORD;
  VerBuf:Pointer;
  VerInfo:^VS_FIXEDFILEINFO;
  
begin
   Result:='0.0.0.0';
   FileName:=Application.ExeName;
   InfoSize:=GetFileVersionInfoSize(PChar(FileName),Wnd);
   if InfoSize<>0 then
   begin
     GetMem(VerBuf,InfoSize);
     try
       if GetFileVersionInfo(PChar(FileName),Wnd,InfoSize,VerBuf) then 
       begin
         VerInfo:=nil; 
         VerQueryValue(VerBuf,'\',Pointer(VerInfo),Wnd);
         if VerInfo<>nil then Result:=Format('%d.%d.%d.%d',[VerInfo^.dwFileVersionMS shr 16, 
                                                            VerInfo^.dwFileVersionMS and $0000ffff,
                                                            VerInfo^.dwFileVersionLS shr 16,
                                                            VerInfo^.dwFileVersionLS and $0000ffff]);
       end;
     finally
       FreeMem(VerBuf,InfoSize);
     end;
   end;
end;

procedure TfrmMain.dxBarButton10Click(Sender: TObject);
begin
  Application.CreateForm(TfrmFastGooutStorage,frmFastGooutStorage);
  try
    frmFastGooutStorage.dwIsEdit    := False;
    frmFastGooutStorage.dwIsContrct := False;
    frmFastGooutStorage.dwDataType  := 1;
    frmFastGooutStorage.ShowModal;
  finally
    frmFastGooutStorage.Free;
  end;
end;

procedure TfrmMain.dxBarButton11Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmConcrete.Create(Application).Show;
  end;

end;

procedure TfrmMain.dxBarButton12Click(Sender: TObject);
var
  Child : TfrmFinalAccount ;

begin
  if MdiChildCount = 0  then
  begin

    if Sender = Self.dxBarButton12 then
    begin
      //外包劳务
      g_ModuleIndex := 1;
    end else
    if Sender = Self.dxBarButton49 then
    begin
      //员工考勤
      g_ModuleIndex := 2;
    end else
    if Sender = Self.dxBarButton50 then
    begin
      //材料租赁
      g_ModuleIndex := 3;
    end else
    if Sender = Self.dxBarButton51 then
    begin
      //机械租赁
      g_ModuleIndex := 4;
    end else
    if Sender = Self.dxBarButton38 then
    begin
      //材料
      g_ModuleIndex := 5;
    end else
    if Sender = Self.dxBarButton39 then
    begin
      //专业分包
      g_ModuleIndex := 6;
    end else
    if Sender = Self.dxBarButton35 then
    begin
      //商混
      g_ModuleIndex := 7;
    end else
    if Sender = Self.dxBarButton41 then
    begin
      //项目
      g_ModuleIndex := 8;
    end else
    if Sender = Self.dxBarButton52 then
    begin
      g_ModuleIndex := 9;
    end;

    Child := TfrmFinalAccount.Create(Application);
    try
      Child.FromTypes := g_ModuleIndex;
      Child.ShowModal;
    finally
      Child.Free;
    end;

  end;

end;

procedure TfrmMain.dxBarButton13Click(Sender: TObject);
var
  frmReconciliationTable  :  TfrmReconciliationTable;
begin
  if MdiChildCount = 0  then
  begin
    frmReconciliationTable := TfrmReconciliationTable.Create(Self);
    try
      frmReconciliationTable.Show;
    finally
    end;
  end;
end;

procedure TfrmMain.dxBarButton14Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    g_RunningIndex := 0;
    TfrmRunningAccount.Create(Application);
  end;

end;

procedure TfrmMain.dxBarButton15Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    g_RunningIndex := 1;
    TfrmRunningAccount.Create(Application);
  end;

end;

procedure TfrmMain.dxBarButton16Click(Sender: TObject);
begin
  {
  //总仓库
  if MdiChildCount = 0  then
  begin
    TfrmMaterialStorage.Create(Application);
  end;
  }
  Application.CreateForm(TfrmFastGiveBack,frmFastGiveBack);
  try
    frmFastGiveBack.dwIsEdit    := False;
    frmFastGiveBack.dwIsContrct := False;
    frmFastGiveBack.dwDataType  := 2;
    frmFastGiveBack.ShowModal;
  finally
    frmFastGiveBack.Free;
  end;
end;

procedure TfrmMain.dxBarButton17Click(Sender: TObject);
var
  Child : TfrmFinalAccount ;

begin
  if MdiChildCount = 0  then
  begin
    if Sender = Self.dxBarButton17 then
    begin
      //员工考勤
      g_ModuleIndex := 2;
    end else
    if Sender = Self.dxBarButton18 then
    begin
      //材料成本
      g_ModuleIndex := 5;
    end else
    if Sender = Self.dxBarButton46 then
    begin
      //专业分包
      g_ModuleIndex := 6;
    end else
    if Sender = Self.dxBarButton48 then
    begin
      //管理成本
      g_ModuleIndex := 8;
    end else
    if Sender = Self.dxBarButton47 then
    begin
      //管理成本
      g_ModuleIndex := 3;
    end;

    Child := TfrmFinalAccount.Create(Application);
    try
      Child.FromTypes := g_ModuleIndex;
      Child.ShowModal;
    finally
      Child.Free;
    end;

  end;

end;

procedure TfrmMain.dxBarButton1Click(Sender: TObject);
var
  Child : Tfrmsystem;
begin
  //系统管理
  Child := Tfrmsystem.Create(Self);
  try
    Child.ShowModal;
  finally
  end;

end;

procedure TfrmMain.dxBarButton21Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    dwSelectTypes := 3;
    TfrmReturnStorage.Create(Application);
  end;

end;

procedure TfrmMain.dxBarButton23Click(Sender: TObject);
begin
  Application.CreateForm(TfastContrctEnterStorage,fastContrctEnterStorage);
  try
    fastContrctEnterStorage.dwIsEdit     := False;
    fastContrctEnterStorage.dwIsContrct  := True;
    fastContrctEnterStorage.dwDataType   := 0; //合同入库
    fastContrctEnterStorage.ShowModal;
  finally
    frmFastEnterStorage.Free;
  end;
end;

procedure TfrmMain.dxBarButton25Click(Sender: TObject);
begin
  Application.CreateForm(TfastContrctGooutStorage,fastContrctGooutStorage);
  try
    fastContrctGooutStorage.dwIsEdit     := False;
    fastContrctGooutStorage.dwIsContrct  := True;
    fastContrctGooutStorage.dwDataType   := 1;  //合同出库
    fastContrctGooutStorage.ShowModal;
  finally
    frmFastEnterStorage.Free;
  end;
end;

procedure TfrmMain.dxBarButton27Click(Sender: TObject);
var
  Child : TfrmReceivablesAccount;
begin
  if MdiChildCount = 0  then
  begin
    Child := TfrmReceivablesAccount.Create(Self);
    try
      g_pacttype := 11;
      Child.ShowModal;
    finally
    end;
  end;
end;

procedure TfrmMain.dxBarButton29Click(Sender: TObject);
var
  Child : TfrmEntryStorage;
begin
  if MdiChildCount = 0  then
  begin
    dwSelectTypes := 1;
    Child := TfrmEntryStorage.Create(Application);
    Child.Show;
  end;

end;

procedure TfrmMain.dxBarButton30Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmProjectPurchase.Create(Application).Show;
  end;

end;

procedure TfrmMain.dxBarButton32Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmFlowingWaterAccount.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarButton37Click(Sender: TObject);
var
  s : string;

begin
  Application.CreateForm(TfrmContract,frmContract);
  try
    s := frmContract.Caption + ' - 合同入库单';
    frmContract.dwIsEdit := False;
    frmContract.Caption  := s;
    frmContract.dwModule := 0;
    frmContract.ShowModal;
  finally
    frmContract.Free;
  end;
end;

procedure TfrmMain.dxBarButton3Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmRevenueAccount.Create(Application);
  end;
end;

procedure TfrmMain.dxBarButton40Click(Sender: TObject);
var
  Child : TfrmBorrowAccount;
begin
  //借货管理
  if MDIChildCount = 0 then
  begin
    Child := TfrmBorrowAccount.Create(Self);
    try
      Child.ShowModal;;
    finally
    end;
  end;
end;

procedure TfrmMain.dxBarButton42Click(Sender: TObject);
var
  s : string;
begin
  Application.CreateForm(TfrmContract,frmContract);
  try
    s := frmContract.Caption + ' - 合同出库单';
    frmContract.dwIsEdit := False;
    frmContract.Caption  := s;
    frmContract.dwModule := 1;
    frmContract.ShowModal;
  finally
    frmContract.Free;
  end;
end;

procedure TfrmMain.dxBarButton43Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    g_RunningIndex := 2;
    TfrmRunningAccount.Create(Application);
  end;
end;

procedure TfrmMain.dxBarButton44Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    g_RunningIndex := 3;
    TfrmRunningAccount.Create(Application);
  end;
end;

procedure TfrmMain.dxBarButton45Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    dwSelectTypes := 4;
    TfrmStorageManage.Create(Application);
  end;
// 入库 + 库存 = 库存   库存 - 出库 = 库存   库存-损耗=库存

//（入库+库存=库存 ）- 出库 = 库存-损耗=库存
end;

procedure TfrmMain.dxBarButton4Click(Sender: TObject);
begin
  //入库
  Application.CreateForm(TfrmFastEnterStorage,frmFastEnterStorage);
  try
    frmFastEnterStorage.dwIsEdit    := False;
    frmFastEnterStorage.dwIsContrct := False;
    frmFastEnterStorage.dwDataType  := 0;
    frmFastEnterStorage.ShowModal;
  finally
    frmFastEnterStorage.Free;
  end;
end;

procedure TfrmMain.dxBarButton53Click(Sender: TObject);
var
  Child : TfrmJobList;
begin
  Child := TfrmJobList.Create(Application);
  Child.Show;
end;

procedure TfrmMain.dxBarButton54Click(Sender: TObject);
var
  Child : TfrmWorkType;
begin
  Child := TfrmWorkType.Create(Application);
  try
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmMain.dxBarButton55Click(Sender: TObject);
var
  Child : TfrmSection;
begin
  Child := TfrmSection.Create(Application);
  try
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmMain.dxBarButton5Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
//  TfrmProjectStorage.Create(Application);
    TfrmProjectSanction.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarButton6Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmProjectMechanics.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarButton7Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
  TfrmProjectSubcontract.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarButton8Click(Sender: TObject);
//var
//  Child : TfrmCompanyStorageEntry;

begin
  Application.CreateForm(TfrmFastReturnGoods,frmFastReturnGoods);
  try
    frmFastReturnGoods.dwIsEdit    := False;
    frmFastReturnGoods.dwIsContrct := False;
    frmFastReturnGoods.dwDataType  := 4;
    frmFastReturnGoods.ShowModal;
  finally
    frmFastReturnGoods.Free;
  end;

  if MdiChildCount = 0  then
  begin
    //公司仓库入库
    {
    Child := TfrmCompanyStorageEntry.Create(Application);
    try
      Child.Show;
    finally
    end;
    }
  end;

end;

procedure TfrmMain.dxBarButton9Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
  dwSelectTypes := 2;
  TfrmOutStorage.Create(Application);
  end;
end;

procedure TfrmMain.dxBarLargeButton10Click(Sender: TObject);
var
  Child : Tfrmsystem;
begin
  Child := Tfrmsystem.Create(Self);
  try
    Child.ShowModal;
  finally
  end;
end;

procedure TfrmMain.dxBarLargeButton11Click(Sender: TObject);
var
  Child : TfrmSort ;
begin
  Child := TfrmSort.Create(Self);
  try
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmMain.dxBarLargeButton13Click(Sender: TObject);
var
  Child : TfrmMakings;
begin
  Child := TfrmMakings.Create(Application);
  try
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmMain.dxBarLargeButton14Click(Sender: TObject);
var
  Child : TfrmProjectManage;
begin
  Child := TfrmProjectManage.Create(Application);
  try
  //  Child.g_ModuleIndex := 0;//财务管理
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmMain.dxBarLargeButton15Click(Sender: TObject);
begin
  if MDIChildCount = 0 then
  begin
    Application.CreateForm(TfrmCustomer,frmCustomer);
    try
      //所有表格全显示
      frmCustomer.dwOfferType := 3;  //客户报价
      frmCustomer.ShowModal;
    finally
      frmCustomer.free;
    end;
  end;
end;

procedure TfrmMain.dxBarLargeButton17Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmProjectLease.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarLargeButton18Click(Sender: TObject);
begin
  Application.CreateForm(TfrmQuotationlib,frmQuotationlib);
  try
    frmQuotationlib.ShowModal;
  finally
    frmQuotationlib.Free;
  end;
end;

procedure TfrmMain.dxBarLargeButton19Click(Sender: TObject);
var
  Child : TfrmMaintainProject;
begin
  Child := TfrmMaintainProject.Create(Application);
  try
    Child.Show;
  finally
  end;
end;

procedure TfrmMain.RunSoft(Section : string);
var
  szIni : TIniFile;
  szFiles : string;

begin
  if FileExists(g_AppList) then
  begin
    szIni := TIniFile.Create(g_AppList);
    try
      szFiles := szIni.ReadString(Section,'AppPath','');
    finally
      szIni.Free;
    end;

  end;

  if Length(szFiles) = 0 then
  begin
    if Application.MessageBox(PWideChar( '无法找到当前所关联的软件，是否选择软件所在位置？'),
                              '查找软件',
                              MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      if dlgOpen.Execute(Handle) then
      begin
        szFiles := dlgOpen.FileName;
        if Length( szFiles  ) <> 0 then
        begin
          szIni := TIniFile.Create(g_AppList);
          try
            szIni.WriteString(Section,'AppPath',szFiles);
          finally
            szIni.Free;
          end;

        end;

      end;

    end;

  end else
  begin
    if FileExists(szFiles) then
    begin
      ShellExecute(handle,'open',PWideChar( szFiles ),'', '', SW_SHOW);
    end;

  end;

end;


procedure TfrmMain.dxBarLargeButton1Click(Sender: TObject);
var
  Child : TfrmCompanyManage;
begin
  Child := TfrmCompanyManage.Create(Application);
  try
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmMain.dxBarLargeButton22Click(Sender: TObject);
begin
  if MDIChildCount = 0 then
  begin
    Application.CreateForm(TfrmCustomer,frmCustomer);
    try
      //所有表格全显示
      frmCustomer.dwOfferType := 2;  //主材报价
      frmCustomer.ShowModal;
    finally
      frmCustomer.free;
    end;
  end;
end;

procedure TfrmMain.dxBarLargeButton23Click(Sender: TObject);
begin
  Application.CreateForm(TfrmSystemConfig,frmSystemConfig);
  try
    frmSystemConfig.ShowModal;
  finally
    frmSystemConfig.Free;
  end;
end;

procedure TfrmMain.dxBarLargeButton24Click(Sender: TObject);
var
  Datum : TfrmDatum;

begin
  Datum := TfrmDatum.Create(Application);
  try
  //  Datum.g_ProjectName := g_ProjectName;
  //  Datum.g_ProjectCode := g_ProjectCode;
    Datum.ShowModal;
  finally
    Datum.Free;
  end;
end;

procedure TfrmMain.dxBarLargeButton25Click(Sender: TObject);
begin
  TfrmEmployee.Create(Application).Show;
end;

procedure TfrmMain.dxBarLargeButton28Click(Sender: TObject);
begin
  if MDIChildCount = 0 then
  begin
    Application.CreateForm(TfrmCustomer,frmCustomer);
    try
      //所有表格全显示
      frmCustomer.dwOfferType := 1;  //客户报价
      frmCustomer.ShowModal;
    finally
      frmCustomer.free;
    end;
  end;
end;

procedure TfrmMain.dxBarLargeButton2Click(Sender: TObject);
begin
  Application.CreateForm(TfrmGoodsPrice,frmGoodsPrice);
  try
    frmGoodsPrice.ShowModal;
  finally
    frmGoodsPrice.Free;
  end;
end;

procedure TfrmMain.dxBarLargeButton30Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmCompanyWork.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarLargeButton35Click(Sender: TObject);
begin
  RunSoft('HwGantt');
end;

procedure TfrmMain.dxBarLargeButton36Click(Sender: TObject);
begin
  RunSoft('Lease');
end;

procedure TfrmMain.dxBarLargeButton37Click(Sender: TObject);
begin
  RunSoft('HZTC');
end;

procedure TfrmMain.dxBarLargeButton38Click(Sender: TObject);
begin
  RunSoft('LGWUJIN');
end;

procedure TfrmMain.dxBarLargeButton3Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
  TfrmProjectFinance.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarLargeButton40Click(Sender: TObject);
begin
  TfrmCostSubject.Create(Application).Show;
end;

procedure TfrmMain.dxBarLargeButton44Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmCompanySanction.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarLargeButton47Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
  TfrmProjectWork.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarLargeButton48Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
    TfrmCompanyfiles.Create(Application).Show;
  end;
end;

procedure TfrmMain.dxBarLargeButton4Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
  //  TfrmStorageBase.Create(Application);
    Application.CreateForm(TfrmStorageBase,frmStorageBase);
  end;
end;

procedure TfrmMain.dxBarLargeButton5Click(Sender: TObject);
begin
  if MDIChildCount = 0 then
  begin
    Application.CreateForm(TfrmCustomer,frmCustomer);
    try
      //所有表格全显示
      frmCustomer.dwOfferType := 0;
      frmCustomer.ShowModal;
    finally
      frmCustomer.free;
    end;
  end;
end;

procedure TfrmMain.dxBarLargeButton6Click(Sender: TObject);
var
  Child : TfrmTenderManage;
begin
  if MdiChildCount = 0  then
  begin
    Child := TfrmTenderManage.Create(Application);
    Child.Show;
  end;
{
  if MdiChildCount = 0  then
  begin
  end;
}
end;

procedure TfrmMain.dxBarLargeButton7Click(Sender: TObject);
begin
  if MdiChildCount = 0  then
  begin
  TfrmConstructManage.Create(Application);
  end;
end;
{
var
  Child : TfrmConstructManage;
begin
  Child := TfrmConstructManage.Create(Application);
  try
    Child.Show;
  finally

  end;

end;
}
{
var
  Child : TfrmProjectAccount;
begin
  if MdiChildCount = 0  then
  begin
    g_pacttype := 2;  //施工
    g_IsSelect := True;
    Child := TfrmProjectAccount.Create(Self);
    Child.g_SuffixName := Self.dxBarLargeButton7.Caption;
    Child.ShowModal;
  end;
end;
}

procedure TfrmMain.dxBarLargeButton8Click(Sender: TObject);
var
  Child : TfrmQualityManage;
begin
  if MdiChildCount = 0  then
  begin
  Child := TfrmQualityManage.Create(Application);
  Child.Show;
  end;
  {

  if MdiChildCount = 0  then
  begin
    g_pacttype := 3;  //监督
    g_IsSelect := True;
    Child := TfrmProjectAccount.Create(Self);
    Child.g_SuffixName := Self.dxBarLargeButton8.Caption;
    Child.ShowModal;
  end;
  }
end;

procedure TfrmMain.dxBarLargeButton9Click(Sender: TObject);
var
  Child : TfrmCompleteManage;
begin
  if MdiChildCount = 0  then
  begin
  Child := TfrmCompleteManage.Create(Application);
  Child.Show;
  end;
  {
  if MdiChildCount = 0  then
  begin
    g_pacttype := 4;  //竣工
    g_IsSelect := True;
    Child := TfrmProjectAccount.Create(Self);
    Child.g_SuffixName := Self.dxBarLargeButton9.Caption;
    Child.ShowModal;
  end;
  }
end;

procedure TfrmMain.FormActivate(Sender: TObject);
begin
{
if Length(g_SelectProjectName) = 0 then
  begin
    Self.dxBarLargeButton14.Click;
  end;
  }
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  i:integer;
begin
  if MessageBox(handle, PChar('是否要退出系统?'),'提示',
     MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
  begin

    DM.ADOconn.Connected := True;
    DM.ADOconn.Cancel;
    DM.ADOconn.Close;
    for I := MDIChildCount - 1 downto 0  do
    begin
      if MDIChildren[i].Visible then
      begin
        MDIChildren[I].Close;
      end;
    end;
    if MessageBox(handle, PChar('注：是否备份更新当前数据库到备分数据库中，如有误删可以从备分的数据库中提取数据?'),'提示',
       MB_ICONQUESTION + MB_YESNO) = IDYES then
    begin
      if frmBackups = nil then
      begin
        frmBackups := TfrmBackups.Create(Self);
        frmBackups.ShowModal;
      end
      else
      begin
        frmBackups.ShowModal;
      end;

    end;

    MyBitmap.Free;

  end
  else
  begin
    CanClose := False;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
var
  szIni : TIniFile;
  Title : string;
  Path : string;
  Jpg : TJpegImage;
  BMP : TBitMap ;
begin
//  Self.dxRibbon1Tab1.VisibleIndex := 0;
  DisableAero := True;
  g_RebarList[0].Model := '一Ф6.5';
  g_RebarList[0].Weight:= 0.26;
  g_RebarList[0].Level := 1;

  g_RebarList[1].Model := '一Ф8';
  g_RebarList[1].Weight:= 0.395;
  g_RebarList[1].Level := 1;

  g_RebarList[2].Model := '一Ф10';
  g_RebarList[2].Weight:= 0.617;
  g_RebarList[2].Level := 1;

  g_RebarList[3].Model := '一Ф12';
  g_RebarList[3].Weight:= 0.888;
  g_RebarList[3].Level := 1;


  g_RebarList[4].Model := '三Ф6';
  g_RebarList[4].Weight:= 0.222;
  g_RebarList[4].Level := 1;

  g_RebarList[5].Model := '三Ф6.5';
  g_RebarList[5].Weight:= 0.26;
  g_RebarList[5].Level := 1;

  g_RebarList[6].Model := '三Ф8';
  g_RebarList[6].Weight:= 0.395;
  g_RebarList[6].Level := 1;

  g_RebarList[7].Model := '三Ф10';
  g_RebarList[7].Weight:= 0.617;
  g_RebarList[7].Level := 1;

  g_RebarList[8].Model := '三Ф12';
  g_RebarList[8].Weight:= 0.888;
  g_RebarList[8].Level := 1;

  g_RebarList[9].Model := '三Ф14';
  g_RebarList[9].Weight:= 1.21;
  g_RebarList[9].Level := 1;

  g_RebarList[10].Model := '三Ф16';
  g_RebarList[10].Weight:= 1.58;
  g_RebarList[10].Level := 1;

  g_RebarList[11].Model := '三Ф18';
  g_RebarList[11].Weight:= 2.0;
  g_RebarList[11].Level := 1;

  g_RebarList[12].Model := '三Ф20';
  g_RebarList[12].Weight:= 2.47;
  g_RebarList[12].Level := 1;

  g_RebarList[13].Model := '三Ф25';
  g_RebarList[13].Weight:= 3.85;
  g_RebarList[13].Level := 1;

  g_RebarList[14].Model := '三Ф32';
  g_RebarList[14].Weight:= 6.31;
  g_RebarList[14].Level := 1;

  g_RebarList[15].Model := '三Ф40';
  g_RebarList[15].Weight:= 9.87;
  g_RebarList[15].Level := 1;


  //=============================
  RebarList[0].Model := '一Ф6.5';
  RebarList[0].Weight:= 0.00026;
  RebarList[0].Level := 1;

  RebarList[1].Model := '一Ф8';
  RebarList[1].Weight:= 0.000395;
  RebarList[1].Level := 1;

  RebarList[2].Model := '一Ф10';
  RebarList[2].Weight:= 0.000617;
  RebarList[2].Level := 1;

  RebarList[3].Model := '一Ф12';
  RebarList[3].Weight:= 0.000888;
  RebarList[3].Level := 1;

  RebarList[4].Model := '三Ф6';
  RebarList[4].Weight:= 0.000222;
  RebarList[4].Level := 1;

  RebarList[5].Model := '三Ф6.5';
  RebarList[5].Weight:= 0.00026;
  RebarList[5].Level := 1;

  RebarList[6].Model := '三Ф8';
  RebarList[6].Weight:= 0.000395;
  RebarList[6].Level := 1;

  RebarList[7].Model := '三Ф10';
  RebarList[7].Weight:= 0.000617;
  RebarList[7].Level := 1;

  RebarList[8].Model := '三Ф12';
  RebarList[8].Weight:= 0.000888;
  RebarList[8].Level := 1;

  RebarList[9].Model := '三Ф14';
  RebarList[9].Weight:= 0.00121;
  RebarList[9].Level := 1;

  RebarList[10].Model := '三Ф16';
  RebarList[10].Weight:= 0.00158;
  RebarList[10].Level := 1;

  RebarList[11].Model := '三Ф18';
  RebarList[11].Weight:= 0.002;
  RebarList[11].Level := 1;

  RebarList[12].Model := '三Ф20';
  RebarList[12].Weight:= 0.00247;
  RebarList[12].Level := 1;

  RebarList[13].Model := '三Ф25';
  RebarList[13].Weight:= 0.00385;
  RebarList[13].Level := 1;

  RebarList[14].Model := '三Ф32';
  RebarList[14].Weight:= 0.00631;
  RebarList[14].Level := 1;

  RebarList[15].Model := '三Ф40';
  RebarList[15].Weight:= 0.00987;
  RebarList[15].Level := 1;

  Jpg :=TJpegImage.Create;
  try
    MyBitmap := TBitmap.Create;
    Jpg.LoadFromFile('Main.jpg');
    MyBitmap.Assign(Jpg);
  finally
    Jpg.Free;
  end;

  Path := ExtractFilePath(ParamStr(0)) ;
  g_AppList := Path + 'App.ini';
  g_Resources := Concat(Path,'Resources');
  szIni := TIniFile.Create(GetFilePath + m_Config);
  try
    Title := szIni.ReadString(m_Node, 'title' ,'系统管理软件');
    g_SelectProjectName := szIni.ReadString(m_Node,'ProjectName','');
    m_AppName := Title;
    Caption := Title +  ' Build - ' + GetApplicationVersion;
  finally
    szIni.Free;
  end;

  If ClientHandle <> 0 Then
  Begin
    If GetWindowLong( ClientHandle, GWL_USERDATA ) <> 0 Then Exit;

    SetWindowLong( ClientHandle, GWL_USERDATA, SetWindowLong( ClientHandle, GWL_WNDPROC, integer( @ClientWindowProc)));
  End;

end;

procedure TfrmMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = 81) then
  begin
    TfrmSystemSet.Create(Application).show;
  end;
end;

procedure DrawFormImage(Form: TForm; Bitmap: TBitmap; DrawMode: TDrawMode = dwCenter);
var
  X, Y: Integer;
begin
  case DrawMode of
    dwCenter:
      begin
        X := (Form.ClientWidth - Bitmap.Width) div 2;
        Y := (Form.ClientHeight - Bitmap.Height) div 2;
        Form.Canvas.Draw(X, Y, Bitmap);
      end;
    dwStretch:
      begin
        Form.Canvas.StretchDraw(Form.ClientRect, Bitmap);
      end;
    dwTile:
      begin
        Form.Canvas.Brush.Bitmap := Bitmap;
        Form.Canvas.FillRect(Form.ClientRect);
      end;
  end;
end;

procedure TfrmMain.FormPaint(Sender: TObject);
begin
  DrawFormImage(Self, MyBitmap,dwStretch);
  { 这个函数的第三个参数可以不写，默认为dwCenter }
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  InitializeCriticalSection(CS);//初始化临界区
end;

procedure TfrmMain.N2Click(Sender: TObject);
var
  Child : TfrmManage;
begin
   if m_user.dwType = 0 then
   begin
     Child := TfrmManage.Create(Application);
   end;

end;

procedure TfrmMain.N3Click(Sender: TObject);
begin
  if m_user.dwType = 0 then
  begin
    frmsystem := Tfrmsystem.Create(Self);
    try
      frmsystem.ShowModal;
    finally
      frmsystem.Free;
    end;
  end; 

end;

procedure TfrmMain.N6Click(Sender: TObject);
begin
  Close;
end;

end.

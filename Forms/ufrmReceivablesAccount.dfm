object frmReceivablesAccount: TfrmReceivablesAccount
  Left = 0
  Top = 0
  Caption = #33829#25910'-'#24080#30446#31649#29702
  ClientHeight = 361
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object RzToolbar10: TRzToolbar
    Left = 0
    Top = 0
    Width = 688
    Height = 34
    AutoStyle = False
    Images = DM.cxImageList1
    RowHeight = 30
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdTop]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 0
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer77
      cxLabel2
      cxLookupComboBox1
      RzSpacer3
      RzBut1
      RzSpacer1
      RzSpacer4
      RzBut2
      RzSpacer5
      RzBut3
      RzSpacer78
      RzBut4
      RzSpacer79
      RzBut5
      RzSpacer7
      RzBut6
      RzSpacer6
      RzBut7
      RzSpacer80
      RzBut8
      RzSpacer8)
    object RzSpacer77: TRzSpacer
      Left = 4
      Top = 5
    end
    object RzBut2: TRzToolButton
      Left = 273
      Top = 5
      Hint = #26032#28155#21152#19968#26465#24037#31243#39033#30446
      ImageIndex = 37
      ShowCaption = False
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      ParentShowHint = False
      ShowHint = True
      OnClick = RzBut2Click
    end
    object RzSpacer78: TRzSpacer
      Left = 331
      Top = 5
    end
    object RzBut4: TRzToolButton
      Left = 339
      Top = 5
      ImageIndex = 3
      ShowCaption = False
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #20445#23384
      ParentShowHint = False
      ShowHint = True
      OnClick = RzBut4Click
    end
    object RzSpacer79: TRzSpacer
      Left = 364
      Top = 5
    end
    object RzBut5: TRzToolButton
      Left = 372
      Top = 5
      Hint = #21024#38500#25152#26377#19982#26412#24037#31243#30456#20851#32852#30340#25968#25454#19982#23376#24037#31243
      ImageIndex = 51
      ShowCaption = False
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21024#38500
      ParentShowHint = False
      ShowHint = True
      OnClick = RzBut5Click
    end
    object RzSpacer80: TRzSpacer
      Left = 477
      Top = 5
    end
    object RzBut8: TRzToolButton
      Left = 485
      Top = 5
      Width = 30
      ImageIndex = 8
      ShowCaption = False
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
      ParentShowHint = False
      ShowHint = True
      OnClick = RzBut8Click
    end
    object RzSpacer1: TRzSpacer
      Left = 250
      Top = 5
      Width = 15
    end
    object RzBut1: TRzToolButton
      Left = 225
      Top = 5
      ImageIndex = 10
      OnClick = RzBut1Click
    end
    object RzBut7: TRzToolButton
      Left = 438
      Top = 5
      Width = 39
      DropDownMenu = pm
      ImageIndex = 16
      ToolStyle = tsDropDown
      Caption = #21382#21490#25968#25454
    end
    object RzSpacer3: TRzSpacer
      Left = 217
      Top = 5
    end
    object RzSpacer4: TRzSpacer
      Left = 265
      Top = 5
    end
    object RzSpacer5: TRzSpacer
      Left = 298
      Top = 5
    end
    object RzBut3: TRzToolButton
      Left = 306
      Top = 5
      Hint = #32534#36753#24037#31243#39033#30446
      ImageIndex = 0
      ParentShowHint = False
      ShowHint = True
      OnClick = RzBut3Click
    end
    object RzSpacer6: TRzSpacer
      Left = 430
      Top = 5
    end
    object RzSpacer7: TRzSpacer
      Left = 397
      Top = 5
    end
    object RzSpacer8: TRzSpacer
      Left = 515
      Top = 5
    end
    object RzBut6: TRzToolButton
      Left = 405
      Top = 5
      Hint = #35835#21462#25152#26377#26410#21024#38500#30340#24037#31243
      ImageIndex = 11
      Caption = #21047#26032
      ParentShowHint = False
      ShowHint = True
      OnClick = RzBut6Click
    end
    object cxLabel2: TcxLabel
      Left = 12
      Top = 9
      Caption = #20851#38190#23383':'
      Transparent = True
    end
    object cxLookupComboBox1: TcxLookupComboBox
      Left = 56
      Top = 7
      RepositoryItem = DM.SignNameBox
      Properties.DropDownListStyle = lsEditList
      Properties.ImmediateDropDownWhenActivated = True
      Properties.KeyFieldNames = 'ProjectName'
      Properties.ListColumns = <
        item
          Caption = #24037#31243#21517#31216
          FieldName = 'ProjectName'
        end
        item
          Caption = #31616#30721
          FieldName = 'ProjectPYCode'
        end>
      Properties.ListOptions.GridLines = glVertical
      Properties.ListOptions.ShowHeader = False
      Properties.ListOptions.SyncMode = True
      Properties.OnCloseUp = cxLookupComboBox1PropertiesCloseUp
      TabOrder = 1
      OnKeyDown = cxLookupComboBox1KeyDown
      Width = 161
    end
  end
  object Grid: TcxGrid
    Left = 0
    Top = 34
    Width = 688
    Height = 308
    Align = alClient
    TabOrder = 1
    object tvView: TcxGridDBTableView
      PopupMenu = Old
      OnDblClick = tvViewDblClick
      Navigator.Buttons.CustomButtons = <>
      OnEditing = tvViewEditing
      DataController.DataSource = DataMaster
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.Visible = True
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsBehavior.FocusCellOnCycle = True
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsView.DataRowHeight = 22
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 23
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 14
      object tvViewCol1: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        Properties.Alignment.Horz = taCenter
        OnGetDisplayText = tvViewCol1GetDisplayText
        HeaderAlignmentHorz = taCenter
        Options.Editing = False
        Options.Filtering = False
        Options.Sorting = False
        Width = 40
      end
      object tvViewCol6: TcxGridDBColumn
        DataBinding.FieldName = 'Id'
        Visible = False
        Width = 40
      end
      object tvViewCol2: TcxGridDBColumn
        Caption = #24448#26469#21333#20301
        DataBinding.FieldName = 'SignName'
        PropertiesClassName = 'TcxButtonEditProperties'
        Properties.Buttons = <
          item
            Default = True
            Kind = bkEllipsis
          end>
        Properties.OnButtonClick = tvViewCol2PropertiesButtonClick
        HeaderAlignmentHorz = taCenter
        Width = 243
      end
      object tvViewCol3: TcxGridDBColumn
        Caption = #26085#26399
        DataBinding.FieldName = 'InputDate'
        PropertiesClassName = 'TcxDateEditProperties'
        Properties.Alignment.Horz = taCenter
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 81
      end
      object tvViewCol4: TcxGridDBColumn
        Caption = #29366#24577
        DataBinding.FieldName = 'status'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.Alignment.Horz = taCenter
        Properties.DropDownListStyle = lsEditFixedList
        Properties.ImmediateDropDownWhenActivated = True
        HeaderAlignmentHorz = taCenter
        Width = 62
      end
      object tvViewCol5: TcxGridDBColumn
        Caption = #31867#21035
        DataBinding.FieldName = 'remarks'
        RepositoryItem = DM.TabBox
        Width = 218
      end
      object tvViewCol7: TcxGridDBColumn
        DataBinding.FieldName = 'Code'
        Visible = False
        Width = 60
      end
    end
    object Lv: TcxGridLevel
      GridView = tvView
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 342
    Width = 688
    Height = 19
    Panels = <>
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = MasterAfterInsert
    Left = 456
    Top = 80
  end
  object DataMaster: TDataSource
    DataSet = Master
    Left = 456
    Top = 128
  end
  object Old: TPopupMenu
    Images = DM.cxImageList1
    Left = 168
    Top = 136
    object N6: TMenuItem
      Caption = #26032#12288#12288#22686
      ImageIndex = 37
    end
    object N8: TMenuItem
      Caption = #20445#12288#12288#23384
      ImageIndex = 3
      OnClick = N8Click
    end
    object N5: TMenuItem
      Caption = #32534#12288#12288#36753
      ImageIndex = 0
    end
    object N4: TMenuItem
      Caption = #21024#12288#12288#38500
      ImageIndex = 51
      OnClick = N4Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object MenuItem1: TMenuItem
      Caption = #21382#21490#24037#31243
      ImageIndex = 41
      OnClick = MenuItem1Click
    end
    object N1: TMenuItem
      Caption = #24674#22797#24037#31243
      Enabled = False
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object MenuItem2: TMenuItem
      Caption = #24443#24213#21024#38500#24037#31243
      Enabled = False
      ImageIndex = 51
      OnClick = MenuItem2Click
    end
  end
  object pm: TPopupMenu
    Left = 224
    Top = 136
    object N7: TMenuItem
      Caption = #21382#21490#24037#31243
      OnClick = N7Click
    end
    object N9: TMenuItem
      Caption = #24674#22797#24037#31243
      Enabled = False
      OnClick = N9Click
    end
    object N10: TMenuItem
      Caption = '-'
    end
    object N11: TMenuItem
      Caption = #24443#24213#21024#38500#24037#31243
      Enabled = False
      OnClick = N11Click
    end
  end
end

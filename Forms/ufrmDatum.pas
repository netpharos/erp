﻿unit ufrmDatum;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxTextEdit, cxMemo, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, Vcl.ComCtrls,
  RzButton, RzPanel, Vcl.ExtCtrls, cxSplitter, cxDropDownEdit, dxBarBuiltInMenu,
  cxPC, RzTabs, cxContainer, cxLabel, cxTL, cxMaskEdit, cxTLdxBarBuiltInMenu,
  cxInplaceContainer, cxDBTL, cxTLData, RzTreeVw, dxSkinsdxStatusBarPainter,
  dxStatusBar, UtilsTree, TreeUtils, ufrmBaseController, FillThrdTree, Vcl.StdCtrls,
  cxDBLookupComboBox, Data.Win.ADODB, cxDBExtLookupComboBox, cxCurrencyEdit,
  BMDThread, dxDBSparkline, cxGridCustomPopupMenu, cxGridPopupMenu, dxmdaset,
  Datasnap.DBClient, Datasnap.Provider, cxLookupEdit, cxDBLookupEdit, Vcl.Menus,
  cxButtons, dxRatingControl;

type
  TfrmDatum = class(TfrmBaseController)
    RzPanel2: TRzPanel;
    RzToolbar2: TRzToolbar;
    RzSpacer5: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzToolButton8: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzSpacer15: TRzSpacer;
    Tv1: TTreeView;
    RzPanel1: TRzPanel;
    RzPanel5: TRzPanel;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    TabSheet2: TRzTabSheet;
    TabSheet3: TRzTabSheet;
    Grid: TcxGrid;
    tvWork: TcxGridDBTableView;
    tvWorkCol1: TcxGridDBColumn;
    tvWorkCol2: TcxGridDBColumn;
    tvWorkCol5: TcxGridDBColumn;
    tvWorkCol6: TcxGridDBColumn;
    tvWorkCol7: TcxGridDBColumn;
    tvWorkCol8: TcxGridDBColumn;
    tvWorkCol9: TcxGridDBColumn;
    Lv0: TcxGridLevel;
    MakingGrid: TcxGrid;
    tvMaking: TcxGridDBTableView;
    tvMakingCol1: TcxGridDBColumn;
    tvMakingCol2: TcxGridDBColumn;
    tvMakingCol3: TcxGridDBColumn;
    tvMakingCol5: TcxGridDBColumn;
    tvMakingCol6: TcxGridDBColumn;
    tvMakingCol7: TcxGridDBColumn;
    tvMakingCol9: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    cxGrid2: TcxGrid;
    tvSubContract: TcxGridDBTableView;
    tvSubContractCol1: TcxGridDBColumn;
    tvSubContractCol2: TcxGridDBColumn;
    tvSubContractCol4: TcxGridDBColumn;
    tvSubContractCol5: TcxGridDBColumn;
    tvSubContractCol8: TcxGridDBColumn;
    tvSubContractCol7: TcxGridDBColumn;
    tvSubContractCol9: TcxGridDBColumn;
    Lv2: TcxGridLevel;
    tvWorkCol4: TcxGridDBColumn;
    tvMakingCol8: TcxGridDBColumn;
    tvMakingCol10: TcxGridDBColumn;
    tvMakingCol4: TcxGridDBColumn;
    tvSubContractCol6: TcxGridDBColumn;
    tvWorkCol3: TcxGridDBColumn;
    tvSubContractCol3: TcxGridDBColumn;
    dxStatusBar1: TdxStatusBar;
    RzToolButton15: TRzToolButton;
    ADOMaking: TADOQuery;
    DataMaking: TDataSource;
    ADOList: TADOQuery;
    DataList: TDataSource;
    ADOSign: TADOQuery;
    DataSign: TDataSource;
    ADOCompany: TADOQuery;
    DataCompany: TDataSource;
    ADOWork: TADOQuery;
    DataWork: TDataSource;
    ADOSubContract: TADOQuery;
    DataSubContract: TDataSource;
    tvSubContractColumn1: TcxGridDBColumn;
    tvMakingCol11: TcxGridDBColumn;
    tvWorkColumn1: TcxGridDBColumn;
    Splitter1: TSplitter;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    TabSheet4: TRzTabSheet;
    tvConcrete: TcxGridDBTableView;
    Lv3: TcxGridLevel;
    GridConcrete: TcxGrid;
    tvConcreteCol1: TcxGridDBColumn;
    tvConcreteCol2: TcxGridDBColumn;
    tvConcreteCol3: TcxGridDBColumn;
    tvConcreteCol4: TcxGridDBColumn;
    tvConcreteCol5: TcxGridDBColumn;
    ADOConcrete: TADOQuery;
    DataConcrete: TDataSource;
    ADOTypeName: TADOQuery;
    DataTypeName: TDataSource;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzSpacer2: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer6: TRzSpacer;
    cxLabel1: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    RzPanel3: TRzPanel;
    cxLookupComboBox2: TcxLookupComboBox;
    cxLabel2: TcxLabel;
    RzSpacer7: TRzSpacer;
    tvWorkColumn2: TcxGridDBColumn;
    tvWorkColumn3: TcxGridDBColumn;
    RzSpacer8: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer11: TRzSpacer;
    RzToolButton6: TRzToolButton;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure Tv1Editing(Sender: TObject; Node: TTreeNode; var AllowEdit: Boolean);
    procedure FormShow(Sender: TObject);
    procedure RzToolButton15Click(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure ADOMakingAfterInsert(DataSet: TDataSet);
    procedure tvMakingCol3PropertiesCloseUp(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure tvWorkCol1GetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
    procedure ADOWorkAfterInsert(DataSet: TDataSet);
    procedure tvWorkEditing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvWorkEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure ADOSubContractAfterInsert(DataSet: TDataSet);
    procedure tvSubContractEditing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvSubContractEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tvWorkEditValueChanged(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
    procedure tvSubContractEditValueChanged(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
    procedure BMDThread1Execute(Sender: TObject; Thread: TBMDExecuteThread; var Data: Pointer);
    procedure tvMakingEditing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvMakingEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
    procedure tvMakingEditValueChanged(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
    procedure tvMakingKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvSubContractKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvWorkKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxLookupComboBox1PropertiesChange(Sender: TObject);
    procedure tvConcreteCol3GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvConcreteCol3PropertiesChange(Sender: TObject);
    procedure tvConcreteEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure ADOConcreteAfterInsert(DataSet: TDataSet);
    procedure tvWorkCol6GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvWorkCol5GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvWorkCol4GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvSubContractCol4GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvConcreteEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure Tv1Change(Sender: TObject; Node: TTreeNode);
    procedure tvMakingCol8GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvSubContractCol7GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure RzToolButton1Click(Sender: TObject);
    procedure ADOWorkAfterOpen(DataSet: TDataSet);
    procedure ADOSubContractAfterOpen(DataSet: TDataSet);
    procedure ADOConcreteAfterOpen(DataSet: TDataSet);
    procedure ADOMakingAfterOpen(DataSet: TDataSet);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure tvMakingCol3PropertiesPopup(Sender: TObject);
    procedure cxLookupComboBox1Enter(Sender: TObject);
    procedure cxLookupComboBox1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cxLookupComboBox2Enter(Sender: TObject);
    procedure cxLookupComboBox2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RzToolButton2Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
    g_MakingsTreeView: TNewUtilsTree;
    g_NewRowCount: Integer;
    IsSave: Boolean;
    function IsSaveData(): Boolean;
  public
    { Public declarations }
    g_ProjectName: string;
    g_ProjectCode: string;
  end;

var
  frmDatum: TfrmDatum;

implementation

uses
  uDataModule, global;

{$R *.dfm}

procedure TfrmDatum.ADOConcreteAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName(Self.tvConcreteCol2.DataBinding.FieldName).Value := Self.cxLookupComboBox2.Text;
      FieldByName('ProjectName').Value := g_ProjectName;
      FieldByName('Code').Value := g_ProjectCode;
    end;
  end;
end;

procedure TfrmDatum.ADOConcreteAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmDatum.ADOMakingAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    FieldByName(Self.tvMakingCol2.DataBinding.FieldName).Value := Self.cxLookupComboBox2.Text;
    FieldByName(Self.tvMakingCol4.DataBinding.FieldName).Value := Date;
    FieldByName('ProjectName').Value := g_ProjectName;
    FieldByName('Code').Value := g_ProjectCode;
  end;
end;

procedure TfrmDatum.ADOMakingAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmDatum.ADOSubContractAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    if not IsEmpty then
    begin
      FieldByName(Self.tvSubContractCol2.DataBinding.FieldName).Value := Self.cxLookupComboBox2.Text;
      FieldByName(Self.tvSubContractCol3.DataBinding.FieldName).Value := Date;
      FieldByName('ProjectName').Value := g_ProjectName;
      FieldByName('Code').Value := g_ProjectCode;

    end;
  end;
end;

procedure TfrmDatum.ADOSubContractAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmDatum.ADOWorkAfterInsert(DataSet: TDataSet);
begin
  inherited;

  with DataSet do
  begin
    if not IsEmpty then
    begin
      FieldByName(Self.tvWorkCol2.DataBinding.FieldName).Value := Self.cxLookupComboBox2.Text;
      FieldByName(Self.tvWorkCol3.DataBinding.FieldName).Value := Date;
      FieldByName('ProjectName').Value := g_ProjectName;
      FieldByName('Code').Value := g_ProjectCode;
    end;
  end;

end;

procedure TfrmDatum.ADOWorkAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmDatum.BMDThread1Execute(Sender: TObject; Thread: TBMDExecuteThread; var Data: Pointer);
var
  s: string;
begin
  inherited;
  with Self.ADOWork do
  begin
    First;
    while not eof do
    begin
      s := FieldByName(Self.tvWorkCol2.DataBinding.FieldName).AsString;

      if Length(s) = 0 then
      begin

        Delete;
        Sleep(30);
      end
      else
      begin
        OutputLog(s);
      end;

      Next;
    end;
  end;
end;

procedure TfrmDatum.tvConcreteCol3GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;

  with (AProperties as TcxComboBoxProperties) do
  begin
    Items.Clear;

    Items.Add( g_ConcreteArray[0] );
    Items.Add( g_ConcreteArray[1] );
    Items.Add( g_ConcreteArray[2] );
    Items.Add( g_ConcreteArray[3] );
    Items.Add( g_ConcreteArray[4] );

  end;

end;

procedure TfrmDatum.tvConcreteCol3PropertiesChange(Sender: TObject);
var
  s , szType : string;
begin
  inherited;
  s := (Sender AS TcxComboBox).EditText;
  with Self.ADOTypeName do
  begin
    Close;
    SQL.Clear;
    if s = g_ConcreteArray[0] then
    begin szType := '1'; //砼强度
    end else
    if s = g_ConcreteArray[1] then
    begin szType := '5';
    end else
    if s = g_ConcreteArray[2] then
    begin szType := '4'; //外加剂
    end else
    if s = g_ConcreteArray[3] then
    begin szType := '2';  //抗渗等级
    end else
    if s = g_ConcreteArray[4] then
    begin szType := '3';  //其他技术要求
    end;
    SQL.Text := 'Select * from unit Where chType=' + szType;
    Open;
    if RecordCount <> 0 then
    begin

      with (Self.tvConcreteCol4.Properties AS TcxComboBoxProperties) do
      begin
        Items.Clear;
        while Not Eof do
        begin
          s := FieldByName('name').AsString;
          Items.Add(s) ;
          Next;
        end;

      end;

    end;
  end;

end;

procedure TfrmDatum.tvConcreteEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := ADOBanEditing(Self.ADOConcrete);
  if AItem.Index = Self.tvConcreteCol1.Index then  AAllow := False;
end;

procedure TfrmDatum.tvConcreteEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvConcreteCol5.Index) then
  begin
    if Self.tvConcrete.Controller.FocusedRow.IsLast and IsNewRow then
    begin
      With Self.ADOConcrete do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvConcreteCol1.FocusWithSelection;
        end;
      end;
    end;
  end;
end;

procedure TfrmDatum.cxLookupComboBox1Enter(Sender: TObject);
begin
  inherited;
  Self.cxLookupComboBox1.DroppedDown := True;
end;

procedure TfrmDatum.cxLookupComboBox1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  Self.cxLookupComboBox1.DroppedDown := True;
end;

procedure TfrmDatum.cxLookupComboBox1PropertiesChange(Sender: TObject);
begin
  Self.RzToolButton1.Click;
end;

procedure TfrmDatum.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
var
  szRowIndex:Integer;
  szCode : Variant;

begin
  inherited;

  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szCode := DataController.DataSet.FieldByName('PS.Code').Value;
      if szCode <> null then
      begin
        g_ProjectCode := VarToStr(szCode);
      end;

    end;

  end;

end;

procedure TfrmDatum.cxLookupComboBox2Enter(Sender: TObject);
begin
  inherited;
  Self.cxLookupComboBox2.DroppedDown := True;
end;

procedure TfrmDatum.cxLookupComboBox2MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  Self.cxLookupComboBox2.DroppedDown := True;
end;

procedure TfrmDatum.Excel1Click(Sender: TObject);
begin
  inherited;
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      CxGridToExcel(Self.Grid,'考勤合同明细表'); //考勤
    end;
    1:
    begin
      //材料
      CxGridToExcel(Self.MakingGrid,'材料合同明细表');
    end;
    2:
    begin
      //分包
      CxGridToExcel(Self.cxGrid2,'分包合同明细表');
    end;
    3:
    begin
      //商混
      CxGridToExcel(Self.GridConcrete,'商混合同明细表');
    end;
  end;

end;

procedure TfrmDatum.FormActivate(Sender: TObject);

  function GetTableData(AQry: TADOQuery; IsDelete: Integer): Boolean;
  begin
    with AQry do
    begin
      Close;
      SQL.Text := 'Select * from ' + g_Table_Maintain_Project   +
                  ' AS PT right join ' + g_Table_Projects_Tree +
                  ' AS PS on PT.ProjectName = PS.SignName WHERE IsDelete = ' + IntToStr(IsDelete) + ';';
      Open;
    end;

  end;

begin
  inherited;
  Self.RzPanel3.Caption := g_ProjectName;
  GetTableData(Self.ADOQuery1, 0);
end;

function TfrmDatum.IsSaveData(): Boolean;
var
  szSignName: string;
begin

  if IsSave then
  begin
    if MessageBox(handle, '有数据未保存,是否需要放弃？', '提示', MB_ICONQUESTION + MB_YESNO) = IDYES then
    begin
      {
      case Self.RzPageControl1.ActivePageIndex of
        0:
          begin
            if Self.tvWork.DataController.RowCount > 0 then
            begin
              szSignName := Self.tvWorkCol2.DataBinding.FieldName;

              with Self.ADOWork do
              begin
                if State <> dsInactive then
                begin
                  if State = dsInsert then
                    Post;
                  ClearNullData(g_Table_Maintain_Work, szSignName);
                  UpdateBatch(arAll);
                  Requery();
                end;
              end;
            end;
          end;
        1:
          begin
            if Self.tvMaking.DataController.RowCount > 0 then
            begin
              szSignName := Self.tvMakingCol2.DataBinding.FieldName;
              with Self.ADOMaking do
              begin
                if State <> dsInactive then
                begin
                  if State = dsInsert then
                    Post;
                  ClearNullData(g_Table_Maintain_Science, szSignName);
                  UpdateBatch(arAll);
                  Requery();
                end;
              end;
            end;
          end;
        2:
          begin
            if Self.tvSubContract.DataController.RowCount > 0 then
            begin
              szSignName := Self.tvSubContractCol2.DataBinding.FieldName;

              with Self.ADOSubContract do
              begin
                if State <> dsInactive then
                begin
                  if State = dsInsert then
                    Post;
                  ClearNullData(g_Table_Maintain_SubContract, szSignName);
                  UpdateBatch(arAll);
                  Requery();
                end;

              end;
            end;
          end;
      end;
      }
    end;
    IsSave := False;
  end;
end;

procedure TfrmDatum.N2Click(Sender: TObject);
begin
  inherited;
  //打印
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      //考勤
      DM.BasePrinterLink1.Component := Self.Grid;
    end;
    1:
    begin
      //材料
      DM.BasePrinterLink1.Component := Self.MakingGrid;
    end;
    2:
    begin
      //分包
      DM.BasePrinterLink1.Component := Self.cxGrid2;
    end;
    3:
    begin
      //商混
      DM.BasePrinterLink1.Component := Self.GridConcrete;
    end;
  end;
  DM.BasePrinterLink1.ReportTitleText := '';
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmDatum.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  inherited;
  IsSaveData;
end;

procedure TfrmDatum.FormCreate(Sender: TObject);
begin
  inherited;

  if Self.Tv1.Items.Count >= 1 then
  begin
    Self.Tv1.Items[1].Selected := True;
  end;

  Self.RzPageControl1.HideAllTabs;

  with Self.ADOList do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_MakingsList;
    Open;
  end;

end;

procedure TfrmDatum.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 27 then
  begin
    Close;
  end;
end;

procedure TreeAddName(Tv: TTreeView); //树增加名称
var
  Node, ChildNode: TTreeNode;
begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin
      ChildNode := Items.AddChild(Node, '请输入名称');
      ChildNode.Expand(True);
      ChildNode.Selected := True;
      ChildNode.EditText;
    end;
  end;

end;

procedure TfrmDatum.FormShow(Sender: TObject);
begin
  //
  Self.Tv1.FullExpand;
end;

procedure TfrmDatum.RzToolButton15Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmDatum.RzToolButton1Click(Sender: TObject);
var
  s : string;
  SqlText : string;

begin
  inherited;
  s := Self.cxLookupComboBox1.Text;
  Self.RzPanel3.Caption := s;

  g_ProjectName := s;
  if Length(s) <> 0 then
  begin
      SqlText := ' Where ProjectName="' + s + '"';
      case Self.RzPageControl1.ActivePageIndex of
        0:
          begin
            with Self.ADOWork do
            begin
              Close;
              SQL.Clear;
              SQL.Text := 'Select * from ' + g_Table_Maintain_Work + SqlText;
              Open;

            end;
          end;
        1:
          begin
            with Self.ADOMaking do
            begin
              Close;
              SQL.Clear;
              SQL.Text := 'Select * from ' + g_Table_Maintain_Science + SqlText;
              Open;
            end;

          end;
        2:
          begin
            with Self.ADOSubContract do
            begin
              Close;
              SQL.Clear;
              SQL.Text := 'Select * from ' + g_Table_Maintain_SubContract + SqlText;
              Open;
            end;

          end;
        3:
          begin
            with Self.ADOConcrete do
            begin
              Close;
              SQL.Clear;
              SQL.Text := 'Select * from ' + g_Table_Maintain_Concrete + SqlText;
              Open;
            end;
          end;
      end;
  end;

end;

procedure TfrmDatum.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      //考勤
      ADOIsEdit(True,Self.ADOWork);
    end;
    1:
    begin
      //材料
      ADOIsEdit(True,Self.ADOMaking);
    end;
    2:
    begin
      //分包
      ADOIsEdit(True,Self.ADOSubContract);
    end;
    3:
    begin
      //商混
      ADOIsEdit(True,Self.ADOConcrete);
    end;
  end;
end;

procedure TfrmDatum.RzToolButton3Click(Sender: TObject);
var
  Node: TTreeNode;
begin
  Node := Self.Tv1.Selected;
  if Assigned(Node) then
  begin
    if Node.Level = 0 then
    begin
      TreeAddName(Self.Tv1);
    end
    else
    begin
      Application.MessageBox('只允许建立顶级分类!', '提示', MB_OKCANCEL + MB_ICONWARNING);
    end;
  end;
end;

procedure TfrmDatum.RzToolButton7Click(Sender: TObject);
var
  szRowIndex: Integer;
  szQry: TADOQuery;
  s: string;
  i: Integer;
  Id: string;
  szRowCount: Integer;
begin
  inherited;
  if Length(g_ProjectName) <> 0 then
  begin
      case Self.RzPageControl1.ActivePageIndex of
        0:
          begin
          //考勤
            if Sender = Self.RzToolButton7 then
            begin

              try
                with Self.ADOWork do
                begin
                  if State <> dsInactive then
                  begin
                    Append;
                    Self.tvWork.Columns[1].Focused := True;
                    Self.Grid.SetFocus;
                    Inc(g_NewRowCount);
                    IsSave := True;
                    keybd_event(VK_RETURN, 0, 0, 0);
                  end;

                end
              except

              end;

            end
            else if Sender = Self.RzToolButton8 then
            begin
              IsDeleteEmptyData(Self.ADOWork, g_Table_Maintain_Work, Self.tvWorkCol2.DataBinding.FieldName);
              IsSave := False;
            end
            else if Sender = Self.RzToolButton9 then
            begin
              IsDelete(Self.tvWork);
            //  IsDeleteData(Self.tvWork, '', g_Table_Maintain_Work, Self.tvWorkColumn1.DataBinding.FieldName);
            //  Self.ADOWork.Requery();
          //  if Not IsDelete(self.tvwork) then
            end;
          end;
        1:
          begin
          //材料
            if Sender = Self.RzToolButton7 then
            begin
              with Self.ADOMaking do
              begin
                if State <> dsInactive then
                begin
                  Append;
                  Self.tvMaking.Columns[1].Focused := True;
                  Self.MakingGrid.SetFocus;
                  Inc(g_NewRowCount);
                  IsSave := True;
                  keybd_event(VK_RETURN, 0, 0, 0);
                end;
              end;
            end
            else if Sender = Self.RzToolButton8 then
            begin
            //材料保存
              IsDeleteEmptyData(Self.ADOMaking, g_Table_Maintain_Science, Self.tvMakingCol2.DataBinding.FieldName);
              IsSave := False;
            end
            else if Sender = Self.RzToolButton9 then
            begin
              IsDelete(Self.tvMaking);
          //  if Not IsDelete(Self.tvMaking) then
            //  IsDeleteData(Self.tvMaking, '', g_Table_Maintain_Science, Self.tvMakingCol11.DataBinding.FieldName);
            //  Self.ADOMaking.Requery();
            end;
          end;
        2:
          begin
          //分包
            if Sender = Self.RzToolButton7 then
            begin
              with Self.ADOSubContract do
              begin
                if State <> dsInactive then
                begin
                  Append;
                  Self.tvSubContractCol2.FocusWithSelection;
                  Self.cxGrid2.SetFocus;
                  Inc(g_NewRowCount);
                  IsSave := True;
                  keybd_event(VK_RETURN, 0, 0, 0);
                end;
              end;

            end
            else if Sender = Self.RzToolButton8 then
            begin
              IsDeleteEmptyData(Self.ADOSubContract, g_Table_Maintain_SubContract, Self.tvSubContractCol2.DataBinding.FieldName);
              IsSave := False;
            end
            else if Sender = Self.RzToolButton9 then
            begin
              IsDelete(Self.tvSubContract);
          //  if not IsDelete(Self.tvSubContract) then
              {
              IsDeleteData(Self.tvSubContract, '', g_Table_Maintain_SubContract, Self.tvSubContractColumn1.DataBinding.FieldName);
              Self.ADOSubContract.Requery();
              }
            end;
          end;
        3:
          begin
            if Sender = Self.RzToolButton7 then
            begin
              with Self.ADOConcrete do
              begin
                if State <> dsInactive then
                begin
                  Append;

                  Self.tvConcreteCol2.FocusWithSelection;
                  Self.GridConcrete.SetFocus;
                  Inc(g_NewRowCount);
                  IsSave := True;
                  keybd_event(VK_RETURN, 0, 0, 0);
                end;
              end;

            end else
            if Sender= Self.RzToolButton8 then
            begin
              IsDeleteEmptyData(Self.ADOConcrete, g_Table_Maintain_Concrete , Self.tvConcreteCol2.DataBinding.FieldName);
              IsSave := False;
            end else
            if Sender = Self.RzToolButton9 then
            begin
              IsDelete(Self.tvConcrete);
            end;

          end;
      end;
  end else
  begin
    ShowMessage('请选择一个工程名称在进行新增合同内容!!');
    Self.cxLookupComboBox1.SetFocus;
  end;
end;

procedure TfrmDatum.Tv1Change(Sender: TObject; Node: TTreeNode);
begin
  inherited;
  if Assigned(Node) then
  begin
    if Node.Level = 1 then
    begin

      case Node.Index of
        0: Self.RzPageControl1.ActivePage := Self.TabSheet1;
        2: Self.RzPageControl1.ActivePage := Self.TabSheet3;
        3: Self.RzPageControl1.ActivePage := Self.TabSheet4;
      end;

      if (Node.Index = 1) or (Node.Index > 4) then
        Self.RzPageControl1.ActivePage := Self.TabSheet2;

      Self.cxLookupComboBox1PropertiesChange(Sender);
    end;

  end;

end;

procedure TfrmDatum.Tv1Editing(Sender: TObject; Node: TTreeNode; var AllowEdit: Boolean);
begin
  if Node.Index < 3 then
  begin
    AllowEdit := False;
  end;
end;

procedure TfrmDatum.tvMakingCol3PropertiesCloseUp(Sender: TObject);
var
  szCol1Name: Variant;
  szCol2Name: Variant;
  szCol3Name: Variant;
  szCol4Name: Variant;
  szRowIndex: Integer;
begin
  inherited;
  with (Sender as TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin

      szCol1Name := DataController.Values[szRowIndex, 0];
      szCol2Name := DataController.Values[szRowIndex, 1];
      szCol3Name := DataController.Values[szRowIndex, 2];
      szCol4Name := DataController.Values[szRowIndex, 3];

      /////////////////////////////////////////////////
      with Self.ADOMaking do
      begin
        if State <> dsInactive then
        begin
          FieldByName(Self.tvMakingCol3.DataBinding.FieldName).Value := szCol1Name;
          FieldByName(Self.tvMakingCol5.DataBinding.FieldName).Value := szCol2Name;
          FieldByName(Self.tvMakingCol6.DataBinding.FieldName).Value := szCol3Name;
          FieldByName(Self.tvMakingCol7.DataBinding.FieldName).Value := szCol4Name;
        end;
      end;
      /////////////////////////////////////////////////

    end;

  end;

end;

procedure TfrmDatum.tvMakingCol3PropertiesPopup(Sender: TObject);
begin
  inherited;
  with Self.ADOMaking do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsInsert) or (State <> dsEdit) then
      begin
        Edit;
      end;

    end;

  end;

end;

procedure TfrmDatum.tvMakingCol8GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;

begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;
end;

procedure TfrmDatum.tvMakingEditing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := ADOBanEditing(Self.ADOMaking);
  if AItem.Index = Self.tvMakingCol1.Index then AAllow := False;
end;

procedure TfrmDatum.tvMakingEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvMakingCol9.Index) then
  begin
    if Self.tvMaking.Controller.FocusedRow.IsLast and IsNewRow then
    begin

      with Self.ADOMaking do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvMakingCol1.FocusWithSelection;
        end;
      end;

    end;

  end;

end;

procedure TfrmDatum.tvMakingEditValueChanged(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
begin
  inherited;
  IsSave := True;
end;

procedure TfrmDatum.tvMakingKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton9.Click;
  end;
end;

procedure TfrmDatum.tvSubContractCol4GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetTableMaintain(AProperties,g_Table_Maintain_WorkType);
  {
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from ' + g_Table_Maintain_WorkType;
  DM.Qry.Open;
  if DM.Qry.RecordCount <> 0 then
  begin
    with (AProperties AS TcxComboBoxProperties) do
    begin
      Items.Clear;
      while Not DM.Qry.Eof do
      begin
        Items.Add(DM.Qry.Fields[1].AsString);
        DM.Qry.Next;

      end;

    end;

  end;
  }
end;

procedure TfrmDatum.tvSubContractCol7GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;

begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;
end;
procedure TfrmDatum.tvSubContractEditing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := ADOBanEditing(Self.ADOSubContract);
  if AItem.Index = Self.tvSubContractCol1.Index then AAllow := False;

end;

procedure TfrmDatum.tvSubContractEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvSubContractCol8.Index) then
  begin
    if Self.tvSubContract.Controller.FocusedRow.IsLast then
    begin
      with Self.ADOSubContract do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvSubContract.Columns[0].Focused := True;
        end;

      end;
    end;

  end;

end;

procedure TfrmDatum.tvSubContractEditValueChanged(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
begin
  inherited;
  IsSave := True;
end;

procedure TfrmDatum.tvSubContractKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton9.Click;
  end;
end;

procedure TfrmDatum.tvWorkCol1GetDisplayText(Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord; var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmDatum.tvWorkCol4GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('男');
    Items.Add('女')
  end;
end;

procedure TfrmDatum.tvWorkCol5GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetTableMaintain(AProperties,g_Table_Maintain_section);
  {
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from ' + g_Table_Maintain_section;
  DM.Qry.Open;
  if DM.Qry.RecordCount <> 0 then
  begin
    with (AProperties AS TcxComboBoxProperties) do
    begin
      Items.Clear;
      while Not DM.Qry.Eof do
      begin
        Items.Add(DM.Qry.Fields[1].AsString);
        DM.Qry.Next;

      end;

    end;

  end;
  }
end;

procedure TfrmDatum.tvWorkCol6GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetTableMaintain(AProperties,g_Table_Maintain_Job);
end;

procedure TfrmDatum.tvWorkEditing(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := ADOBanEditing(Self.ADOWork);
  if AItem.Index = Self.tvWorkCol1.Index then AAllow := False;
end;

procedure TfrmDatum.tvWorkEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    if Self.tvWork.Controller.FocusedRow.IsLast then
    begin
      if AItem.Index = Self.tvWorkCol8.Index then
      begin
        with Self.ADOWork do
        begin

          if State <> dsInactive then
          begin
            Append;
            Self.tvWork.Columns[0].Focused := True;
          end;
        end;

      end;
    end;
  end;
end;

procedure TfrmDatum.tvWorkEditValueChanged(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
begin
  inherited;
  IsSave := True;
end;

procedure TfrmDatum.tvWorkKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton9.Click;
  end;
end;

end.


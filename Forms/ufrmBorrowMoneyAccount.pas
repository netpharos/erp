unit ufrmBorrowMoneyAccount;
 //�����Ŀ
interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, RzButton, ExtCtrls, DB, ADODB,TreeFillThrd,TreeUtils,
  ImgList;

type
  TfrmBorrowMoneyAccount = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Label2: TLabel;
    RzBitBtn6: TRzBitBtn;
    RzBitBtn7: TRzBitBtn;
    RzBitBtn8: TRzBitBtn;
    Edit2: TEdit;
    RzBitBtn9: TRzBitBtn;
    RzBitBtn10: TRzBitBtn;
    GroupBox2: TGroupBox;
    Panel2: TPanel;
    Label1: TLabel;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    Edit1: TEdit;
    RzBitBtn4: TRzBitBtn;
    RzBitBtn5: TRzBitBtn;
    ListView1: TListView;
    ListView2: TListView;
    il1: TImageList;
    Qry: TADOQuery;
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView2DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn4Click(Sender: TObject);
    procedure RzBitBtn9Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzBitBtn8Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure RzBitBtn10Click(Sender: TObject);
  private
    { Private declarations }

    procedure DeleteListView(List : TListView; lptype : Byte);
  public
    { Public declarations }
    procedure ShowListView(List : TListView ; AIndex : Byte ; AName,ATableName  : string ; AType : Byte );
  end;

var
  frmBorrowMoneyAccount: TfrmBorrowMoneyAccount;

implementation

uses
  ufrmBorrowMoney,uDataModule,ufrmPostAccounts,global,ufunctions;

{$R *.dfm}

procedure TfrmBorrowMoneyAccount.ShowListView(List : TListView ; AIndex : Byte ; AName,ATableName : string ; AType : Byte );
var
  Query: TADOQuery;
  PNode: PNodeData;
  ListItem : TListItem;
  szType : Integer;
  
begin
  Query := TADOQuery.Create(nil);
  try

      Query.Connection := DM.ADOconn;
      case AIndex of
        0:
        begin
          Query.SQL.Text := 'Select * from ' + ATableName + ' where ' + 'PID' + ' = 0' ;
        end;
        1:
        begin
          Query.SQL.Text := 'Select * from ' + ATableName + ' where ' + 'PID' + ' = 0 and name like "%'+ AName +'%"';
        end;
      end;
      OutputLog(Query.SQL.Text);
      if Query.Active then
         Query.Close;

      Query.Open;

      List.Clear;
      while not Query.Eof do
      begin
        New(PNode);
        PNode^.Caption    := Query.FieldByName('name').AsString;
        PNode^.parent     := Query.FieldByName('parent').AsInteger;
        PNode^.Code       := Query.FieldByName('Code').AsString;
        PNode^.Input_time := Query.FieldByName('Input_time').AsDateTime;
        PNode^.End_time   := Query.FieldByName('End_time').AsDateTime;
        PNode^.remarks    := Query.FieldByName('remarks').AsString;
        szType := Query.FieldByName('PatType').AsInteger;
        if szType = AType then
        begin
          ListItem := List.Items.Add ;
          ListItem.Data := PNode;
          ListItem.Caption := PNode.Code;
          ListItem.SubItems.Add(PNode^.Caption);
          ListItem.SubItems.Add(FormatDateTime('yyyy-MM-dd',PNode^.Input_time));
          ListItem.SubItems.Add(PNode^.remarks);
        end;

        Query.Next;
      end;

  finally
    Query.Free;
  end;
end;

procedure TfrmBorrowMoneyAccount.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn4.Click;
  end;
end;

procedure TfrmBorrowMoneyAccount.Edit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn9.Click;
  end; 
end;

procedure TfrmBorrowMoneyAccount.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Close;
  end;  
end;

procedure TfrmBorrowMoneyAccount.FormShow(Sender: TObject);
begin
  g_TableName_Borrow  := 'sk_BorrowMoney_Tree';
  g_TableName_Loan    := 'sk_Loan_Tree' ;

  ShowListView(Self.ListView1,0,'',g_TableName_Borrow,1);
  ShowListView(Self.ListView2,0,'',g_TableName_Loan,2);
end;

procedure TfrmBorrowMoneyAccount.ListView1DblClick(Sender: TObject);


procedure TfrmBorrowMoneyAccount.ListView2DblClick(Sender: TObject);


procedure TfrmBorrowMoneyAccount.RzBitBtn10Click(Sender: TObject);


procedure TfrmBorrowMoneyAccount.RzBitBtn1Click(Sender: TObject);


procedure TfrmBorrowMoneyAccount.RzBitBtn8Click(Sender: TObject);
begin

end;

procedure TfrmBorrowMoneyAccount.RzBitBtn3Click(Sender: TObject);
begin

end;

procedure TfrmBorrowMoneyAccount.RzBitBtn4Click(Sender: TObject);


procedure TfrmBorrowMoneyAccount.RzBitBtn5Click(Sender: TObject);


procedure TfrmBorrowMoneyAccount.RzBitBtn9Click(Sender: TObject);


end.

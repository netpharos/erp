unit ufrmCompanyManage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxTextEdit, cxLabel, RzButton, RzPanel,
  Vcl.ComCtrls, Vcl.ExtCtrls, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer,
  cxMaskEdit, cxDBTL, cxTLData, Data.Win.ADODB, Vcl.StdCtrls,UtilsTree, TreeUtils,
  cxDropDownEdit,ufrmBaseController, cxCheckBox, Vcl.Menus;

type
  TfrmCompanyManage = class(TfrmBaseController)
    RzPanel4: TRzPanel;
    Tv: TTreeView;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzToolButton2: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzSpacer23: TRzSpacer;
    RzToolButton25: TRzToolButton;
    RzSpacer24: TRzSpacer;
    RzToolButton26: TRzToolButton;
    RzPanel6: TRzPanel;
    RzToolbar4: TRzToolbar;
    RzSpacer6: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzToolButton11: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzToolButton13: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzSpacer11: TRzSpacer;
    RzSpacer12: TRzSpacer;
    RzToolButton14: TRzToolButton;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvCol1: TcxGridDBColumn;
    tvCol3: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzPanel5: TRzPanel;
    Splitter2: TSplitter;
    StatusBar1: TStatusBar;
    tvCol5: TcxGridDBColumn;
    qry: TADOQuery;
    ds: TDataSource;
    ADOTree: TADOQuery;
    DataTree: TDataSource;
    tvCol6: TcxGridDBColumn;
    tvCol7: TcxGridDBColumn;
    RzSpacer3: TRzSpacer;
    tvCol2: TcxGridDBColumn;
    tvCol4: TcxGridDBColumn;
    RzSpacer4: TRzSpacer;
    RzToolButton3: TRzToolButton;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    RzSpacer5: TRzSpacer;
    RzSpacer9: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton5: TRzToolButton;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton10Click(Sender: TObject);
    procedure TvCancelEdit(Sender: TObject; Node: TTreeNode);
    procedure TvKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure TvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure RzToolButton6Click(Sender: TObject);
    procedure TvDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure TvDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure RzToolButton25Click(Sender: TObject);
    procedure RzToolButton26Click(Sender: TObject);
    procedure tvCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure qryAfterEdit(DataSet: TDataSet);
    procedure RzToolButton14Click(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tvViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure TvChange(Sender: TObject; Node: TTreeNode);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvCol3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure RzToolButton3Click(Sender: TObject);
    procedure tvViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure qryAfterInsert(DataSet: TDataSet);
    procedure RzToolButton11Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvViewDblClick(Sender: TObject);
    procedure qryAfterOpen(DataSet: TDataSet);
    procedure RzToolButton4Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure qryAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    g_TreeView : TNewUtilsTree;
    g_IsDataPost : Boolean;
    g_DataIndex  : Integer;
    Prefix : string;
    Suffix : string;
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
    procedure TreeAddName(Tv : TTreeView); //树增加名称
    procedure UpDateIndex(lpNode : TTreeNode);
  public
    { Public declarations }
    dwIsReadonly : Boolean;
    dwSignName : Variant;
  end;

var
  frmCompanyManage: TfrmCompanyManage;
  g_TopNode  : TTreeNode;
  g_DirCompany : string = 'MakingsCompany';

implementation

uses
   uDataModule , global , FillThrdTree , CreateOrderNum ;

{$R *.dfm}

procedure TfrmCompanyManage.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  S := 'UPDATE ' + g_Table_CompanyTree + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    ExecSQL;
  end;
end;

procedure TfrmCompanyManage.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    Self.RzToolButton14.Click;
  end;
end;

procedure TfrmCompanyManage.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'往来单位明细表');
end;

procedure TfrmCompanyManage.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if g_IsDataPost then
  begin
    if MessageBox(handle, PChar('有数据未保存,现在是否要保存? 如不保存本次新增或编辑的数据将丢失!'),'提示',
       MB_ICONQUESTION + MB_YESNO) = IDYES then
    begin
      Self.qry.UpdateBatch(arAll);
    end;
  end;

end;

procedure TfrmCompanyManage.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 27  then
  begin
    Close;
  end;
end;

procedure TfrmCompanyManage.FormShow(Sender: TObject);
begin
  g_TreeView := TNewUtilsTree.Create(Self.tv,DM.ADOconn,g_Table_CompanyTree,'往来单位');
  g_TreeView.GetTreeTable(0);
  Self.Tv.FullExpand;
  Prefix := 'JY-';
  Suffix := '100000';
end;

procedure TfrmCompanyManage.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  s : string;

begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin
      s := GetNewBHStr(g_Table_CompanyTree,'ID','SIGN',10);
      g_TreeView.AddChildNode(s,'',Date,m_OutText);
    end;
  end;

end;

function TfrmCompanyManage.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    if Assigned(Node) then
    begin
      treeCldnode(Node,szCodeList);
      g_TreeView.DeleteTree(Node);
    end;

  end else
  begin
    g_TreeView.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;

procedure TfrmCompanyManage.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '';
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmCompanyManage.qryAfterEdit(DataSet: TDataSet);
begin
  if not DataSet.IsEmpty then
  begin
  //  Self.StatusBar1.Panels[0].Text := '有数据';
    g_IsDataPost := True;
  end;  
   
end;

procedure TfrmCompanyManage.qryAfterInsert(DataSet: TDataSet);
var
  s : string;
//  szColName : string;

begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      s := Prefix + GetUserCode(g_Table_CompanyManage,10000);
      FieldByName( Self.tvCol4.DataBinding.FieldName ).Value := s;
      FieldByName(m_Sign_Id).Value := g_DataIndex;
      FieldByName(Self.tvCol6.DataBinding.FieldName).Value := True;
    //  s := 'WL-' + GetRowCode(g_Table_CompanyManage,100000);
    //  szColName := Self.tvViewColumn4.DataBinding.FieldName;
    //  FieldByName(szColName).Value := s;
    end;

  end;

end;

procedure TfrmCompanyManage.qryAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0
end;

procedure TfrmCompanyManage.qryAfterScroll(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    if Self.tvView.Controller.SelectedRowCount = 1 then
    begin
      Self.StatusBar1.Panels[0].Text := FieldByName(tvCol3.DataBinding.FieldName).AsString;
    end;

  end;
end;

procedure TfrmCompanyManage.TvCancelEdit(Sender: TObject; Node: TTreeNode);
begin
  IstreeEdit(Node.Text,Node);
end;

procedure TfrmCompanyManage.TvChange(Sender: TObject; Node: TTreeNode);
var
  I: Integer;
  szData : PNodeData;
  szCodeId : Integer;
begin
  inherited;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin
      szData := PNodeData(Node.Data);
      if Assigned(szData) then
      begin
        szCodeId := szData^.Index;
        g_DataIndex := szData.Index;
        with Self.qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from ' + g_Table_CompanyManage + ' WHERE ' + m_Sign_Id + '=' + IntToStr(szCodeId) ;
          Open;
        end;

      end;

    end;

  end;

end;

procedure TfrmCompanyManage.TvDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  TargetNode, SourceNode: TTreeNode;

begin
  TargetNode := tv.DropTarget;
  SourceNode := tv.Selected;
  if MessageBox(handle, '您确认要移动合同吗？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
    if g_TreeView.ModifyNodePID(PNodeData(TargetNode.Data)^.Index, SourceNode) then
       g_TreeView.GetTreeTable(0);

end;

procedure TfrmCompanyManage.TvDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  if (Source = Self.Tv) then
     Accept := True
  else
     Accept := False;
end;

procedure TfrmCompanyManage.TvEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  IstreeEdit(s,Node);
end;

procedure TfrmCompanyManage.TvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.Tv.Selected.Parent.Selected := True;
    Self.RzToolButton1.Click;
  end else
  if Key = 46 then
  begin
    Self.RzToolButton6.Click;
  end;
end;

procedure TfrmCompanyManage.tvCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmCompanyManage.tvCol3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;
  s := GetPyCode(DisplayValue);
  with Self.qry do
  begin
    if State <> dsInactive then
    begin
      Edit;
      FieldByName(Self.tvCol3.DataBinding.FieldName).Value := DisplayValue;
      FieldByName(Self.tvCol5.DataBinding.FieldName).Value := s;
    end;
  end;

end;

procedure TfrmCompanyManage.tvViewDblClick(Sender: TObject);
var
  szDir : string;
  szCode: string;
  szColName : string;

begin
//增加附件
  inherited;
  if not dwIsReadonly then
  begin
    szDir := Concat(g_Resources,'\'+ g_DirCompany);
    with Self.tvView.DataController.DataSource.DataSet do
    begin
      if not IsEmpty then
      begin
        szColName := Self.tvCol3.DataBinding.FieldName;
        szCode := FieldByName(szColName).AsString;
        CreateOpenDir(Handle,Concat(szDir,'\' + szCode),True);
      end;

    end;

  end else
  begin
    dwSignName := Self.tvCol3.EditValue;
    Close;
  end;

end;

procedure TfrmCompanyManage.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := ADOBanEditing(Self.qry);
  if AItem.Index = Self.tvCol1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmCompanyManage.tvViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    if AItem.Index = Self.tvCol3.Index then
    begin
      if Self.tvView.Controller.FocusedRow.IsLast then
      begin
        with Self.qry do
        begin
          Append;
          Self.tvView.Columns[3].Focused := True;
        end;
      end;

    end;

  end;

end;

procedure TfrmCompanyManage.tvViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton13.Click;
  end;
end;

procedure TfrmCompanyManage.RzToolButton10Click(Sender: TObject);
var
  Node : TTreeNode;
  szData : PNodeData;

begin
  if Sender = Self.RzToolButton10 then
  begin
    Node:= Self.Tv.Selected;
    if Assigned(Node) then
    begin
      szData := PNodeData(Node.Data);
      if Assigned(szData) then
      begin
        with Self.qry do
        begin
          if State <> dsInactive then
          begin
            Append;

            Self.tvView.Columns[3].Focused := True;
            Self.Grid.SetFocus;
            keybd_event(VK_RETURN,0,0,0);
          end else
          begin
            Application.MessageBox( '当前状态无效，单击分类选项在进行操作!', '提示:',
                   MB_OKCANCEL + MB_ICONWARNING)
          end;
        end;
      end;
    end;

  end else
  if Sender = Self.RzToolButton13 then
  begin
    //删除
  //  IsDelete(Self.tvView);

    DeleteSelection(Self.tvView,Concat(g_Resources,'\'+ g_DirCompany),g_Table_CompanyManage,
    Self.tvCol3.DataBinding.FieldName);
    Self.qry.Requery();
  end;
end;

procedure TfrmCompanyManage.RzToolButton11Click(Sender: TObject);
begin
  inherited;
//  Self.qry.UpdateBatch(arAll);
//  IsDeleteEmptyData(Self.qry ,'',Self.tvCol3.DataBinding.FieldName);
  IsDeleteEmptyData(Self.qry ,
                    g_Table_CompanyManage ,
                    Self.tvCol3.DataBinding.FieldName);
  g_IsDataPost := False;
end;

procedure TfrmCompanyManage.RzToolButton14Click(Sender: TObject);
begin
  inherited;
  with Self.qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_CompanyManage + ' WHERE ' + Self.tvCol3.DataBinding.FieldName + ' Like "%' + Self.cxTextEdit1.Text + '%"';
    Open;
  end;

end;

procedure TfrmCompanyManage.RzToolButton1Click(Sender: TObject);
begin
  TreeAddName(Self.tv);
end;

procedure TfrmCompanyManage.RzToolButton25Click(Sender: TObject);
var
  PrevNode , NextNode , SourceNode , TargetNode : TTreeNode;
  S : string;
begin
  PrevNode   := Tv.Selected.getPrevSibling;
  SourceNode := Tv.Selected;
  if (PrevNode.Index <> -1) then
  begin
    NextNode   := Tv.Selected.getNextSibling;
    if NextNode <> nil then
      PrevNode.MoveTo(NextNode, naInsert)
    else
      PrevNode.MoveTo(SourceNode, naAdd);
  end;
  SourceNode := Tv.Selected; //中
  UpDateIndex(SourceNode);
//  PrevNode   := Tv.Selected.getPrevSibling;//上
//  UpDateIndex(PrevNode);
  NextNode   := Tv.Selected.getNextSibling;//下
  UpDateIndex(NextNode);
end;

procedure TfrmCompanyManage.RzToolButton26Click(Sender: TObject);
var
  OldNode , Node , SourceNode ,PrevNode: TTreeNode;
begin
  Node := Self.Tv.Selected;
  OldNode := Node.getNextSibling;
  if OldNode.Index <> -1 then
  begin
  //  ShowMessage('Index:' + IntToStr(OldNode.Index));
    OldNode.MoveTo(Node, naInsert);

    SourceNode := Tv.Selected; //中
    UpDateIndex(SourceNode);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    UpDateIndex(PrevNode);
  //  NextNode   := Tv.Selected.getNextSibling;//下
  //  UpDateIndex(NextNode);
  end;
end;

procedure TfrmCompanyManage.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  Self.Tv.Selected.EditText;
end;

procedure TfrmCompanyManage.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmCompanyManage.RzToolButton4Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit(true,Self.qry);
end;

function InDeleteData(ATable,code : string):Integer;stdcall;
var
  s : string;
begin
  if Length(ATable) <>  0 then
  begin
    s := 'delete * from '+ ATable +' where Sign_Id in('+ Code  +')';
    with  DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Result:= ExecSQL;
    end;

  end;

end;

procedure TfrmCompanyManage.RzToolButton6Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;

begin
  if MessageBox(handle, '是否删除分类管理节点？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
  begin
      Node := Self.Tv.Selected;
      if Assigned(Node) then
      begin
      //  GetTreeIndexList(Node,szCodeList);

        GetIndexList(Node,szCodeList);
        InDeleteData(g_Table_CompanyManage,szCodeList);
        g_TreeView.DeleteTree(Node);
        Self.qry.Requery();

      end;
  end;

end;

end.

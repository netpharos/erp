unit ufrmEnterStorage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxGroupBox, RzButton, cxLabel,
  cxTextEdit, Vcl.ComCtrls, dxCore, cxDateUtils, cxMaskEdit, cxDropDownEdit,
  cxCalendar, cxSpinEdit, cxMemo;

type
  TfrmEnterStorage = class(TForm)
    cxGroupBox1: TcxGroupBox;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxComboBox1: TcxComboBox;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxLabel6: TcxLabel;
    cxTextEdit3: TcxTextEdit;
    cxLabel7: TcxLabel;
    cxTextEdit4: TcxTextEdit;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxMemo1: TcxMemo;
    cxLabel10: TcxLabel;
    cxComboBox2: TcxComboBox;
    cxComboBox3: TcxComboBox;
    procedure RzBitBtn1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_PostCode    : string;
    g_ProjectDir  : string;
    g_PostNumbers : string;
    g_TableName   : string;
    g_IsModify    : Boolean;
    g_Prefix      : string;
  end;

var
  frmEnterStorage: TfrmEnterStorage;

implementation

uses
   uDataModule,global;

{$R *.dfm}


function GetUnitList(List : TcxComboBox ; chType : Integer):Boolean;stdcall;
var
  name , Remarks , id : string;
  ListItem : TListItem;
  i : Integer;
  s : string;

begin
  if m_user.dwType = 0  then
  begin
      DM.Qry.Close;
      DM.Qry.SQL.Clear;
      DM.Qry.SQL.Text := 'Select * from unit where chType=' + IntToStr(chType);
      DM.Qry.Open;
      i:=0;

      if not DM.Qry.IsEmpty then
      begin
        List.Clear;
        while not DM.Qry.Eof do
        begin
          name := DM.Qry.FieldByName('name').AsString;
          Inc(i);
          //有所有权限
          List.Properties.Items.Add(name);
          DM.Qry.Next;
        end;
      end;

  end;

end;

procedure TfrmEnterStorage.FormCreate(Sender: TObject);
var
  i : Integer;

begin
  GetUnitList(Self.cxComboBox1,7);
  GetUnitList(Self.cxComboBox3,0);

  i := Self.cxComboBox3.Properties.Items.IndexOf('根');
  if i < 0 then  i := 0;
  Self.cxComboBox3.ItemIndex := i;
end;

procedure TfrmEnterStorage.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    13:
    begin
      Self.RzBitBtn1.Click;
    end;
    27:
    begin
      Self.RzBitBtn2.Click;
    end;
  end;

end;

procedure TfrmEnterStorage.RzBitBtn1Click(Sender: TObject);
var
  szCaption : string;
  szSupplyUnit : string; //供应单位
  szDates   : string;     //日期
  szUnit    : string;    //单位
  szNum     : Integer;   //数量
  szDelivery: string;    //送货人
  szSign    : string;    //签收人
  szRemarks : string;    //备注
  sqlstr    : string;    //Sql语句
  szTypes   : string;    //制单类型
  szUnitTypes : string;  //单位类别

begin
//工程名称、供货单位、收货人、签收人可以做成维护 , 用的时候可以选择
  szTypes := Self.cxComboBox2.Text;
  if Self.cxComboBox2.ItemIndex < 0 then
  begin
    //制单类别
    Self.cxComboBox2.DroppedDown := True;
    Application.MessageBox('请选择制单类型！', '提示' , MB_OK + MB_ICONINFORMATION);
  end else
  begin
      szCaption := Self.cxTextEdit1.Text;
      if Length(szCaption)  = 0 then
      begin
        //工程名称
        Self.cxTextEdit1.SetFocus;
        Application.MessageBox('工程名称不可为空!', '提示'  ,MB_OK + MB_ICONINFORMATION);
      end else
      begin
        szSupplyUnit := Self.cxTextEdit2.Text;
        if Length(szSupplyUnit) = 0 then
        begin
          //供应单位
          Self.cxTextEdit2.SetFocus;
          Application.MessageBox('供应单位不可为空！' , '提示' , MB_OK + MB_ICONINFORMATION);
        end else
        begin
          szUnit := Self.cxComboBox3.Text ;
          if Length(szUnit) = 0 then
          begin
            //单位类别

            Self.cxComboBox3.SetFocus;
            Application.MessageBox('请选单位类别！', '提示' ,MB_OK + MB_ICONINFORMATION);
          end else
          begin
            szNum := Self.cxSpinEdit1.Value;
            if szNum = 0 then
            begin
              Self.cxSpinEdit1.SetFocus;
              Application.MessageBox('请输入数量!' , '提示' ,MB_OK + MB_ICONINFORMATION);
            end else
            begin
              szDelivery := Self.cxTextEdit3.Text;
              if Length(szDelivery) = 0 then
              begin
                Application.MessageBox('送货人不能为空!' , '提示' ,MB_OK + MB_ICONINFORMATION);
              end else
              begin
                szSign := Self.cxTextEdit4.Text;
                if Length( szSign) = 0 then
                begin
                  Self.cxTextEdit4.SetFocus;
                  Application.MessageBox('签收人不能为空!' , '提示' ,MB_OK + MB_ICONINFORMATION);

                end else
                begin
                  szRemarks   := Self.cxMemo1.Text;
                  szDates     := Self.cxDateEdit1.Text;
                  szUnitTypes := Self.cxComboBox1.Text;
                  if g_IsModify then
                  begin
                    //修改
                     sqlstr := Format('Update '+ g_tableName +' set Caption ="%s",'+
                                                                   'SupplyUnit="%s",'+
                                                                   'Unit="%s",' +
                                                                   'Num=%d,' +
                                                                   'Delivery="%s",'+
                                                                   'Sign="%s",' +
                                                                   'Dates="%s",'+
                                                                   'Remarks="%s",' +
                                                                   'UnitTypes="%s",' +
                                                                   'Types="%s"' +
                                                                   ' where numbers="%s"', [
                                                                   szCaption,
                                                                   szSupplyUnit,
                                                                   szUnit,
                                                                   szNum,
                                                                   szDelivery,
                                                                   szSign,
                                                                   szDates,
                                                                   szRemarks,
                                                                   szUnitTypes,
                                                                   szTypes,
                                                                   g_PostNumbers
                                                                   ]);
                     OutputLog(sqlstr);
                     with DM.Qry do
                     begin
                       Close;
                       SQL.Clear;
                       SQL.Text := sqlstr;
                       if 0 <> ExecSQL  then
                       begin
                         m_IsModify := True;
                         Application.MessageBox('编辑成功' , '提示' ,MB_OK + MB_ICONINFORMATION);
                       end else
                       begin
                         Application.MessageBox('编辑失败' , '提示' ,MB_OK + MB_ICONINFORMATION);
                       end;
                     end;

                  end else
                  begin
                    //添加
                    sqlstr := Format( 'Insert into '+ g_tableName +' (code,'+
                                                                   'numbers,'      +
                                                                   'Caption,'+
                                                                   'SupplyUnit,'+
                                                                   'Unit,' +
                                                                   'Num,' +
                                                                   'Delivery,'+
                                                                   'Sign,' +
                                                                   'Dates,'+
                                                                   'UnitTypes,'+
                                                                   'Types,'  +
                                                                   'Remarks' +
                                              ') values("%s","%s","%s","%s","%s",%d,"%s","%s","%s","%s","%s","%s")',[
                                                                      g_PostCode,
                                                                      g_PostNumbers,
                                                                      szCaption,
                                                                      szSupplyUnit,
                                                                      szUnit,
                                                                      szNum,
                                                                      szDelivery,
                                                                      szSign,
                                                                      szDates,
                                                                      szUnitTypes,
                                                                      szTypes,
                                                                      szRemarks
                                                                      ] );

                     OutputLog(sqlstr);
                     with DM.Qry do
                     begin
                       Close;
                       SQL.Clear;
                       SQL.Text := sqlstr;
                       if 0 <> ExecSQL  then
                       begin
                         g_PostNumbers := DM.getDataMaxDate(g_TableName);
                         m_IsModify := True;
                         Application.MessageBox('添加成功' , '提示' ,MB_OK + MB_ICONINFORMATION);
                       end else
                       begin
                         Application.MessageBox('添加失败' , '提示' ,MB_OK + MB_ICONINFORMATION);
                       end;
                     end;

                  end;

                end;

              end;

            end;

          end;

        end;

      end;

  end;

end;

procedure TfrmEnterStorage.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

end.

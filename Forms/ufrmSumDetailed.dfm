object frmSumDetailed: TfrmSumDetailed
  Left = 0
  Top = 0
  Caption = #27719#24635#26126#32454
  ClientHeight = 462
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object RzPageControl2: TRzPageControl
    Left = 0
    Top = 0
    Width = 384
    Height = 462
    Hint = ''
    ActivePage = Tab1
    Align = alClient
    BackgroundColor = clBtnFace
    BoldCurrentTab = True
    ButtonColor = 16250613
    Color = clWhite
    UseColoredTabs = True
    HotTrackStyle = htsText
    ParentBackgroundColor = False
    ParentColor = False
    ShowShadow = False
    TabOverlap = -4
    TabHeight = 26
    TabIndex = 0
    TabOrder = 0
    TabStyle = tsSquareCorners
    TabWidth = 80
    OnTabClick = RzPageControl2TabClick
    FixedDimension = 26
    object Tab1: TRzTabSheet
      Color = clWhite
      Caption = #27719#24635#26126#32454
      object Grid: TcxGrid
        Left = 0
        Top = 0
        Width = 382
        Height = 402
        Align = alClient
        TabOrder = 0
        LevelTabs.Style = 8
        LockedStateImageOptions.ShowText = True
        LookAndFeel.NativeStyle = True
        LookAndFeel.SkinName = 'Office2013White'
        RootLevelOptions.DetailTabsPosition = dtpLeft
        object TableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = SumSource
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = #165',0.00;'#165'-,0.00'
              Kind = skSum
              Column = TableView1Column3
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsView.DataRowHeight = 23
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 14
          object TableView1Column1: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = TableView1Column1GetDisplayText
            Width = 40
          end
          object TableView1Column2: TcxGridDBColumn
            Caption = #39033#30446#21517#31216
            DataBinding.FieldName = 'PrjectName'
            Width = 128
          end
          object TableView1Column3: TcxGridDBColumn
            Caption = #24080#30446#24635#20215
            DataBinding.FieldName = 'SumMoney'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 113
          end
        end
        object TableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = PaidSource
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = #165',0.00;'#165'-,0.00'
              Kind = skSum
              Column = TableView2Column2
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsView.DataRowHeight = 23
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 14
          object TableView2Column1: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = TableView1Column1GetDisplayText
            Width = 40
          end
          object TableView2Column3: TcxGridDBColumn
            Caption = #39033#30446#21517#31216
            DataBinding.FieldName = 'PrjectName'
            Width = 119
          end
          object TableView2Column2: TcxGridDBColumn
            Caption = #24050#20184#24635#39069
            DataBinding.FieldName = 'PaidMoney'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 120
          end
        end
        object TableView3: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = RPSource
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = #165',0.00;'#165'-,0.00'
              Kind = skSum
              Column = TableView3Column3
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsView.DataRowHeight = 23
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          object TableView3Column1: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = TableView1Column1GetDisplayText
            Width = 40
          end
          object TableView3Column2: TcxGridDBColumn
            Caption = #39033#30446#21517#31216
            DataBinding.FieldName = 'ProjectName'
            Width = 134
          end
          object TableView3Column3: TcxGridDBColumn
            Caption = #22870#32602#37329#39069
            DataBinding.FieldName = 'RPSumMoney'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Width = 104
          end
        end
        object Lv1: TcxGridLevel
          Caption = #24080#30446#24635#20215
          GridView = TableView1
          Options.DetailTabsPosition = dtpLeft
        end
        object Lv2: TcxGridLevel
          Caption = #24050#20184#25104#26412
          GridView = TableView2
        end
        object Lv3: TcxGridLevel
          Caption = #22870#32602#26126#32454
          GridView = TableView3
          Visible = False
        end
      end
      object RzToolbar4: TRzToolbar
        Left = 0
        Top = 402
        Width = 382
        Height = 29
        Align = alBottom
        AutoStyle = False
        Images = DM.cxImageList1
        BorderInner = fsNone
        BorderOuter = fsNone
        BorderSides = [sdTop]
        BorderWidth = 0
        Caption = #28165#31639
        GradientColorStyle = gcsCustom
        TabOrder = 1
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer17
          RzToolButton8
          RzSpacer12)
        object RzSpacer17: TRzSpacer
          Left = 4
          Top = 2
        end
        object RzToolButton8: TRzToolButton
          Left = 12
          Top = 2
          Width = 70
          SelectionColorStop = 16119543
          DropDownMenu = pm
          ImageIndex = 5
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          ToolStyle = tsDropDown
          Caption = #25253#34920
        end
        object RzSpacer12: TRzSpacer
          Left = 82
          Top = 2
        end
      end
    end
    object Tab2: TRzTabSheet
      Color = clWhite
      TabVisible = False
      Caption = #27719#24635#22270#34920
      object DBChart1: TDBChart
        Left = 0
        Top = 0
        Width = 382
        Height = 431
        Border.Visible = True
        Title.ClipText = False
        Title.Text.Strings = (
          #24080#30446#24635#20215)
        Title.Visible = False
        Legend.Visible = False
        Panning.MouseWheel = pmwNone
        View3DOptions.Elevation = 315
        View3DOptions.Orthogonal = False
        View3DOptions.Perspective = 0
        View3DOptions.Rotation = 360
        ZoomWheel = pmwNormal
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        DefaultCanvas = 'TGDIPlusCanvas'
        PrintMargins = (
          15
          22
          15
          22)
        ColorPaletteIndex = 19
        object Series2: TPieSeries
          Marks.Frame.Visible = False
          Marks.Style = smsLabelPercentValue
          Marks.Callout.Length = 20
          DataSource = ChartData
          Title = #27719#24635#22270#34920
          XLabelsSource = 'ProjectName'
          XValues.Order = loAscending
          YValues.Name = 'Pie'
          YValues.Order = loNone
          YValues.ValueSource = 'SumMoney'
          Frame.InnerBrush.BackColor = clRed
          Frame.InnerBrush.Gradient.EndColor = clGray
          Frame.InnerBrush.Gradient.MidColor = clWhite
          Frame.InnerBrush.Gradient.StartColor = 4210752
          Frame.InnerBrush.Gradient.Visible = True
          Frame.MiddleBrush.BackColor = clYellow
          Frame.MiddleBrush.Gradient.EndColor = 8553090
          Frame.MiddleBrush.Gradient.MidColor = clWhite
          Frame.MiddleBrush.Gradient.StartColor = clGray
          Frame.MiddleBrush.Gradient.Visible = True
          Frame.OuterBrush.BackColor = clGreen
          Frame.OuterBrush.Gradient.EndColor = 4210752
          Frame.OuterBrush.Gradient.MidColor = clWhite
          Frame.OuterBrush.Gradient.StartColor = clSilver
          Frame.OuterBrush.Gradient.Visible = True
          Frame.Width = 4
          Shadow.Color = clWhite
          Gradient.MidColor = clWhite
          OtherSlice.Legend.Visible = False
          PiePen.Color = clWhite
        end
      end
    end
  end
  object SumSource: TDataSource
    DataSet = SumMem
    Left = 32
    Top = 264
  end
  object SumMem: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 32
    Top = 208
  end
  object PaidSource: TDataSource
    DataSet = PaidMem
    Left = 96
    Top = 264
  end
  object PaidMem: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 96
    Top = 208
  end
  object RPSource: TDataSource
    DataSet = RPMem
    Left = 160
    Top = 264
  end
  object RPMem: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 160
    Top = 208
  end
  object ChartSource: TDataSource
    Left = 32
    Top = 384
  end
  object ChartData: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 30
    Top = 336
    object ChartProjectName: TStringField
      FieldName = 'ProjectName'
      Size = 100
    end
    object ChartSumMoney: TCurrencyField
      FieldName = 'SumMoney'
    end
  end
  object pm: TPopupMenu
    Left = 177
    Top = 126
    object N1: TMenuItem
      Caption = #23548#20986'Excel'
      OnClick = N1Click
    end
  end
end

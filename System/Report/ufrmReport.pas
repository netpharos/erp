﻿unit ufrmReport;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, Vcl.ExtCtrls, RzPanel, cxContainer, cxLabel, cxTextEdit, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, Data.Win.ADODB, frxDesgn, frxClass, frxDBSet;

type
  TfrmReport = class(TForm)
    tv: TcxGridDBTableView;
    Lv: TcxGridLevel;
    Grid: TcxGrid;
    RzPanel1: TRzPanel;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    tvColumn1: TcxGridDBColumn;
    ADOReport: TADODataSet;
    DataReport: TDataSource;
    ADOReportReportName: TStringField;
    frxReport1: TfrxReport;
    frxDesigner1: TfrxDesigner;
    frxStorageReport: TfrxDBDataset;
    procedure cxButton5Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReport: TfrmReport;

implementation

uses
   uDataModule;

{$R *.dfm}

procedure TfrmReport.cxButton1Click(Sender: TObject);
var
  pDataPage :TfrxDataPage;
  pPage :TfrxReportPage ;
  pBand :TfrxBand;
  pDataBand :TfrxMasterData ;
  pMemo :TfrxMemoView ;
  i:integer;
  x,y,z,width:extended;
begin
// 清除原报表内容
    frxReport1.Clear;
// 设置 Report 的 DataSet
    frxReport1.DataSets.Add(frxStorageReport);
// 添加 Report 的 Data 页
    pDataPage :=   TfrxDatapage.Create(frxReport1);
// 增加报表显示内容
    pPage := TfrxReportPage.Create(frxReport1);
    ppage.CreateUniqueName();
// 设置 Fields 的默认尺寸
    pPage.SetDefaults;
// 页面方向
 //   pPage.Orientation := poLandscape;
// 页面边距
    pPage.TopMargin := 20;
    pPage.LeftMargin := 20;
    pPage.RightMargin := 20;
    pPage.BottomMargin := 20;
// 创建报表 Title Band
    pBand := TfrxReportTitle.Create(pPage);
    pBand.CreateUniqueName();
    pBand.Top := 0;
    pBand.Height := 20;
// 创建显示 Field 的 Memo
    pMemo := TfrxMemoView.Create(pBand);
    pMemo.CreateUniqueName();
    pMemo.Text := 'Hello FastReport!';
    pMemo.Height := 20;
    // 风格
    pMemo.Align := baWidth;
    // 创建 DataBand
    pDataBand := TfrxMasterData.Create(pPage);
    pDataBand.CreateUniqueName();
    pDataBand.DataSet := frxStorageReport;
    pDataBand.Top := 100;
    pDataBand.Height := 20;
    // 将 Memo 添加到 DataBand 上
    pMemo :=  TfrxMemoView.Create(pDataBand);
    pMemo.CreateUniqueName();
    // 连接数据
    pMemo.DataSet := frxStorageReport;
    // 显示 username 字段
    pMemo.DataField := 'user_name';
    pMemo.SetBounds(0, 0, 100, 20);
    // memo 带边框
     pMemo.Frame.Typ := [ftLeft , ftRight , ftTop , ftBottom];
    // 风格
    pMemo.HAlign := haRight;
    // 报表显示
    frxReport1.ShowReport(true);

end;

procedure TfrmReport.cxButton5Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmReport.cxButton3Click(Sender: TObject);
var
  pDataPage :TfrxDataPage;
  pPage :TfrxReportPage ;
  pBand :TfrxBand;
  pDataBand :TfrxMasterData ;
  pMemo :TfrxMemoView ;
  pTitle :TfrxReportTitle;
  pPageHeader :TfrxPageHeader;
  pPagetFooter :TfrxPageFooter;
  i:integer;
  x,y,z,width:extended;
  poPortrait, poLandscape: TPrinterOrientation ;
begin
  {
    width:= 80 ;   // 字段宽度
    x:= 0 ;
    y:= 0 ;
    z:= 0 ;

    frxReport1.Clear;
    frxReport1.DataSets.Add(frxStorageReport);
    pDataPage :=   TfrxDatapage.Create(frxReport1);
    pPage := TfrxReportPage.Create(frxReport1);
    ppage.CreateUniqueName();
    pPage.SetDefaults;
    pPage.Orientation := poLandscape;  // 橫印， poPortrait:  直印（默認），
    pPage.TopMargin := 10;  // 上邊距 (mm)
    pPage.LeftMargin := 20;  // 左邊距 (mm)
    pPage.RightMargin := 20;  //右邊距 (mm)
    pPage.BottomMargin := 15;  // 下邊距(mm)
     {
    pBand := TfrxReportTitle.Create(pPage);
    pBand.CreateUniqueName();
    pBand.Top := 0;
    pBand.Height := 20;
      }
  {
    pTitle := TfrxReportTitle.Create(pPage);
    pTitle.CreateUniqueName();
    pTitle.Top := 0;
    pTitle.Height := 30;
    // 標題頭
    pMemo := TfrxMemoView.Create(pTitle);
    pMemo.CreateUniqueName();
    pMemo.Text :='AutoReport for FastReport4.14';
    pMemo.SetBounds(0,0,300,30);   //MemoView :長、寬、高 ；
    pMemo.Font.Size := 14 ;  // 14號字
    pMemo.Font.Style :=[fsBold];  // 粗體
    pMemo.Height := 30;
    pMemo.Align := baCenter;  // MemoView:居中 ；

    //  創建 表頭欄
    pPageHeader := TfrxPageHeader.Create(pPage);
    pPageHeader.CreateUniqueName;
    pPageHeader.Top := 50;
    pPageHeader.Height:= 20;


    for i := 0 to clientDataset1.FieldCount -1  do
    begin

      // 表頭欄位
      pMemo := TfrxMemoView.Create(pPageHeader);
      pMemo.CreateUniqueName();
      pMemo.Text := clientDataset1.Fields[i].FieldName;
      pMemo.SetBounds( i * width, 0, width, 20);
      pMemo.Frame.Typ:=[ftLeft , ftRight , ftTop , ftBottom];
      pMemo.Height := 20;
      // 风格
     // pMemo.Align := baWidth;
       pMemo.Align := baLeft;
    end; // end for ;
    // 创建 DataBand
    pDataBand := TfrxMasterData.Create(pPage);
    pDataBand.CreateUniqueName();
    pDataBand.DataSet := frxStorageReport;
    pDataBand.Top := 100;
    pDataBand.Height := 20;

    for i := 0 to clientdataset1.FieldCount-1 do
    begin

      // 将 Memo 添加到 DataBand 上
      pMemo :=  TfrxMemoView.Create(pDataBand);
      pMemo.CreateUniqueName();
      // 连接数据
      pMemo.DataSet := frxStorageReport;
      // 显示 username 字段
      pMemo.DataField := clientDataset1.Fields[i].FieldName;
      pMemo.SetBounds( i * width, 0, width, 20);  // setBountds(X横行起点，Y起点，字段宽度，字段高度)
      // memo 带边框
      pMemo.Frame.Typ:=[ftLeft , ftRight , ftTop , ftBottom];
      // 风格
      // pMemo.HAlign := haRight;  // 字 右對齊；
      pMemo.HAlign := haLeft;
    end; // end for ;

    // 报表显示
    frxReport1.ShowReport(true);
  }
end;

procedure TfrmReport.FormCreate(Sender: TObject);
begin
  Self.ADOReport.CreateDataSet;
  Self.ADOReport.Close;
  Self.ADOReport.CommandText := 'Select *from sk_SysReportList';
  Self.ADOReport.Open;
end;

procedure TfrmReport.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Close;
  end;
end;

end.

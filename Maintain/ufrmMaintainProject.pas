unit ufrmMaintainProject;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzPanel, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  cxTextEdit, cxLabel, RzButton, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Data.Win.ADODB,ufrmBaseController, Vcl.ComCtrls,
  dxSkinsdxStatusBarPainter, dxStatusBar,UtilsTree, FillThrdTree, Vcl.Menus;

type
  TfrmMaintainProject = class(TfrmBaseController)
    RzToolbar10: TRzToolbar;
    RzSpacer77: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer78: TRzSpacer;
    RzBut5: TRzToolButton;
    RzSpacer79: TRzSpacer;
    RzBut6: TRzToolButton;
    RzBut12: TRzToolButton;
    RzToolButton1: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    cxLabel1: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    tvView: TcxGridDBTableView;
    Lv: TcxGridLevel;
    Grid: TcxGrid;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    tvViewCol1: TcxGridDBColumn;
    tvViewCol2: TcxGridDBColumn;
    tvViewCol3: TcxGridDBColumn;
    tvViewCol4: TcxGridDBColumn;
    RzPanel1: TRzPanel;
    Tv1: TTreeView;
    RzToolbar3: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzSpacer19: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer20: TRzSpacer;
    RzToolButton22: TRzToolButton;
    RzSpacer25: TRzSpacer;
    RzToolButton4: TRzToolButton;
    Splitter1: TSplitter;
    RzPanel2: TRzPanel;
    dxStatusBar1: TdxStatusBar;
    RzSpacer5: TRzSpacer;
    RzToolButton2: TRzToolButton;
    pm: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure RzBut4Click(Sender: TObject);
    procedure RzBut12Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tvViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton1Click(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure RzToolButton12Click(Sender: TObject);
    procedure RzToolButton22Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure Tv1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Tv1Edited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure Tv1DblClick(Sender: TObject);
    procedure ADOQuery1AfterInsert(DataSet: TDataSet);
    procedure tvViewDblClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
  private
    { Private declarations }
    g_NodeIndex : Integer;
    g_PatTree: TNewUtilsTree;
    procedure UpDateIndex(lpNode : TTreeNode);
    procedure TreeAddName(Tv : TTreeView); //树增加名称
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
  public
    { Public declarations }
    dwSingName : string;
    dwIsReadoly: Boolean;
  end;

var
  frmMaintainProject: TfrmMaintainProject;

implementation

uses
   uDataModule,global,CreateOrderNum,ufunctions;

{$R *.dfm}

procedure TfrmMaintainProject.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  szCode : string;
  Prefix : string;
  Suffix : string;

begin
  Prefix := 'Na-';
  Suffix := '009000';

  with Tv do
  begin
    Node := Selected;
      if Assigned(Node) then
      begin

        szCode := Prefix + GetRowCode( g_Table_Collect_Receivables ,
                                          'Code',
                                          Suffix,
                                          907000 );
        g_PatTree.AddChildNode(szCode,'',Date,m_OutText);
      end;
  end;

end;

function TfrmMaintainProject.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    g_PatTree.DeleteTree(Node);
  end else
  begin
    g_PatTree.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv1.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv1.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;

procedure TfrmMaintainProject.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzBut4.Click;
end;

procedure TfrmMaintainProject.N2Click(Sender: TObject);
begin
  inherited;
  with Self.ADOQuery1 do Edit;
end;

procedure TfrmMaintainProject.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzBut5.Click;
end;

procedure TfrmMaintainProject.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzBut6.Click;
end;

procedure TfrmMaintainProject.N5Click(Sender: TObject);
begin
  inherited;
  Self.RzBut12.Click;
end;

procedure TfrmMaintainProject.Tv1DblClick(Sender: TObject);
var
  Node : TTreeNode;
  szNode : PNodeData;
begin
  inherited;
  Node := Self.Tv1.Selected;
  if Assigned(Node) then
  begin
    szNode :=  PNodeData(Node.Data);
    if Assigned(szNode) then
    begin
      g_NodeIndex := szNode.Index;
      with Self.ADOQuery1 do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from ' + g_Table_Maintain_Project + ' WHERE code ="' + IntToStr( g_NodeIndex ) + '"';
        Open;

      end;

    end;

  end;

end;

procedure TfrmMaintainProject.Tv1Edited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  inherited;
  IstreeEdit(s,Node);
end;

procedure TfrmMaintainProject.Tv1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    13:
    begin
      with Self.Tv1.Selected do
      begin
        if Level > 0 then
        begin
          Parent.Selected := True;
          Self.RzToolButton3.Click;
        end

      end;

    end;
    46:
    begin
      Self.RzToolButton5.Click;
    end;
  end;
end;

procedure TfrmMaintainProject.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  S := 'UPDATE ' + g_Table_maintain_ProjectTree + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    ExecSQL;
  end;
end;

procedure TfrmMaintainProject.ADOQuery1AfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    FieldByName('code').Value := g_NodeIndex;
  end;
end;

procedure TfrmMaintainProject.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzToolButton1.Click;
  end;
end;

procedure TfrmMaintainProject.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  Self.ADOQuery1.Close;
end;

procedure TfrmMaintainProject.FormCreate(Sender: TObject);
begin
  g_PatTree := TNewUtilsTree.Create(Self.Tv1,DM.ADOconn,g_Table_maintain_ProjectTree, '分类管理');
  g_PatTree.GetTreeTable( 0 );

end;

procedure TfrmMaintainProject.RzBut12Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMaintainProject.RzBut4Click(Sender: TObject);
begin
  if Sender = Self.RzBut4 then
  begin
    with Self.ADOQuery1 do
    begin
      if State <> dsInactive then
      begin
        Append;
        Self.tvViewCol2.FocusWithSelection;
        Self.Grid.SetFocus;
        keybd_event(VK_RETURN,0,0,0);

      end;

    end;
  end else
  if Sender = Self.RzBut5 then
  begin
    //保存
    IsDeleteEmptyData(Self.ADOQuery1,
                      g_Table_Maintain_Project ,
                      Self.tvViewCol2.DataBinding.FieldName);

  end else
  if Sender = Self.RzBut6 then
  begin
    //删除
    IsDelete(Self.tvView);
  end;
end;

procedure TfrmMaintainProject.RzToolButton12Click(Sender: TObject);
var
  PrevNode , NextNode , SourceNode , TargetNode : TTreeNode;
  S : string;
begin
  inherited;

  PrevNode   := Tv1.Selected.getPrevSibling;
  SourceNode := Tv1.Selected;
  if (PrevNode.Index <> -1) then
  begin
    NextNode   := Tv1.Selected.getNextSibling;
    if NextNode <> nil then
      PrevNode.MoveTo(NextNode, naInsert)
    else
      PrevNode.MoveTo(SourceNode, naAdd);
  end;
  SourceNode := Tv1.Selected; //中
  UpDateIndex(SourceNode);
  NextNode   := Tv1.Selected.getNextSibling;//下
  UpDateIndex(NextNode);

end;

procedure TfrmMaintainProject.RzToolButton1Click(Sender: TObject);
var
  s : string;

begin
  s := Self.cxTextEdit1.Text;
  GridLocateRecord(Self.tvView,Self.tvViewCol2.DataBinding.FieldName,s);
end;

procedure TfrmMaintainProject.RzToolButton22Click(Sender: TObject);
var
  OldNode , Node , SourceNode ,PrevNode: TTreeNode;
begin
  inherited;
  Node := Self.Tv1.Selected;
  OldNode := Node.getNextSibling;
  if OldNode.Index <> -1 then
  begin
    OldNode.MoveTo(Node, naInsert);
    SourceNode := Tv1.Selected; //中
    UpDateIndex(SourceNode);
    PrevNode   := Tv1.Selected.getPrevSibling;//上
    UpDateIndex(PrevNode);

  end;

end;

procedure TfrmMaintainProject.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  TreeAddName(Self.Tv1);
end;

procedure TfrmMaintainProject.RzToolButton4Click(Sender: TObject);
begin
  inherited;
  Self.Tv1.Selected.EditText;
end;

procedure TfrmMaintainProject.RzToolButton5Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szTableList: array[0..1] of TTableList;
begin
  inherited;
  if MessageBox(handle, '是否删除分类管理节点？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
  begin
      Node := Self.Tv1.Selected;
      if Assigned(Node) then
      begin
        GetIndexList(Node,szCodeList);

        szTableList[0].ATableName := g_Table_Maintain_Project;
        szTableList[0].ADirectory := '';

        DeleteTableFile(szTableList,szCodeList);
        DM.InDeleteData(g_Table_maintain_ProjectTree,szCodeList);
        g_PatTree.DeleteTree(Node);

      end;
  end;

end;

procedure TfrmMaintainProject.tvViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmMaintainProject.tvViewDblClick(Sender: TObject);
begin
  inherited;
  dwSingName := VarToStr( Self.tvViewCol2.EditValue );
  Close;
end;

procedure TfrmMaintainProject.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  if AItem.Index = Self.tvViewCol1.Index then
  begin
    AAllow := False;
  end;
  if dwIsReadoly then
  begin
    if Self.ADOQuery1.State in [dsEdit,dsInsert] then
    begin
      AAllow := True;
    end else
    begin
      AAllow := False;
    end;
  end;
end;

procedure TfrmMaintainProject.tvViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (Self.tvViewCol2.Index = AItem.Index) then
  begin
    if Self.tvView.Controller.FocusedRow.IsLast then
    begin
      with Self.ADOQuery1 do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvView.Columns[0].Focused := True;
        end;
      end;
    end;
  end;
end;

end.

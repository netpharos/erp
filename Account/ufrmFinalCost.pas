unit ufrmFinalCost;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCalendar, cxTextEdit,
  cxCurrencyEdit, cxSpinEdit, cxDropDownEdit, cxMemo, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxMaskEdit, cxLabel, RzTabs, RzPanel, RzButton, Vcl.ExtCtrls,
  uBaseProject, Vcl.Menus, cxGroupBox, Vcl.StdCtrls, cxButtons, Data.Win.ADODB,ufrmBaseController,
  cxSplitter, UtilsTree, FillThrdTree, frxCross, frxDesgn, frxClass, cxDBEdit,
  cxCheckBox, dxmdaset, cxButtonEdit;

type
  TfrmFinalCost = class(TfrmBaseController)
    final: TADOQuery;
    ds: TDataSource;
    ADOProjectName: TADOQuery;
    DataProjectName: TDataSource;
    DataGroup: TDataSource;
    Left: TPanel;
    TreeView1: TTreeView;
    RzToolbar3: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton19: TRzToolButton;
    RzPanel19: TRzPanel;
    RzPanel3: TRzPanel;
    RzPanel2: TRzPanel;
    RzPanel15: TRzPanel;
    RzPanel17: TRzPanel;
    RzPanel18: TRzPanel;
    RzPanel16: TRzPanel;
    RzPanel5: TRzPanel;
    RzPanel11: TRzPanel;
    RzPanel12: TRzPanel;
    RzPanel13: TRzPanel;
    RzPanel14: TRzPanel;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel9: TRzPanel;
    RzPanel10: TRzPanel;
    RzPanel8: TRzPanel;
    pcSys: TRzPageControl;
    TabSheet1: TRzTabSheet;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzToolButton5: TRzToolButton;
    RzToolButton6: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzSpacer19: TRzSpacer;
    RzSpacer20: TRzSpacer;
    RzSpacer21: TRzSpacer;
    RzSpacer11: TRzSpacer;
    RzToolButton15: TRzToolButton;
    cxLabel1: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel2: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    cxGrid2: TcxGrid;
    tvDealGrid: TcxGridDBTableView;
    tvDealGridColumn1: TcxGridDBColumn;
    tvDealGridColumn18: TcxGridDBColumn;
    tvDealGridColumn2: TcxGridDBColumn;
    tvDealGridColumn3: TcxGridDBColumn;
    tvDealGridColumn4: TcxGridDBColumn;
    tvDealGridColumn10: TcxGridDBColumn;
    tvDealGridColumn19: TcxGridDBColumn;
    tvDealGridColumn9: TcxGridDBColumn;
    tvDealGridColumn15: TcxGridDBColumn;
    tvDealGridColumn16: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    RzPanel1: TRzPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxGroupBox1: TcxGroupBox;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    cxDBDateEdit1: TcxDBDateEdit;
    cxDBDateEdit2: TcxDBDateEdit;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    Splitter1: TSplitter;
    RzPanel4: TRzPanel;
    cxSplitter1: TcxSplitter;
    Splitter3: TSplitter;
    TabSheet2: TRzTabSheet;
    cxGrid1: TcxGrid;
    tvGrid3: TcxGridDBTableView;
    tvGrid3Column1: TcxGridDBColumn;
    tvGrid3Column2: TcxGridDBColumn;
    tvGrid3Column18: TcxGridDBColumn;
    tvGrid3Column3: TcxGridDBColumn;
    tvGrid3Column4: TcxGridDBColumn;
    tvGrid3Column5: TcxGridDBColumn;
    tvGrid3Column6: TcxGridDBColumn;
    tvGrid3Column7: TcxGridDBColumn;
    tvGrid3Column8: TcxGridDBColumn;
    tvGrid3Column9: TcxGridDBColumn;
    tvGrid3Column10: TcxGridDBColumn;
    tvGrid3Column13: TcxGridDBColumn;
    tvGrid3Column14: TcxGridDBColumn;
    tvGrid3Column15: TcxGridDBColumn;
    tvGrid3Column16: TcxGridDBColumn;
    tvGrid3Column11: TcxGridDBColumn;
    tvGrid3Column12: TcxGridDBColumn;
    tvGrid3Column17: TcxGridDBColumn;
    tvView: TcxGridDBTableView;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn16: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    tvViewColumn9: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    tvViewColumn13: TcxGridDBColumn;
    tvViewColumn14: TcxGridDBColumn;
    tvViewColumn15: TcxGridDBColumn;
    lv3: TcxGridLevel;
    Lv2: TcxGridLevel;
    RzToolbar2: TRzToolbar;
    RzSpacer5: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzToolButton11: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer12: TRzSpacer;
    RzSpacer15: TRzSpacer;
    RzToolButton16: TRzToolButton;
    RzToolButton23: TRzToolButton;
    RzSpacer26: TRzSpacer;
    RzSpacer27: TRzSpacer;
    cxLabel10: TcxLabel;
    Date3: TcxDateEdit;
    cxLabel11: TcxLabel;
    Date4: TcxDateEdit;
    Splitter2: TSplitter;
    FlowingGrid: TcxGrid;
    tvFlowingView: TcxGridDBTableView;
    tvFlowingViewColumn1: TcxGridDBColumn;
    tvFlowingViewColumn2: TcxGridDBColumn;
    tvFlowingViewColumn4: TcxGridDBColumn;
    tvFlowingViewColumn5: TcxGridDBColumn;
    tvFlowingViewColumn3: TcxGridDBColumn;
    FlowingLv: TcxGridLevel;
    Master: TADOQuery;
    DataMaster: TDataSource;
    DataDetailed: TDataSource;
    Detailed: TADOQuery;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    DataFlowingGroup: TDataSource;
    FlowingGroup: TADOQuery;
    tvFlowingViewColumn6: TcxGridDBColumn;
    RzPageControl1: TRzPageControl;
    TabSheet3: TRzTabSheet;
    TabSheet4: TRzTabSheet;
    RzPageControl2: TRzPageControl;
    RzTabSheet1: TRzTabSheet;
    RzTabSheet2: TRzTabSheet;
    GridProjectNameGrid: TcxGrid;
    tvProjectView: TcxGridDBTableView;
    ProjectCol1: TcxGridDBColumn;
    ProjectCol2: TcxGridDBColumn;
    lvProject: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    WorkGroup: TcxGrid;
    tvGroupView: TcxGridDBTableView;
    tvGroupViewCol1: TcxGridDBColumn;
    tvGroupViewCol2: TcxGridDBColumn;
    tvGroupViewCol4: TcxGridDBColumn;
    tvGroupViewCol5: TcxGridDBColumn;
    cxgrdbclmnGroupViewColumn1: TcxGridDBColumn;
    tvGroupViewColumn1: TcxGridDBColumn;
    WorkGroupLv: TcxGridLevel;
    cxGrid5: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridLevel3: TcxGridLevel;
    ADOYears: TADOQuery;
    DataYears: TDataSource;
    ADOSumInfo: TADODataSet;
    DataSumInfo: TDataSource;
    ADOWorkGroup: TADOQuery;
    DataWorkGroup: TDataSource;
    ADOSumInfom: TCurrencyField;
    ADOSumInfof: TFloatField;
    cxGrid4: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    Splitter4: TSplitter;
    ADOSumOverInfo: TADODataSet;
    CurrencyField1: TCurrencyField;
    FloatField1: TFloatField;
    DataSumOverInfo: TDataSource;
    ADOSumInfoYear: TIntegerField;
    ADOSumOverInfoYear: TIntegerField;
    RzSpacer13: TRzSpacer;
    RzToolButton13: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton14: TRzToolButton;
    RzSpacer22: TRzSpacer;
    RzToolButton20: TRzToolButton;
    pm1: TPopupMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    pm2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    pmFrozen: TPopupMenu;
    MenuItem7: TMenuItem;
    N18: TMenuItem;
    RzSpacer23: TRzSpacer;
    RzToolButton21: TRzToolButton;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    RzSpacer24: TRzSpacer;
    RzToolButton22: TRzToolButton;
    TabSheet5: TRzTabSheet;
    cxGrid6: TcxGrid;
    tvGroupView1: TcxGridDBTableView;
    tvGroupView1Column1: TcxGridDBColumn;
    tvGroupView1Column2: TcxGridDBColumn;
    tvGroupView1Column3: TcxGridDBColumn;
    Lv4: TcxGridLevel;
    tvGroupView1Column4: TcxGridDBColumn;
    dxMemData1: TdxMemData;
    DataSource1: TDataSource;
    dxMemData1c: TStringField;
    dxMemData1t: TCurrencyField;
    dxMemData1d: TStringField;
    ADOGroup: TdxMemData;
    ADOGroupProjectName: TStringField;
    ADOGroupt: TCurrencyField;
    ADOGroupd: TStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton17Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure RzToolButton1Click(Sender: TObject);
    procedure FinalAfterInsert(DataSet: TDataSet);
    procedure tvDealGridColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton3Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure FinalAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure tvDealGridColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvProjectViewDblClick(Sender: TObject);
    procedure RzToolButton15Click(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBCurrencyEdit2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBTextEdit2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBCurrencyEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvDealGridColumn15PropertiesPopup(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxDBTextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvDealGridDblClick(Sender: TObject);
    procedure tvDealGridEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvDealGridEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvDealGridStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvDealGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure RzToolButton6Click(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure RzToolButton16Click(Sender: TObject);
    procedure tvGrid3ColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvGrid3Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvGrid3StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvDealGridColumn18PropertiesChange(Sender: TObject);
    procedure MasterAfterOpen(DataSet: TDataSet);
    procedure tvFlowingViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvGrid3Column11PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvGrid3TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvGroupViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure tvGrid3EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView3TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxGridDBTableView2TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure RzPanel3Resize(Sender: TObject);
    procedure RzToolButton13Click(Sender: TObject);
    procedure RzToolButton14Click(Sender: TObject);
    procedure RzToolButton20Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure tvDealGridColumn10PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
    g_PatTree: TNewUtilsTree;
    g_ProjectName : string;
    g_PostCode    : string;
    g_CodeList    : string;
    g_TopLevelName: string;
    g_SignName : string;
    Prefix : string;
    Suffix : string;
    dwCompanyMoney : Currency;
    function GetGroupMoney():string;
  public
    { Public declarations }
    FromTypes : Integer;
    ParentIndex  : Integer;

    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
    function GetSumMoney(lpCount : Double; lpUnitPrice : Currency ):Boolean;
    procedure RefreshDetailTable();
    function CalcValue():Boolean;
  end;

var
  frmFinalCost: TfrmFinalCost;


implementation

uses
  uDataModule,ufrmPostPact,global,ufrmIsViewGrid,ufrmDetailed,ufunctions,ufrmCompanyManage;

{$R *.dfm}

function TfrmFinalCost.CalcValue():Boolean;
var
  szContractValue : Variant;
  szProjectGroupValue : Variant;
  szPayValue : Variant;
  szAccessValue : Variant;
  DD1 , DD2 , DD3 , DD4 , DD5 , DD6 : Currency;
  s : string;
  szCompanyValue : Variant;
  szCompanyOverValue : Variant;

begin
  szContractValue := Self.tvDealGrid.DataController.Summary.FooterSummaryValues[0]; //合同总价
  szProjectGroupValue := Self.tvGroupView.DataController.Summary.FooterSummaryValues[0]; //项目统计
  szCompanyValue := Self.cxGridDBTableView3.DataController.Summary.FooterSummaryValues[0];
  szCompanyOverValue := Self.cxGridDBTableView2.DataController.Summary.FooterSummaryValues[0];

  szPayValue    := Self.tvGrid3.DataController.Summary.FooterSummaryValues[0]; //交易记录
  szAccessValue := Self.tvFlowingView.DataController.Summary.FooterSummaryValues[0]; //交易流水

  s   := VarToStr(szContractValue);
  DD1 := StrToCurrDef(s,0);
  s   := VarToStr(szProjectGroupValue);

  DD2 := StrToCurrDef(s,0);
  s   := VarToStr(szPayValue);

  DD3 := StrToCurrDef(s,0);
  s   :=  VarToStr(szAccessValue);

  DD4 := StrToCurrDef(s,0);
  s   := VarToStr(szCompanyValue);

  DD5 := StrToCurrDef(s,0);
  s   := VarToStr(szCompanyOverValue);
  DD6 := StrToCurrDef(s,0);

  Self.RzPanel10.Caption := Format('%2.2m', [DD1 + DD2 + DD5 + DD6 + dwCompanyMoney ]);  //合同总价
  Self.RzPanel9.Caption  := MoneyConvert(DD1 + DD2 + DD5 + DD6 + dwCompanyMoney);

  Self.RzPanel13.Caption := Format('%2.2m', [DD3 + DD4]);  //交易总额
  Self.RzPanel14.Caption := MoneyConvert( DD3 + DD4);

  Self.RzPanel17.Caption := Format('%2.2m', [ (DD1 + DD2 + DD5 + DD6) - (DD3 + DD4) ]);
  Self.RzPanel18.Caption := MoneyConvert( (DD1 + DD2 + DD5 + DD6) - (DD3 + DD4));

end;

procedure TfrmFinalCost.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmFinalCost.cxButton1Click(Sender: TObject);
begin
  inherited;
  with Self.tvDealGridColumn15 do
  begin
    EditValue := Self.cxDBCurrencyEdit1.EditingValue;
    Editing := False;
  end;
end;

procedure TfrmFinalCost.cxButton2Click(Sender: TObject);
begin
  inherited;
  Self.tvDealGridColumn15.Editing := False;
end;

procedure TfrmFinalCost.cxDBCurrencyEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;
//总额
  if DisplayValue <> null then
  begin
    s :=  VarToStr(DisplayValue);
    Self.cxDBTextEdit1.EditValue := MoneyConvert( StrToCurrDef(s,0) );
    Self.cxDBTextEdit1.PostEditValue;
  end;

end;

function TfrmFinalCost.GetSumMoney(lpCount : Double; lpUnitPrice : Currency ):Boolean;
var
  szSumMoney : Currency;

begin
  szSumMoney := lpCount * lpUnitPrice;
  Self.cxDBCurrencyEdit1.EditValue := szSumMoney;
  self.cxDBCurrencyEdit1.PostEditValue;

  Self.cxDBTextEdit1.EditValue := MoneyConvert(szSumMoney);
  Self.cxDBTextEdit1.PostEditValue;
end;

procedure TfrmFinalCost.MasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmFinalCost.MenuItem2Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton8.Click;
end;

procedure TfrmFinalCost.MenuItem3Click(Sender: TObject);
begin
  inherited;
  RzToolButton9.Click;
end;

procedure TfrmFinalCost.MenuItem6Click(Sender: TObject);
begin
  inherited;
  CreateEnclosure(Self.tvGrid3.DataController.DataSource.DataSet,Handle,
                  Concat( g_Resources ,'\' + g_dir_BranchMoney));
end;

procedure TfrmFinalCost.MenuItem7Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  if Sender = Self.MenuItem7 then
  begin
    s := 'no'
  end else
  if Sender = Self.N18 then
  begin
    s := 'yes'
  end;

  case Self.pcSys.ActivePageIndex of
    0:
    begin

      with Self.final do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from '+ g_Table_Finance_ProjectFinal +' Where ' + g_CodeList + ' AND ' + Self.tvDealGridColumn18.DataBinding.FieldName + '=' + s;
        Open;
      end;

    end;
    1:
    begin
      with Self.Master do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from ' + g_Table_BranchMoney + ' where ' + g_CodeList + ' AND ' + Self.tvGrid3Column18.DataBinding.FieldName + '=' + s;
        Open;
      end;
    end;
  end;
end;

procedure TfrmFinalCost.N11Click(Sender: TObject);
begin
  inherited;
  //冻结
  SetGridStuat(Self.tvDealGrid,Self.tvDealGridColumn18.DataBinding.FieldName,False);
end;

procedure TfrmFinalCost.N12Click(Sender: TObject);
begin
  inherited;
  //解冻
  SetGridStuat(Self.tvDealGrid,Self.tvDealGridColumn18.DataBinding.FieldName,True);
end;

procedure TfrmFinalCost.N14Click(Sender: TObject);
begin
  inherited;
  SetGridStuat(Self.tvGrid3,Self.tvGrid3Column18.DataBinding.FieldName,False);
end;

procedure TfrmFinalCost.N15Click(Sender: TObject);
begin
  inherited;
  SetGridStuat(Self.tvGrid3,Self.tvGrid3Column18.DataBinding.FieldName,True);
end;

procedure TfrmFinalCost.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton13.Click;
end;

procedure TfrmFinalCost.N2Click(Sender: TObject);
begin
  inherited;
  case pcSys.ActivePageIndex of
    0: DM.BasePrinterLink1.Component := Self.cxGrid2;
    1: DM.BasePrinterLink1.Component := Self.cxGrid1;   
  end;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmFinalCost.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Click;
end;

procedure TfrmFinalCost.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton3.Click;
end;

procedure TfrmFinalCost.N6Click(Sender: TObject);
begin
  inherited;
  CreateEnclosure(Self.tvDealGrid.DataController.DataSource.DataSet,Handle,
                  Concat( g_Resources ,'\' + g_Dir_ProjectFinal));
end;

procedure TfrmFinalCost.N8Click(Sender: TObject);
begin
  inherited;

  Self.RzToolButton1.Click;
end;

procedure TfrmFinalCost.N9Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton7.Click;
end;

procedure TfrmFinalCost.cxDBCurrencyEdit2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szUnitPrice : Currency;
  szCount : Double;

  s : string;
  Value : Variant;

begin
  inherited;
//单价
  Value := Self.cxDBSpinEdit1.EditingValue;
  if Value <> null then
  begin
    s := VarToStr(  Value );
    szCount := StrToFloatDef(s,0);
  end;

  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);

    szUnitPrice := StrToCurrDef(s,0);
  end;

  GetSumMoney(szCount , szUnitPrice);
end;

procedure TfrmFinalCost.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szUnitPrice : Currency;
  szCount : Double;

  s : string;
  Value : Variant;

begin
  inherited;
//数量
  Value := Self.cxDBCurrencyEdit2.EditingValue;
  if Value <> null then
  begin
    s := VarToStr(  Value );
    szUnitPrice := StrToCurrDef(s,0);
  end;

  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    szCount := StrToFloatDef( s,0);
  end;
  GetSumMoney(szCount , szUnitPrice);
end;

procedure TfrmFinalCost.cxDBTextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then Self.cxDBTextEdit2.PostEditValue;
end;

procedure TfrmFinalCost.cxDBTextEdit2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szUnitPrice : Currency;
  szCount : Double;

  s : string;
  Value : Variant;

begin
  inherited;
//计算公式
  Value := Self.cxDBCurrencyEdit2.EditingValue;
  if Value <> null then
  begin
    s := VarToStr(  Value );
    szUnitPrice := StrToCurrDef(s,0);
  end;

  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    s :=  FunExpCalc(s,-2) ;
  //  Self.cxLabel11.Caption := s ;
    szCount := StrToFloatDef(s,0);
  end;

  Self.cxDBSpinEdit1.EditValue := szCount;
  Self.cxDBSpinEdit1.PostEditValue;

  GetSumMoney(szCount , szUnitPrice);
end;

function GetWorkSQLText(lpTreeId , lpContacts , lpTableName : string):string;
begin
  Result := 'Select t2.Year, SUM(t2.SumMoney) AS m,SUM(t2.ForWork)  AS f '+
            'from (sk_Maintain_CompanyInfoList AS t1 right join '+ lpTableName +' AS t2'+
            ' on t1.id = t2.Parent) WHERE t1.Contacts="'+ lpContacts +'" AND t1.TreeId=' + lpTreeId +
            ' AND t1.status=yes GROUP BY t2.Year';
end;

function GetOverSQLText(lpTreeId , lpContacts , lpTableName : string):string;
begin
  Result := 'Select t2.Year, SUM(t2.SumMoney) AS m,SUM(t2.SumOvertime)  AS f '+
            'from (sk_Maintain_CompanyInfoList AS t1 right join '+ lpTableName +' AS t2'+
            ' on t1.id = t2.Parent) WHERE t1.Contacts="'+ lpContacts +'" AND t1.TreeId=' + lpTreeId +
            ' AND t1.status=yes GROUP BY t2.Year';
end;


procedure TfrmFinalCost.cxGridDBTableView1DblClick(Sender: TObject);
var
  szTreeId : string;
  szColName : string;
  szColList : string;
  szSumListName : string;
  s : string;
  szTableName : string;
  DD1 : string;
  t:Currency;
begin
  inherited;
  szColName := Self.ProjectCol2.DataBinding.FieldName;
  with Self.ADOYears do
  begin
    if State <> dsInactive then
     begin
       if not IsEmpty then
        begin
          szTreeId := FieldByName('Parent').AsString;
          s := GetWorkSQLText(szTreeId,g_SignName,g_Table_Company_Staff_CheckWork);
          with Self.ADOSumInfo do
          begin
            Close;
            CommandText := s;
            Open;
          end;

          s := GetOverSQLText(szTreeId,g_SignName,g_Table_Company_Staff_Over);
          with Self.ADOSumOverInfo do
          begin
            Close;
            CommandText := s;
            Open;
          end;

          szColList := Self.tvGroupViewCol2.DataBinding.FieldName;
          //帐目总价
          s := 'SELECT ProjectName,SUM(SumMoney) as t,Receivables,Payee FROM '+
          g_Table_RuningAccount +' where Receivables="' + g_SignName + '" AND fromType=1 GROUP BY ProjectName,Receivables,Payee';

          with Self.FlowingGroup do
          begin
            Close;
            SQL.Text := s;
            Open;
          end;

        end;

     end;

  end;

end;

procedure TfrmFinalCost.cxGridDBTableView2TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;

begin
  inherited;
  if AValue <> null then
  begin
    CalcValue;
  end;
end;

procedure TfrmFinalCost.cxGridDBTableView3TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;

begin
  inherited;
  if AValue <> null then
  begin
  //  s := VarToStr(AValue);
    CalcValue;
  end;
end;

procedure TfrmFinalCost.Excel1Click(Sender: TObject);
begin
  inherited;
  case pcSys.ActivePageIndex of
    0: CxGridToExcel(Self.cxGrid2,'帐目明细表');
    1: CxGridToExcel(Self.cxGrid1,'交易明细表');  
  end;
end;

procedure TfrmFinalCost.FinalAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName('Code').Value := g_PostCode;
      FieldByName(Self.tvDealGridColumn18.DataBinding.FieldName).Value := True;
      FieldByName(Self.tvDealGridColumn10.DataBinding.FieldName).Value := g_TopLevelName;
      FieldByName(Self.tvDealGridColumn4.DataBinding.FieldName).Value  := g_ProjectName;
      FieldByName(Self.tvDealGridColumn3.DataBinding.FieldName).Value  := Date;
      szColName := Self.tvDealGridColumn2.DataBinding.FieldName;
      szCode := Prefix + GetRowCode(g_Table_Finance_ProjectFinal,szColName,Suffix,120000);
      FieldByName( szColName ).Value := szCode;
      FieldByName(Self.cxDBDateEdit1.DataBinding.DataField).Value := Date;
      FieldByName(Self.cxDBDateEdit2.DataBinding.DataField).Value := Date;

    end;
  end;
end;

procedure TfrmFinalCost.FinalAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmFinalCost.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TfrmFinalCost.FormCreate(Sender: TObject);
begin
  inherited;
  Prefix := 'CB-';
  Suffix := '120000';
  SetcxGrid(Self.tvDealGrid,0,g_Dir_Project_Mechanics);
  SetcxGrid(Self.tvGrid3,0,g_dir_BranchMoney);
  Self.ADOSumInfo.CreateDataSet;
  Self.ADOSumOverInfo.CreateDataSet;

end;

procedure TfrmFinalCost.FormShow(Sender: TObject);
var
  g_Table_Name : string;
begin
  inherited;
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.Date3.Date := Date;
  Self.Date4.Date := Date;
  g_PatTree := TNewUtilsTree.Create(Self.TreeView1,DM.ADOconn,g_Table_Finance_FinalAccount,'往来单位');
  g_PatTree.GetTreeTable( g_ParentIndex  );

  if g_ModuleIndex = 2 then
  begin
    Self.RzPageControl2.ActivePage := Self.RzTabSheet2;
    Self.RzPageControl1.ActivePage := Self.TabSheet4;
    Self.TabSheet4.TabVisible   := True;
    Self.RzTabSheet2.TabVisible := True;

    Self.TabSheet3.TabVisible  := False;
    Self.RzTabSheet1.TabVisible:= False;
  end else
  begin
    Self.RzPageControl2.ActivePage := Self.RzTabSheet1;
    Self.RzPageControl1.ActivePage := Self.TabSheet3;
    Self.TabSheet4.TabVisible   := False;
    Self.RzTabSheet2.TabVisible := False;

    Self.TabSheet3.TabVisible  := True;
    Self.RzTabSheet1.TabVisible:= True;
  end;
  Self.pcSys.ActivePage := Self.TabSheet1;
end;

procedure TfrmFinalCost.RzPanel3Resize(Sender: TObject);
var
  szWidth : Double;
begin
  inherited;
  szWidth    := Self.RzPanel3.Width / 3;
  Self.RzPanel2.Width:= Round(szWidth);
  Self.RzPanel5.Width:= Round(szWidth);
  Self.RzPanel6.Width:= Round(szWidth);
end;

procedure TfrmFinalCost.RzToolButton13Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit( tvDealGridColumn18.EditValue , Self.Final)
end;

procedure TfrmFinalCost.RzToolButton14Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit(tvGrid3Column18.EditValue,Self.Master);
end;

procedure TfrmFinalCost.RzToolButton15Click(Sender: TObject);
var
  szColName : string;
  sqltext : string;

begin
  inherited;
  szColName := Self.tvDealGridColumn3.DataBinding.FieldName;
  sqltext := 'select * from ' + g_Table_Finance_ProjectFinal + ' where Code= "' +
  g_PostCode + ' " and ' +
  szColName  +' between :t1 and :t2';
  SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.final);
end;

procedure TfrmFinalCost.RefreshDetailTable();
begin
  with Self.Detailed do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_DetailTable;
    Open;
    if RecordCount <> 0 then SetcxGrid(Self.tvView,0,g_dir_DetailTable);

  end;

end;

procedure TfrmFinalCost.RzToolButton16Click(Sender: TObject);
var
  Child   : TfrmPayDetailed;
  szName  : string;
  szMoney : Currency;

begin
  inherited;
  with Self.tvGrid3.DataController.DataSource.DataSet do
  begin
      if not IsEmpty then
      begin
          Child := TfrmPayDetailed.Create(Self);
          try
            Self.tvGrid3.Controller.FocusedRow.Collapse(True);
            szName := Self.tvGrid3Column11.DataBinding.FieldName;

            szMoney:= FieldByName(szName).AsCurrency;
            Child.g_Money    := szMoney;
            Child.g_formName := Self.Name;

            Child.g_Detailed := g_dir_DetailTable;

            szName := Self.tvGrid3Column2.DataBinding.FieldName;
            Child.g_PactCode   := g_PostCode;
            Child.g_TableName  := g_Table_DetailTable;

            Child.g_parentCode := FieldByName(szName).AsString;
            Child.ShowModal;
            SetcxGrid(Self.tvView,0,g_dir_DetailTable);

            Self.RefreshDetailTable;

            Self.tvGrid3.Controller.FocusedRow.Expand(True);
          finally
            Child.Free;
          end;

      end;
  end;

end;

procedure TfrmFinalCost.RzToolButton17Click(Sender: TObject);
var
   Node   : TTreeNode;
   szNode : PNodeData;
   Child  : TfrmPostPact;
   szCodeList  : string;
   szTableList : array[0..3] of TTableList;
begin

  Node := Self.TreeView1.Selected;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin
       szNode :=  PNodeData(Node.Data);
       treeCldnode(Node,szCodeList);
       Child := TfrmPostPact.Create(Self);
       try

         Child.g_PatTree    := g_PatTree;
         Child.g_PostCode   := szNode.Code;
         Child.g_TableName  := g_Table_Finance_FinalAccount;
         Child.g_Prefix     := 'DTGC';
         Child.g_ProjectDir := ''; //附件目录
         Child.g_TreeNode   := Node;
         if Sender = Self.RzToolButton17 then
         begin
           //新增
           Child.cxDateEdit1.Date := Date;
           Child.g_IsModify := False;
           Child.ShowModal;
         end else
         if Sender = Self.RzToolButton18 then
         begin
           //编辑

           Child.cxDateEdit1.Date := szNode.InputDate;
           Child.cxLookupComboBox1.Text := szNode.SignName;
           Child.cxMemo1.Text := szNode.remarks;
           Child.g_IsModify   := True;
           Child.ShowModal;

         end else
         if Sender = Self.RzToolButton19 then
         begin

           if Node.Level <= 1 then
           begin
             Application.MessageBox('顶级帐目禁止删除!', ' ' ,MB_OK + MB_ICONQUESTION ) ;
           end
           else
           begin
             //可以删除

              szNode := PNodeData(Node.Data);
              if MessageBox(handle, PChar('确认要删除"' + szNode.SignName + '"'),
                       '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
              begin
                szTableList[0].ATableName := g_Table_Finance_ProjectFinal;  //成本管理
                szTableList[0].ADirectory := g_Dir_ProjectFinal;

                szTableList[1].ATableName := g_Table_BranchMoney; //交易记录
                szTableList[1].ADirectory := g_dir_BranchMoney;

                szTableList[2].ATableName := g_Table_DetailTable; //交易记录明细
                szTableList[2].ADirectory := '';

                treeCldnode(Node,szCodeList);
                DeleteTableFile(szTableList,szCodeList);

                DM.InDeleteData(g_Table_Projects_Tree,szCodeList);
                g_PatTree.DeleteTree(Node);

              end;

           end;

         end;

       finally
         Child.Free;
       end;

    end else
    begin
      Application.MessageBox('顶级节点禁止新增/编辑!','',MB_OK + MB_ICONQUESTION ) ;
    end;

  end else
  begin
    Application.MessageBox('请选择合同在点击新增/编辑/删除!','',MB_OK + MB_ICONQUESTION )
  end;

end;

procedure TfrmFinalCost.RzToolButton1Click(Sender: TObject);
var
  Node : TTreeNode;

begin
  inherited;
  Node := Self.TreeView1.Selected;
  if Node.Level > 1 then
  begin
    with Self.final do
    begin
      if State <> dsInactive then
      begin
        Append;
      end;
    end;
  end else
  begin
    Application.MessageBox('该界面以限制，请新建一个工程项目，在进行操作！!!',m_title,MB_OK + MB_ICONQUESTION )
  end;
end;

procedure TfrmFinalCost.RzToolButton20Click(Sender: TObject);
begin
  inherited;
  GetMaintainInfo;
  ShowMessage('刷新完成!');
end;

procedure TfrmFinalCost.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  IsDeleteEmptyData(Self.final ,
                g_Table_Finance_ProjectFinal ,
                Self.tvDealGridColumn10.DataBinding.FieldName);
end;

procedure TfrmFinalCost.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  DeleteSelection(Self.tvDealGrid,'',g_Table_Finance_ProjectFinal,
          Self.tvDealGridColumn2.DataBinding.FieldName);
          Self.final.Requery;
end;

procedure TfrmFinalCost.RzToolButton4Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_Dir_Project_Mechanics;
    Child.ShowModal;
    SetcxGrid(Self.tvDealGrid,0,g_Dir_Project_Mechanics);
  finally
    Child.Free;
  end;
end;

procedure TfrmFinalCost.RzToolButton6Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmFinalCost.RzToolButton7Click(Sender: TObject);
var
  s , str : string;
  Node: TTreeNode;
  Code: string;
  szNode : PNodeData;
  szName : string;
  i , szIndex : Integer;
  Id , dir: string;
  Child : TfrmIsViewGrid;
  szProjectName : string;
begin
  inherited;

  if Sender = Self.RzToolButton7 then
  begin
  //支款添加
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      if Node.Level > 1 then
      begin
        szProjectName := Node.Text;

        with Self.Master do
        begin
          if State = dsInactive then
          begin
            Application.MessageBox( '当前状态无效，选择左侧后在尝试新增!', '提示:',MB_OKCANCEL + MB_ICONWARNING)
          end else
          begin
            Append;
            FieldByName('Code').Value := g_PostCode;

            szName := Self.tvGrid3Column2.DataBinding.FieldName;
            FieldByName(szName).Value := DM.getDataMaxDate(g_Table_BranchMoney);

            szName := Self.tvGrid3Column3.DataBinding.FieldName;
            FieldByName(szName).Value := Date;

            szName := Self.tvGrid3Column4.DataBinding.FieldName;
            FieldByName(szName).Value := szProjectName;

            szName := Self.tvGrid3Column8.DataBinding.FieldName;
            FieldByName(szName).Value := g_TopLevelName;

            szName := Self.tvGrid3Column18.DataBinding.FieldName;
            FieldByName(szName).Value := True ; //g_SettleStatus[1];

            Self.cxGrid1.SetFocus;
            SetGridFocus(Self.tvgrid3);
            Self.tvGrid3.DataController.Edit;

          end;

        end;
      end else
      begin
        Application.MessageBox('该界面以限制，请新建一个工程项目，在进行操作！!!',m_title,MB_OK + MB_ICONQUESTION )
      end;

    end else
    begin
      Application.MessageBox('请选择要添加的合同!!',m_title,MB_OK + MB_ICONQUESTION )
    end;

  end else
  if Sender = Self.RzToolButton8 then
  begin
    IsDeleteEmptyData(Self.Master ,
              g_Table_BranchMoney ,
              Self.tvGrid3Column8.DataBinding.FieldName);
  end else
  if Sender = Self.RzToolButton9 then
  begin
  //支款删除

      if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin
          with Self.tvGrid3.DataController.DataSource.DataSet do
          begin
            if not IsEmpty then
            begin

              for I := 0 to Self.tvGrid3.Controller.SelectedRowCount -1 do
              begin
                szName  := Self.tvGrid3Column2.DataBinding.FieldName;
                szIndex := Self.tvGrid3.GetColumnByFieldName(szName).Index;
                Id := Self.tvGrid3.Controller.SelectedRows[i].Values[szIndex];

                dir := ConcatEnclosure( Concat(g_Resources ,  '\' + g_dir_BranchMoney) , id);
                if DirectoryExists(dir) then  DeleteDirectory(dir);

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;
              end;

              with DM.Qry do
              begin
                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_BranchMoney + ' where ' + 'numbers' +' in("' + str + '")';
                ExecSQL;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  g_Table_DetailTable + ' where ' + 'parent' +' in("' + str + '")';
                ExecSQL;
              end;
              Self.Master.Requery();
              Self.Detailed.Requery();
            end;

          end;

      end;

  end else
  if Sender = Self.RzToolButton10 then
  begin
     //表格设置
     Child := TfrmIsViewGrid.Create(Application);
     try
        Child.g_fromName := Self.Name +  g_dir_BranchMoney;
        Child.ShowModal;
        SetcxGrid(Self.tvGrid3,0,g_dir_BranchMoney);
     finally
        Child.Free;
     end;
  end;

end;

function GetModuleTableName:string;stdcall;
var
  szTableName : string;
begin
  case g_ModuleIndex of
    1: szTableName := g_Table_Project_Worksheet; //外包劳务
    2: ; //员工考勤
    3: ; //材料租赁
    4: szTableName := g_Table_Project_Lease; //机械租赁
    5: szTableName := g_Table_Project_BuildStorage; //材料
    6: szTableName := g_Table_Project_EngineeringQuantity; //专业分包
    7: szTableName := g_Table_Project_Concrete; //商混
    8: szTableName := ''; //项目成本
    9: szTableName := g_Table_Project_Mechanics; //土石方
  end;
  Result := szTableName;
end;

function TfrmFinalCost.GetGroupMoney():string;
var
  szColName , s: string;
begin
  szColName := Self.ProjectCol2.DataBinding.FieldName;
  s := 'SELECT ProjectName,SUM(SumMoney) as t,Receivables,Payee FROM '+
       g_Table_RuningAccount +' where '+ szColName +'="' +
       g_ProjectName + '" AND Receivables="' + g_TopLevelName +
       '" AND fromType=1 AND status=yes GROUP BY ProjectName,Receivables,Payee';
  with Self.FlowingGroup do
  begin
    Close;
    SQL.Text := s;
    Open;
  end;
end;

procedure TfrmFinalCost.TreeView1Change(Sender: TObject; Node: TTreeNode);
var
  szNode : PNodeData;
  s : string;
  szColName  : string;
  szTableName: string;
  t : Currency;

begin
  inherited;
  if Assigned(Node) then
  begin
    szNode :=  PNodeData(Node.Data);
    if Assigned(szNode) then
    begin
      g_SignName := szNode.SignName;
      g_CodeList:='';
      treeCldnode(Node,g_CodeList);
      g_TopLevelName:= GetTreeLevelName(Node);

      if Node.Level > 1 then
      begin
        g_ProjectName := Node.Text;// string;
        GetGroupMoney;
      end;

      if Node.Level = 1 then
      begin
        //取出项目名称

        szColName := Self.ProjectCol2.DataBinding.FieldName;
        szTableName := GetModuleTableName;
        if Length(szTableName) <> 0 then
        begin
          if g_ModuleIndex = 2 then
          begin

          end else
          begin
            s := 'SELECT '+ szColName + ' FROM '+ szTableName +
               ' where SignName="' + Node.Text +'" GROUP BY ' + szColName;

          end;

          with Self.ADOProjectName do
          begin
            Close;
            SQL.Clear;
            SQL.Text := s;
            Open;
          end;

        end;

      end else
      begin
        Self.ADOProjectName.Close;
      end;

      g_PostCode    := szNode.Code;
      g_CodeList := 'Code in('''+ g_CodeList + ''')';
      with Self.final do
      begin
        Close;
        SQL.Text := 'Select * from '+ g_Table_Finance_ProjectFinal +' Where ' + g_CodeList;
        Open;
      end;

      with Self.Master do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from ' + g_Table_BranchMoney + ' where ' + g_CodeList;
        Open;
      end;
      Self.RefreshDetailTable;

      //g_Table_Maintain_CompanyInfoList //员工表
      //g_Table_Company_Staff_CheckWork  //考勤表

      s := 'select t3.SignName,t3.Parent from ('+ g_Table_Maintain_CompanyInfoList +
           ' AS t1 right join '+ g_Table_Company_Staff_CheckWork +
           ' AS t2 on t1.id=t2.Parent) left join ' + g_Table_Maintain_CompanyInfoTree +
           ' AS t3 on t1.TreeId=t3.Parent WHERE  t1.Contacts = "'+ g_SignName +'" GROUP BY  t3.SignName,t3.Parent';
      with Self.ADOYears do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;

      end;

      //进销管理
      dwCompanyMoney := 0;
      s := 'SELECT DeliverCompany as c,SUM(SumMonery) as t FROM '+ g_Table_Company_StorageDetailed +' where ' +
        ' status=yes AND DeliverCompany="'+ g_TopLevelName +'" AND AccountsStatus="成本挂账" GROUP BY DeliverCompany';
      with DM.Qry do
      begin
        Close;
        dxMemData1.Close;
        SQL.Text := s ;
        Open;
        if RecordCount <> 0 then
        begin
          t := FieldByName('t').AsCurrency;
          dxMemData1.Active := True;
          while not Eof do
          begin
            dwCompanyMoney := dwCompanyMoney + t;
              dxMemData1.Append;
              dxMemData1.FieldByName('c').AsString   := FieldByName('c').AsString;
              dxMemData1.FieldByName('t').AsCurrency := t;
              dxMemData1.FieldByName('d').AsString   := MoneyConvert( t );
            Next;
          end;
          dxMemData1.Post;
        end;

      end;

    end;

  end;

end;

procedure TfrmFinalCost.tvDealGridColumn10PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmCompanyManage;
  lvTelephone : Variant;

begin
  inherited;
  Child := TfrmCompanyManage.Create(Application);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;
    tvDealGridColumn10.EditValue :=  Child.dwSignName;
  finally
    Child.Free;
  end;
end;

procedure TfrmFinalCost.tvDealGridColumn15PropertiesPopup(Sender: TObject);
begin
  inherited;
  if Self.cxDBDateEdit1.Text = '' then Self.cxDBDateEdit1.Date := Date;

  if Self.cxDBDateEdit2.Text = '' then Self.cxDBDateEdit2.Date := Date;
  
end;

procedure TfrmFinalCost.tvDealGridColumn18PropertiesChange(Sender: TObject);
begin
  inherited;
  Self.final.UpdateBatch(arAll);
end;

procedure TfrmFinalCost.tvDealGridColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr( ARecord.Index + 1 );
end;

procedure TfrmFinalCost.tvDealGridColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvDealGrid,1,g_Dir_Project_Mechanics);
end;

procedure TfrmFinalCost.tvDealGridDblClick(Sender: TObject);
begin
  inherited;
  Self.RzToolButton13.Click;
end;

procedure TfrmFinalCost.tvDealGridEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  AAllow := ADOBanEditing(Self.Final);
  if (AItem.Index = Self.tvDealGridColumn1.Index) or
     (AItem.Index = Self.tvDealGridColumn2.Index) then
  begin
    AAllow := False;
  end;

end;

procedure TfrmFinalCost.tvDealGridEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvDealGridColumn15.Index) then
  begin
    if Self.tvDealGrid.Controller.FocusedRow.IsLast then
    begin
      //在最后一行新增
      if IsNewRow then Self.RzToolButton1.Click;
    end;
  end;  
end;

procedure TfrmFinalCost.tvDealGridStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvDealGridColumn18.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmFinalCost.tvDealGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;

begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr( AValue );
    Self.RzPanel10.Caption := s;
    s := MoneyConvert(StrToCurrDef(s,0));
    CalcValue;
  end;
  Self.tvDealGridColumn16.Summary.FooterFormat := s;
  Self.RzPanel19.Caption := s;
end;

procedure TfrmFinalCost.tvFlowingViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    Self.tvFlowingViewColumn6.Summary.FooterFormat := MoneyConvert(StrToCurrDef(s,0));
    CalcValue;
  end;
end;

procedure TfrmFinalCost.tvGrid3Column11PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  szColName : string;

begin
  inherited;
  //写入大写
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    with Self.Master do
    begin
      if State <> dsInactive then
      begin
        szColName := Self.tvGrid3Column12.DataBinding.FieldName;
        FieldByName(szColName).value := MoneyConvert(StrToCurrDef(s,0));
        szColName := Self.tvGrid3Column11.DataBinding.FieldName;
        FieldByName(szColName).Value := DisplayValue;
      end;

    end;

  end;

end;

procedure TfrmFinalCost.tvGrid3ColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvGrid3,1,g_dir_BranchMoney);
end;

procedure TfrmFinalCost.tvGrid3Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  AAllow := ADOBanEditing(Self.Master);
  if AItem.Index = Self.tvGrid3Column1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmFinalCost.tvGrid3EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvGrid3Column11.Index) then
  begin
    if Self.tvGrid3.Controller.FocusedRow.IsLast then
    begin
      //在最后一行新增
      if IsNewRow then Self.RzToolButton7.Click;
    end;
  end; 
end;

procedure TfrmFinalCost.tvGrid3StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvGrid3Column18.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmFinalCost.tvGrid3TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
  szColName : string;

begin
  inherited;
  //写入大写
  if  AValue <> null then
  begin
    s := VarToStr( AValue );
    Self.RzPanel13.Caption := s;
    Self.RzPanel14.Caption := MoneyConvert(StrToCurrDef(s,0));

    Self.tvGrid3Column12.Summary.FooterFormat := MoneyConvert(StrToCurrDef(s,0));
    CalcValue;
  end;

end;

procedure TfrmFinalCost.tvGroupViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var 
  s : string;
  
begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    Self.tvGroupViewColumn1.Summary.FooterFormat := MoneyConvert(StrToCurrDef(s,0));
    CalcValue;
  end;
end;

procedure TfrmFinalCost.tvProjectViewDblClick(Sender: TObject);
var
  szName : string;
  szColName : string;
  szColList : string;
  szSumListName : string;
  s : string;
  szTableName : string;
  DD1 : string;

begin
  inherited;
  szColName := Self.ProjectCol2.DataBinding.FieldName;

  with Self.ADOProjectName do
  begin
    if State <> dsInactive then
     begin
       if not IsEmpty then
        begin
          szName := FieldByName(szColName).AsString;
          szColList := Self.tvGroupViewCol2.DataBinding.FieldName;
          //帐目总价
          s := 'SELECT ProjectName,SUM(SumMoney) as t,Receivables,Payee FROM '+
          g_Table_RuningAccount +' where '+ szColName +'="' +
          szName + '" AND Receivables="' + g_SignName +
          '" AND fromType=1 AND status=yes GROUP BY ProjectName,Receivables,Payee';

          with Self.FlowingGroup do
          begin
            Close;
            SQL.Text := s;
            Open;
          end;

          //考勤汇总
          szTableName := GetModuleTableName;
          if Length(szTableName ) <> 0 then
          begin

            DD1 := 'SUM(SumMoney) as t,';
            case g_ModuleIndex of
              1: szSumListName := DD1 + 'SUM(WorkDays) as w , SUM(OverSumday) as O'; //外包劳务
              2: szSumListName := ''; //员工考勤
              3: szSumListName := ''; //材料租赁
              4: szSumListName := DD1 + 'SUM(LeaseDayNum) as w '; //机械租赁
              5: szSumListName := DD1 + 'SUM(Total) as w'; //材料
              6: szSumListName := DD1 + 'SUM(MeteringCount) as w'; //专业分包
              7: szSumListName := DD1 + 'SUM(Stere) as w '; //商混
              8: szSumListName := ''; //项目成本
              9: szSumListName := DD1 + 'SUM(TotalVolume) as w '; //土石方
            end;

            s := 'SELECT '+ szColList +','+ szSumListName +' FROM '+ szTableName +' where '+ szColName
               +'="'+ szName + '" AND SignName="' + g_SignName + '" AND status=yes GROUP BY ' + szColList;

            if Length(s) <> 0 then
            begin
              with DM.Qry do
              begin
                Close;
                ADOGroup.Close;
                SQL.Text := s;
                Open;
                if RecordCount <> 0 then
                begin
                  ADOGroup.Active := True;
                  while not Eof do
                  begin
                    ADOGroup.Edit;
                    ADOGroup.FieldByName('ProjectName').AsString   := FieldByName('ProjectName').AsString;
                    ADOGroup.FieldByName('t').AsCurrency := FieldByName('t').AsCurrency;
                    ADOGroup.FieldByName('d').AsString   := MoneyConvert(FieldByName('t').AsCurrency);
                    Next;
                  end;
                  ADOGroup.Post;
                end;
              end;

            end;

          end;
          
        end;
        
     end;

  end;

end;

end.


unit ufrmPostStorage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxGroupBox, Vcl.ExtCtrls,
  RzPanel, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, RzButton, cxLabel, cxTextEdit, cxMemo, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxMaskEdit, cxDropDownEdit, cxCalendar, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxDBEdit, cxSpinEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, Vcl.DBCtrls, RzDBCmbo, Datasnap.DBClient,UnitADO,
  cxCheckBox, cxCurrencyEdit,ufrmBaseController, frxDesgn, frxClass, frxDBSet,
  frxBarcode, Data.Win.ADODB;

type
  TfrmPostStorage = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    cxLabel2: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    RzPanel4: TRzPanel;
    RzToolbar13: TRzToolbar;
    RzSpacer30: TRzSpacer;
    RzToolButton85: TRzToolButton;
    RzToolButton90: TRzToolButton;
    RzSpacer120: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzSpacer6: TRzSpacer;
    Grid: TcxGrid;
    tView: TcxGridDBTableView;
    tViewCol1: TcxGridDBColumn;
    tViewCol4: TcxGridDBColumn;
    tViewCol3: TcxGridDBColumn;
    tViewCol5: TcxGridDBColumn;
    tViewCol6: TcxGridDBColumn;
    tViewCol7: TcxGridDBColumn;
    tViewCol8: TcxGridDBColumn;
    tViewCol9: TcxGridDBColumn;
    tViewCol10: TcxGridDBColumn;
    tViewCol11: TcxGridDBColumn;
    Lv: TcxGridLevel;
    cxLabel8: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel10: TcxLabel;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Bevel1: TBevel;
    Bevel2: TBevel;
    cxLabel9: TcxLabel;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    tViewCol12: TcxGridDBColumn;
    tViewCol13: TcxGridDBColumn;
    tViewCol14: TcxGridDBColumn;
    DataDetailed: TDataSource;
    Detailed: TClientDataSet;
    Master: TClientDataSet;
    tViewCol2: TcxGridDBColumn;
    ClientMakings: TClientDataSet;
    DataMakings: TDataSource;
    tViewCol15: TcxGridDBColumn;
    tViewCol16: TcxGridDBColumn;
    tViewCol17: TcxGridDBColumn;
    cxLabel16: TcxLabel;
    tViewColumn1: TcxGridDBColumn;
    tViewColumn2: TcxGridDBColumn;
    tViewColumn3: TcxGridDBColumn;
    tViewColumn4: TcxGridDBColumn;
    tViewColumn5: TcxGridDBColumn;
    tViewColumn6: TcxGridDBColumn;
    tViewColumn7: TcxGridDBColumn;
    tViewColumn8: TcxGridDBColumn;
    tViewColumn9: TcxGridDBColumn;
    tViewColumn10: TcxGridDBColumn;
    tViewColumn11: TcxGridDBColumn;
    cxCheckBox1: TcxCheckBox;
    tViewColumn12: TcxGridDBColumn;
    cxButton6: TcxButton;
    tViewColumn13: TcxGridDBColumn;
    pm: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    tViewColumn14: TcxGridDBColumn;
    Report: TPopupMenu;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    cxButton7: TcxButton;
    RzSpacer2: TRzSpacer;
    cxDBLookupComboBox1: TcxDBLookupComboBox;
    cxDBLookupComboBox3: TcxDBLookupComboBox;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBComboBox1: TcxDBComboBox;
    BillNumber: TcxDBTextEdit;
    cxDBDateEdit1: TcxDBDateEdit;
    cxDBComboBox2: TcxDBComboBox;
    cxDBTextEdit3: TcxDBTextEdit;
    cxDBComboBox3: TcxDBComboBox;
    cxDBTextEdit5: TcxDBTextEdit;
    cxDBTextEdit4: TcxDBTextEdit;
    cxDBTextEdit6: TcxDBTextEdit;
    cxDBDateEdit2: TcxDBDateEdit;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    cxDBTextEdit7: TcxDBTextEdit;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    Timer1: TTimer;
    cxLabel18: TcxLabel;
    cxLabel15: TcxLabel;
    ADOContract: TADOQuery;
    ContractSource: TDataSource;
    cxDBLookupComboBox4: TcxDBLookupComboBox;
    cxDBLookupComboBox5: TcxDBLookupComboBox;
    RzPanel3: TRzPanel;
    Bevel3: TBevel;
    cxGroupBox4: TcxGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label3: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    cxSpinEdit5: TcxSpinEdit;
    cxComboBox1: TcxComboBox;
    cxGroupBox2: TcxGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    cxDBMemo2: TcxDBMemo;
    cxDBSpinEdit2: TcxDBSpinEdit;
    cxDBSpinEdit3: TcxDBSpinEdit;
    cxLabel13: TcxLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxLabel14: TcxLabel;
    cxComboBox2: TcxComboBox;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    procedure cxButton3Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure RzToolButton90Click(Sender: TObject);
    procedure tViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton6Click(Sender: TObject);
    procedure DetailedAfterInsert(DataSet: TDataSet);
    procedure cxButton4Click(Sender: TObject);
    procedure tViewCol7PropertiesCloseUp(Sender: TObject);
    procedure tViewCol7PropertiesInitPopup(Sender: TObject);
    procedure tViewCol7PropertiesPopup(Sender: TObject);
    procedure cxDBMemo2PropertiesChange(Sender: TObject);
    procedure tViewCol9GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure tViewCol4PropertiesCloseUp(Sender: TObject);
    procedure tViewCol8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxButton5Click(Sender: TObject);
    procedure RzToolButton85Click(Sender: TObject);
    procedure tViewCol15PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tViewCol16PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tViewDblClick(Sender: TObject);
    procedure tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure RzToolButton1Click(Sender: TObject);
    procedure cxDBSpinEdit2PropertiesChange(Sender: TObject);
    procedure cxDBSpinEdit3PropertiesChange(Sender: TObject);
    procedure tViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure cxButton6Click(Sender: TObject);
    procedure tViewColumn10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxComboBox2PropertiesCloseUp(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure tViewCol10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxButton7Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure cxDBLookupComboBox1Enter(Sender: TObject);
    procedure cxDBComboBox2Enter(Sender: TObject);
    procedure cxDBDateEdit2Enter(Sender: TObject);
  private
    { Private declarations }
    dwSQLtext : string;
    Radix : Integer;
    function GetRadix(lpRadix : Integer):BOOL;
    procedure GetDetailed(lpNumber : string);
    procedure PopupKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure GetSumMoney(Count , UnitPrice  , Weight : Variant);
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  public
    { Public declarations }
    dwADO : TADO;
    dwTableName : string;
    dwDetailedTable : string;
    dwMasterSQL : string;
    dwTreeCode  : string;
    dwDataType  : Integer;
    dwNodeId    : Integer;
    dwAssortment: Integer;  //分类

  end;

type
  TCusDropDownEdit = class(TcxCustomDropDownEdit);

var
  frmPostStorage: TfrmPostStorage;

implementation

uses
   uDataModule,System.Math,global,ufrmIsViewGrid,ufrmReport;

{$R *.dfm}

procedure TfrmPostStorage.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, lpSuffix);
  end;
end;

procedure TfrmPostStorage.Timer1Timer(Sender: TObject);
var
  szNumber : string;
  s : string;
  i : Integer;
begin
  inherited;
  Timer1.Enabled := False;
  Self.cxComboBox1.Clear;
  for I := 0 to High(RebarList) do Self.cxComboBox1.Properties.Items.Add( RebarList[i].Model );

  SetcxGrid(Self.tView,0, dwDetailedTable);

  dwADO.OpenSQL(ClientMakings,'Select * from ' + g_Table_MakingsList);

  case dwDataType of
     0: Self.RzPanel1.Caption := '入　　库　　单';
     1: Self.RzPanel1.Caption := '出　　库　　单';
     2: Self.RzPanel1.Caption := '退　　还　　单';
     3: Self.RzPanel1.Caption := '编  辑  单  据';
     4: Self.RzPanel1.Caption := '退　　货　　单';
  end;

  //进货退货单、销货退货单、租入退还单、租出退还单、调入退还单、调出退还单、借入退换单、借出退还单、

  //入库单：进货入库单、租赁入库单、调存入库单、借入入库单、成本挂账、已付款额、租入记账、记账统计
  //出库单：销货出库单、租出出库单、调出出库单、借出出库单、营收挂账、已收款额、记账统计                                                                                 退还单： 进货退货单、销货退货单、租入退还单、租出退还单、调存退还单、调出退还单、借入退换单、借出退还单、                      记账统计   退货统计  租赁统计   外借统计、调还统计

  Self.cxDBComboBox1.Properties.Items.Clear;
  case dwAssortment of
     0:
     begin
       s := '入　　库　　单';
       Self.cxDBComboBox1.Properties.Items.Add('账目入库单');
       Self.cxDBComboBox1.Properties.Items.Add('进货入库单');
       Self.cxDBComboBox1.Properties.Items.Add('租入进库单');
       Self.cxDBComboBox1.Properties.Items.Add('借入进货单');
       Self.cxDBComboBox1.Properties.Items.Add('调入进货单');
       with Self.cxDBComboBox2.Properties.Items do
       begin
         Clear;
         Add('成本挂账');
         Add('现金支出');
         Add('租入统计');
         Add('借入统计');
         Add('调入统计');
       end;
     end;
     1:
     begin
       s := '出　　库　　单';
       Self.cxDBComboBox1.Properties.Items.Add('账目出库单');
       Self.cxDBComboBox1.Properties.Items.Add('销货出库单');
       Self.cxDBComboBox1.Properties.Items.Add('租出销货单');
       Self.cxDBComboBox1.Properties.Items.Add('借出销货单');
       Self.cxDBComboBox1.Properties.Items.Add('调出销货单');
       with Self.cxDBComboBox2.Properties.Items do
       begin
         Clear;
         Add('营收挂账');
         Add('现金收出');
         Add('租出统计');
         Add('借出统计');
         Add('调出统计');
       end;

     end;
     2:
     begin
       s := '退　　还　　单';
       Self.cxDBComboBox1.Properties.Items.Add('销货退还单');
       Self.cxDBComboBox1.Properties.Items.Add('销货退还单');
       Self.cxDBComboBox1.Properties.Items.Add('租出退还单');
       Self.cxDBComboBox1.Properties.Items.Add('借出退还单');
       Self.cxDBComboBox1.Properties.Items.Add('调出退还单');
       with Self.cxDBComboBox2.Properties.Items do
       begin
         Clear;
         Add('销货退还统计');
         Add('销货退还统计');
         Add('租出退还统计');
         Add('借出退还统计');
         Add('调出退还统计');
       end;
     end;
     3:
     begin
       //编辑
     end;
     4:
     begin
       s := '退　　货　　单';
       Self.cxDBComboBox1.Properties.Items.Add('进货退货单');
       Self.cxDBComboBox1.Properties.Items.Add('进货退货单');
       Self.cxDBComboBox1.Properties.Items.Add('租入退货单');
       Self.cxDBComboBox1.Properties.Items.Add('借入退货单');
       Self.cxDBComboBox1.Properties.Items.Add('调入退货单');
       with Self.cxDBComboBox2.Properties.Items do
       begin
         Clear;
         Add('进货退货统计');
         Add('进货退货统计');
         Add('租入退货统计');
         Add('借入退货统计');
         Add('调入退货统计');
       end;
     end;
  end;
  szNumber := Self.BillNumber.Text;
  if Self.Detailed.IsEmpty then  GetDetailed(szNumber);
end;

procedure TfrmPostStorage.GetSumMoney(Count , UnitPrice , Weight : Variant);
var
  szSumMoney : Currency;
  szColName : string;
  szValue : Currency;
  s : string;

begin
  inherited;                                                     
  if (UnitPrice <> null) and (Count <> null) and (Weight <> null) then
  begin
    if self.Master.State in [dsEdit,dsInsert] then Self.Master.Edit;

    if Weight <= 0 then
    begin
       Weight := 1;
    end;

    szSumMoney := StrToCurrDef(  VarToStr( UnitPrice ),0) *
                  StrToFloatDef( VarToStr( Count ) ,0 ) *
                  StrToFloatDef( VarToStr( Weight ) ,0 );

    Self.tViewCol10.EditValue := szSumMoney;

    Self.cxDBCurrencyEdit1.Clear;
    Self.cxDBTextEdit7.Clear;

    s := VarToStr(Self.tView.DataController.Summary.FooterSummaryValues[0]);
    szValue := StrToCurrDef(s,0);

    Self.tViewColumn14.EditValue := MoneyConvert( szSumMoney );
  end;
end;

procedure TfrmPostStorage.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton3.Click;
end;

procedure TfrmPostStorage.N2Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton6.Click;
end;

procedure TfrmPostStorage.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton85.Click;
end;

function TfrmPostStorage.GetRadix(lpRadix : Integer):BOOL;
begin
  case lpRadix of
    0: Radix := -1;
    1: Radix := -2;
    2: Radix := -3;
    3: Radix := -4;
    4: Radix := -5;
    5: Radix := -6;
    6: Radix := -7;
    7: Radix := -8;
  end;

end;

procedure TfrmPostStorage.PopupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 27 then //如果按下“ESC”键
    TcxCustomEditPopupWindow(sender).ClosePopup;//关闭弹出窗
end;

procedure TfrmPostStorage.GetDetailed(lpNumber : string);
begin
  with DM do
  begin
    ADOQuery1.Close;
    ADOQuery1.SQL.Clear;
    dwSQLtext := 'Select * from ' + dwTableName + ' WHERE ' + Self.tViewCol3.DataBinding.FieldName + ' ="' + lpNumber +'"';
    ADOQuery1.SQL.Text := dwSQLtext;
    ADOQuery1.Open;

    Self.Detailed.Data := DataSetProvider1.Data;
    Self.Detailed.Open;
  end;
end;

procedure TfrmPostStorage.cxButton2Click(Sender: TObject);
var
  ErrorCount : Integer;

begin

  with Self.Detailed do
  begin
    if State <> dsInactive then
    begin
      if State in [DsEdit, DSInsert] then Post;

      if ChangeCount > 0 then
      begin
        DM.DataSetProvider1.ApplyUpdates(Delta,0,ErrorCount);
        MergeChangeLog;
      end;

    end;

  end;
  dwADO.ADOSaveData(Self.Master,dwMasterSQL);
//  dwADO.UpdateTableData(dwMasterSQL,Self.BillNumber.DataBinding.DataField,1,Self.Master);
  if dwDataType <> 3 then
  begin
    if Self.cxCheckBox1.Checked then
    begin
      dwADO.ADOAppend(Self.Master);
    end;
  end;

  GetDetailed( Self.BillNumber.Text );
  if not Self.cxCheckBox1.Checked then
  begin
    Close;
  end;
end;

procedure TfrmPostStorage.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmPostStorage.cxButton4Click(Sender: TObject);
begin
  Self.tViewCol7.Editing := False;
end;

procedure TfrmPostStorage.cxButton5Click(Sender: TObject);
begin
  inherited;
  Self.tViewCol7.Editing := False;
end;

procedure TfrmPostStorage.cxButton6Click(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources, '\'+ g_DirBuildStorage);
//  CreateEnclosure(Self.tv1.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmPostStorage.cxButton7Click(Sender: TObject);
var
  Report : TfrmReport;
  s : string;
begin
  inherited;
  s := Self.cxDBComboBox1.Text;
  Report := TfrmReport.Create(Application);
  try
    with Report.dwDictionary do
    begin
      Clear;
      Add('名称'    , Self.cxDBTextEdit1.Text);
      Add('经手人'  , Self.cxDBTextEdit5.Text);
      Add('签收人'  , Self.cxDBTextEdit3.Text);
      Add('发货人'  , Self.cxDBTextEdit4.Text);
      Add('发货单位', Self.cxDBLookupComboBox1.Text);
      Add('单据编号', Self.BillNumber.Text);
      Add('收货单位', Self.cxDBLookupComboBox3.Text);
      Add('开票日期', Self.cxDBDateEdit1.Text);
      Add('审核签字', Self.cxDBTextEdit6.Text);
      Add('签收日期', Self.cxDBDateEdit2.Text);
    end;

    Report.frxDBDataset.DataSource := Self.DataDetailed;
    Report.frxDBDataset.UserName   := '进销管理';
    Report.dwReportName := '进销管理.fr3'; //报表文件
    Report.dwReportTitle := s; //报表标题
    Report.Show;
  finally
  end;
end;

procedure TfrmPostStorage.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  i : Integer;
begin
  inherited;

  i := Self.cxComboBox1.ItemIndex;
  if i >= 0 then
  begin
    Self.cxDBSpinEdit2.EditValue := RebarList[i].Weight;
    Self.cxDBSpinEdit2.PostEditValue;

    Self.tViewCol6.EditValue := Self.cxComboBox1.Text;
  //  Self.cxDBTextEdit3.PostEditValue;
  end;

end;

procedure TfrmPostStorage.cxComboBox2PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  GetRadix(Self.cxComboBox2.ItemIndex);
end;

procedure TfrmPostStorage.cxDBComboBox2Enter(Sender: TObject);
begin
  inherited;
  (Sender AS TcxDBComboBox).DroppedDown := True;
end;

procedure TfrmPostStorage.cxDBDateEdit2Enter(Sender: TObject);
begin
  inherited;
  (Sender AS TcxDBDateEdit).DroppedDown := True;
end;

procedure TfrmPostStorage.cxDBLookupComboBox1Enter(Sender: TObject);
begin
  inherited;
  (Sender AS TcxDBLookupComboBox).DroppedDown := True;
end;

procedure TfrmPostStorage.cxDBMemo2PropertiesChange(Sender: TObject);
var
  s : Variant;
  szValue : string;
  i : Integer;
  f : Double;
begin
  inherited;

  s := Self.cxDBMemo2.EditingValue;

  if s <> null then
  begin
    szValue := FunExpCalc(s,2);

    if TryStrToInt(szValue,i) or TryStrToFloat(szValue, f) then
    begin
      Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef( szValue , 0 ) , Radix );
      Self.cxDBSpinEdit3.EditValue := szValue;
    end;
  end;

end;

procedure TfrmPostStorage.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0) , Radix );
    Self.cxDBSpinEdit1.PostEditValue;
  end;
end;

procedure TfrmPostStorage.cxDBSpinEdit2PropertiesChange(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  szValue := Self.cxDBSpinEdit3.EditingValue;

  if szValue <> null then
  begin
    s := VarToStr(szValue);

    Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0)  * Self.cxDBSpinEdit2.Value , Radix );
    Self.cxDBSpinEdit1.PostEditValue;
  end;
end;

procedure TfrmPostStorage.cxDBSpinEdit3PropertiesChange(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  szValue := Self.cxDBSpinEdit3.EditingValue;
  if szValue <> null then
  begin
    s := VarToStr(szValue);
    Self.cxDBSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0) , Radix ) ;
    Self.cxDBSpinEdit1.PostEditValue;
  end;
end;

procedure TfrmPostStorage.cxSpinEdit1PropertiesChange(Sender: TObject);
var
  szWeight    : Double;   //重量
  szMeter     : Double;   //米数
  szRootCount : Double;   //根数
  szTheoryWeight : Double; //理论重量
  szPieceCount: Double;   //件数

begin
  inherited;
  szTheoryWeight:= Self.cxDBSpinEdit2.Value; //变量数
  szMeter       := Self.cxSpinEdit2.Value;
  szRootCount   := Self.cxSpinEdit5.Value;
  szPieceCount  := Self.cxSpinEdit1.Value;

  if (szRootCount <> 0) then
  begin
    szWeight := (szMeter * (szRootCount * szPieceCount) );
    szWeight := RoundTo( szWeight * szTheoryWeight ,  Radix );

    Self.cxDBSpinEdit3.EditValue := szWeight; //计算数值编辑框
    Self.cxDBSpinEdit3.PostEditValue;

    Self.cxDBSpinEdit1.EditValue := szWeight;// szWeight;
    Self.cxDBSpinEdit1.PostEditValue;

  end;

end;

procedure TfrmPostStorage.DetailedAfterInsert(DataSet: TDataSet);
begin
  Self.tViewCol3.EditValue := Self.BillNumber.Text;
  Self.tViewCol2.EditValue := True;
  Self.tViewColumn8.EditValue := Self.cxDBLookupComboBox1.Text;
  Self.tViewColumn9.EditValue := Self.cxDBLookupComboBox3.Text;
  Self.tViewColumn11.EditValue:= Self.cxDBDateEdit1.EditValue;
  Self.tViewColumn10.EditValue:= 1;
//  Self.tViewCol7.EditValue    := 1;
  with DataSet do
  begin
    FieldByName('DataType').Value := dwDataType;
    FieldByName('TreeId').Value   := dwNodeId;
    FieldByName('Code').Value     := dwTreeCode;
    FieldByName('AccountsStatus').Value := Self.cxDBComboBox2.EditValue;
  end;
  Self.tViewCol4.Editing := True;
end;

procedure TfrmPostStorage.RzToolButton1Click(Sender: TObject);
  //表格设置
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  //http://www.a-hospital.com/
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := dwDetailedTable;
    Child.ShowModal;
    SetcxGrid(Self.tView,0, dwDetailedTable);
  finally
    Child.Free;
  end;

end;

procedure TfrmPostStorage.RzToolButton3Click(Sender: TObject);
var
  s : Variant;

begin
  s := Self.cxDBComboBox2.EditValue;
  if s <> null then
  begin
    if Length( Self.cxDBLookupComboBox1.Text ) = 0 then
    begin
      Application.MessageBox( '发货单位不能为空！', '新增记录？', MB_OKCANCEL + MB_ICONWARNING);
      Exit;
    end;

    if Length(Self.cxDBLookupComboBox3.Text) = 0 then
    begin
      Application.MessageBox( '收货单位不能为空！', '新增记录？', MB_OKCANCEL + MB_ICONWARNING);
      Exit;
    end;
    Self.Grid.SetFocus;
    dwADO.ADOAppend(Self.Detailed);
  end else
  begin
    ShowMessage('先选择结算状态，在添加明细!!');
    Self.cxDBComboBox2.SetFocus;
    Self.cxDBComboBox2.DroppedDown := True;
  end;

end;

procedure TfrmPostStorage.RzToolButton6Click(Sender: TObject);
begin
  if Sender = Self.RzToolButton6 then
  begin
    if Self.tViewCol2.EditValue = False then
    begin
      ShowMessage('冻结状态禁止编辑!');
      Exit;
    end;

  end;
  dwADO.ADOIsEdit(Self.Detailed);
end;

procedure TfrmPostStorage.RzToolButton85Click(Sender: TObject);
begin
  inherited;
  dwADO.ADODeleteSelectionData(Self.tView ,Self.Detailed, dwSQLtext );
end;

procedure TfrmPostStorage.RzToolButton90Click(Sender: TObject);
begin
//  dwADO.ADOSaveData(Self.Detailed,dwSQLtext);
end;

procedure TfrmPostStorage.tViewCol10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    s := MoneyConvert( StrToFloatDef(s,0) );
    Self.tViewColumn14.EditValue := s;
  end;
end;

procedure TfrmPostStorage.tViewCol15PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  dwUnitPrice : Currency;
  szDisPrice  : Currency;

  dwDisCount : Currency;
  dwValue : Currency;
  Count : Variant;
  szWeight  : Variant;
begin
  inherited;
  s := VarToStr( Self.tViewCol8.EditValue );
  dwUnitPrice := StrToCurrDef(s,0);
  s := VarToStr(DisplayValue);
  dwDisCount := StrToCurrDef(s,0);
  szDisPrice := dwDisCount * dwUnitPrice;
//  dwValue := dwDisCount / 100;
  Self.tViewCol16.EditValue := szDisPrice;
  Self.tViewCol15.EditValue := DisplayValue;
  Count := Self.cxDBSpinEdit1.EditingValue; //数量
  szWeight := Self.tViewColumn10.EditValue; //重量
  GetSumMoney(Count, szDisPrice , szWeight );

end;

procedure TfrmPostStorage.tViewCol16PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  Count , szWeight : Variant;
begin
  inherited;
  Count := Self.cxDBSpinEdit1.EditingValue; //数量
  Self.tViewCol16.EditValue := DisplayValue;
  szWeight := Self.tViewColumn10.EditValue; //重量
  GetSumMoney(Count, DisplayValue , szWeight );
end;

procedure TfrmPostStorage.tViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmPostStorage.tViewCol4PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;
  szColContentB : Variant;
  szColContentC : Variant;
  szColContentD : Variant;
  szRowIndex:Integer;
  szColName : string;
  szCount : Variant;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;

    if szRowIndex >= 0 then
    begin
      szColContentA :=  DataController.Values[szRowIndex,0] ; //名称
      szColContentB :=  DataController.Values[szRowIndex,1] ; //型号
      szColContentC :=  DataController.Values[szRowIndex,2] ; //规格

      if szColContentA <> null then
      begin
        Self.tViewCol4.EditValue := szColContentA;  //名称
      end;
      if szColContentB <> null then
      begin
        Self.tViewCol5.EditValue := szColContentB;  //型号
      end;
      if szColContentC <> null then
      begin
        Self.tViewCol6.EditValue := szColContentC;  //规格
      end;

    end;

  end;

end;

procedure TfrmPostStorage.tViewCol7PropertiesCloseUp(Sender: TObject);
var
  Count,UnitPrice , szWeight : Variant;
begin
  inherited;
  //Self.tvViewColumn8.Index //数量
  UnitPrice  := Self.tViewCol8.EditValue;
  Count := Self.cxDBSpinEdit1.EditingValue; //数量
  szWeight := Self.tViewColumn10.EditValue; //重量
  GetSumMoney(Count,UnitPrice , szWeight);
end;

procedure TfrmPostStorage.tViewCol7PropertiesInitPopup(Sender: TObject);
begin
  TCusDropDownEdit(sender).PopupWindow.KeyPreview := true;//令弹出窗按键事件有效
  TCusDropDownEdit(sender).PopupWindow.OnKeyDown  := PopupKeyDown;//关联按键事件给弹出窗
end;

procedure TfrmPostStorage.tViewCol7PropertiesPopup(Sender: TObject);
begin
//  Radix := -2;
  GetRadix(Self.cxComboBox2.ItemIndex);
  if Self.cxDBSpinEdit2.Value = 0 then
  begin
    Self.cxSpinEdit2.Value := 9;
    Self.cxSpinEdit5.Value := 0;
    Self.cxSpinEdit1.Value := 0;
    Self.cxComboBox1.ItemIndex := -1;
  end;
end;

procedure TfrmPostStorage.tViewCol8PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  Count , szWeight : Variant;
begin
  inherited;
  Count := Self.cxDBSpinEdit1.EditingValue; //数量
  Self.tViewCol8.EditValue  := DisplayValue;
  szWeight := Self.tViewColumn10.EditValue; //重量
  GetSumMoney(Count, DisplayValue ,szWeight );
end;

procedure TfrmPostStorage.tViewCol9GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;
begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;

end;

procedure TfrmPostStorage.tViewColumn10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  Count : Variant;
begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    Count := Self.cxDBSpinEdit1.EditingValue; //数量
    Self.tViewColumn10.EditValue  := DisplayValue;
    GetSumMoney(Count, Self.tViewCol8.EditValue , DisplayValue);
  end;

end;

procedure TfrmPostStorage.tViewColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tView,1, dwDetailedTable);
end;

procedure TfrmPostStorage.tViewDblClick(Sender: TObject);
begin
  inherited;
  Self.RzToolButton6.Click;
end;

procedure TfrmPostStorage.tViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Detailed,AAllow);
end;

procedure TfrmPostStorage.tViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  
  if (Key = 13) and
     (AItem.Index = Self.tViewCol10.Index) and
     (Self.tView.Controller.FocusedRow.IsLast) then
  begin
    if (IsNewRow ) then
    begin
      dwADO.ADOAppend(Self.Detailed);
      Self.tViewCol1.FocusWithSelection;
      Self.tViewCol1.Editing := True;
    end;
  end;
end;

procedure TfrmPostStorage.tViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;

  Value := ARecord.Values[Self.tViewCol2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmPostStorage.tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  if AValue <> null then
  begin
    Self.cxDBCurrencyEdit1.EditValue := AValue;
    Self.cxDBCurrencyEdit1.PostEditValue;

    Self.cxDBTextEdit7.EditValue := MoneyConvert( Self.cxDBCurrencyEdit1.EditValue );
    Self.cxDBTextEdit7.PostEditValue;
  end else
  begin
    Self.cxDBCurrencyEdit1.EditValue := 0;
    Self.cxDBCurrencyEdit1.PostEditValue;

    Self.cxDBTextEdit7.EditValue := MoneyConvert( Self.cxDBCurrencyEdit1.EditValue );
    Self.cxDBTextEdit7.PostEditValue;
  end;
end;

end.
